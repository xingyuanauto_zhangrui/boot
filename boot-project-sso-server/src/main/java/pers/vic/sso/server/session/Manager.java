package pers.vic.sso.server.session;

/**
 * 描述:
 *      session相关的顶级接口
 * @author Vic.xu
 * @date 2021-11-08 9:08
 */
public interface Manager {

    /**
     * 失效时间 ：秒
     */
    int expire();

    String cacheName();
}
