package pers.vic.sso.server.service;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.sso.common.model.SsoUser;

/**
 * 描述:
 *      用户登录
 * @author Vic.xu
 * @date 2021-10-29 17:39
 */
public interface SsoUserService {

    BaseResponse<SsoUser> login(String username, String password);
}
