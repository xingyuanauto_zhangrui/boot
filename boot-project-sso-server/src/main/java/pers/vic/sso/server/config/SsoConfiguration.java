package pers.vic.sso.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 描述:
 *      sso server端的一些配置
 * @author Vic.xu
 * @date 2021-11-02 17:12
 */
@Configuration
@ConfigurationProperties(prefix = "sso")
public class SsoConfiguration {

    /**
     * 超时时间：默认2个小时 <br />
     * <p>
     *  包括：登录凭证TicketGrantingTicket(tgt)；
     *  刷新凭证 RefreshToken；
     *
     *</p>
     */
    @Value("${sso.timeout:7200}")
    private int timeout;

    /**
     * 授权码超时时间 默认10分钟
     */
    @Value("${sso.codeTimeout:600}")
    private int codeTimeout;

    /**
     * accessToken 的超时时间 默认为timeout的一半
     */
    private int accessTokenTimeout;

    /**
     * 存储策略，只支持redis和local
     */
    @Value("${sso.storageStrategy:local}")
    private String storageStrategy;

    /**
     * 可用的sso client 列表
     */
    private List<AppClient> clients;

    @PostConstruct
    private void post(){
        if (accessTokenTimeout > timeout) {
            throw new IllegalStateException("accessTokenTimeout必须小于timeout， 否则不能及时刷新更换token");
        }
        if (clients ==null) {
            throw new IllegalStateException("尚未配置可用的sso客户端， 请前往配置 clients");
        }

        if (accessTokenTimeout == 0) {
            accessTokenTimeout = timeout / 2;
        }
    }

    @Override
    public String toString() {
        return "SsoConfiguration{" +
                "timeout=" + timeout +
                ", codeTimeout=" + codeTimeout +
                ", accessTokenTimeout=" + accessTokenTimeout +
                ", storageStrategy='" + storageStrategy + '\'' +
                ", clients=" + clients +
                '}';
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getCodeTimeout() {
        return codeTimeout;
    }

    public void setCodeTimeout(int codeTimeout) {
        this.codeTimeout = codeTimeout;
    }

    public int getAccessTokenTimeout() {
        return accessTokenTimeout;
    }

    public void setAccessTokenTimeout(int accessTokenTimeout) {
        this.accessTokenTimeout = accessTokenTimeout;
    }

    public String getStorageStrategy() {
        return storageStrategy;
    }

    public void setStorageStrategy(String storageStrategy) {
        this.storageStrategy = storageStrategy;
    }

    public List<AppClient> getClients() {
        return clients;
    }

    public void setClients(List<AppClient> clients) {
        this.clients = clients;
    }

    /**
     * app信息，用户server端校验client端的合法性 包含id和secret, 暂时通过配置的方式，后续考虑存入数据库
     */
    public static class AppClient {
        String id;

        String secret;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        @Override
        public String toString() {
            return "AppInfo{" +
                    "id='" + id + '\'' +
                    ", secret='" + secret + '\'' +
                    '}';
        }
    }
}
