package pers.vic.sso.server.service;

import pers.vic.boot.base.vo.BooleanWithMsg;

/**
 * 应用服务相关接口
 */
public interface AppService {

    /**
     * 检测appId是否存在
     * @param appId
     * @return
     */
    boolean checkAppId(String appId);

    /**
     * 检测appId 和对应的秘钥是否正确
     * @param appId
     * @param appSecret
     * @return
     */
    BooleanWithMsg validate(String appId, String appSecret);
}
