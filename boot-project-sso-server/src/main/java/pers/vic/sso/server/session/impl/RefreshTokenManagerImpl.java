package pers.vic.sso.server.session.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pers.vic.sso.common.model.RefreshTokenContent;
import pers.vic.sso.server.config.SsoConfiguration;
import pers.vic.sso.server.session.RefreshTokenManager;
import pers.vic.sso.server.storage.StorageStrategy;

import javax.annotation.Resource;

/**
 *  刷新凭证管理
 * 
 * @author Joe
 */
@Component
public class RefreshTokenManagerImpl implements RefreshTokenManager {

	private Logger logger = LoggerFactory.getLogger(RefreshTokenManagerImpl.class);

	@Resource
	private StorageStrategy<RefreshTokenContent> storageStrategy;

	@Resource
	private SsoConfiguration ssoConfiguration;
	


	@Override
	public int expire() {
		return ssoConfiguration.getTimeout();
	}

	@Override
	public String cacheName() {
		return "refresh-token";
	}

	@Override
	public void storage(String refreshToken, RefreshTokenContent refreshTokenContent) {
		storageStrategy.storage(cacheName(), refreshToken, refreshTokenContent, expire());
	}

	@Override
	public RefreshTokenContent validate(String refreshToken) {
		return storageStrategy.remove(cacheName(), refreshToken,RefreshTokenContent.class);
	}
}
