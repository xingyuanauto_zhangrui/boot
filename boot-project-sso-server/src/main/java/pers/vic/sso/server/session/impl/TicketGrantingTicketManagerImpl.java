package pers.vic.sso.server.session.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pers.vic.sso.common.model.SsoUser;
import pers.vic.sso.server.config.SsoConfiguration;
import pers.vic.sso.server.session.TicketGrantingTicketManager;
import pers.vic.sso.server.storage.StorageStrategy;

import javax.annotation.Resource;

/**
 * 描述:
 *      登录凭证的管理
 * @author Vic.xu
 * @date 2021-11-01 10:07
 */
@Component
public class TicketGrantingTicketManagerImpl implements TicketGrantingTicketManager {

    //SsoUser
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private StorageStrategy<SsoUser> ssoUserStorageStrategy;

    @Resource
    private SsoConfiguration ssoConfiguration;


    @Override
    public int expire() {
        return ssoConfiguration.getTimeout();
    }

    @Override
    public String cacheName() {
        return "tgt";
    }


    @Override
    public void storage(String tgt, SsoUser user) {
        ssoUserStorageStrategy.storage(cacheName(), tgt, user, expire());
    }

    @Override
    public SsoUser getAndRefresh(String tgt) {
        return ssoUserStorageStrategy.refresh(cacheName(), tgt, expire(), SsoUser.class);
    }


    @Override
    public void remove(String tgt) {
        ssoUserStorageStrategy.remove(cacheName(), tgt, SsoUser.class);
        logger.debug("删除tgt：{}", tgt);
    }

}
