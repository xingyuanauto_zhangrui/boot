package pers.vic.sso.server.constant;

/**
 * 描述:
 *
 * @author Vic.xu
 * @date 2021-11-03 16:57
 */
public class SsoServerConstant {

    /**
     * 配置文件中配置的存储策略的key，value只支持redis和local
     */
    public static final String STORAGE_STRATEGY = "sso.storageStrategy";

    /**
     * 本地存储 默认 sso.storage.strategy=local
     */
    public static final String LOCAL_MANAGER = "local";

    /**
     * redis存储  sso.storage.strategy=redis
     */
    public static final String REDIS_MANAGER = "redis";


}
