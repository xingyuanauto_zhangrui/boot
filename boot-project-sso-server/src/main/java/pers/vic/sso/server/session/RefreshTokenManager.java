package pers.vic.sso.server.session;

import pers.vic.boot.base.tool.Tools;
import pers.vic.sso.common.model.AccessTokenContent;
import pers.vic.sso.common.model.RefreshTokenContent;

/**
 * 描述:
 *    刷新凭证refreshToken管理器接口
 * @author Vic.xu
 * @date 2021-11-02 14:34
 */
public interface RefreshTokenManager extends  Manager{
    /**
     * 生成refreshToken
     *
     * @param accessTokenContent
     * @param accessToken
     * @return
     */
    default String generate(AccessTokenContent accessTokenContent, String accessToken) {
        String refreshToken = "rt-" + Tools.randomUuid();
        storage(refreshToken, new RefreshTokenContent(accessTokenContent, accessToken));
        return refreshToken;
    }

    /**
     * 存储 refreshToken
     * @param refreshToken
     * @param refreshTokenContent
     */
    void storage(String refreshToken, RefreshTokenContent refreshTokenContent);

    /**
     * 验证refreshToken有效性，无论有效性与否，都remove掉
     * @param refreshToken
     * @return
     */
    RefreshTokenContent validate(String refreshToken);
}
