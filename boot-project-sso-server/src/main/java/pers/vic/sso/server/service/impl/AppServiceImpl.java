package pers.vic.sso.server.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pers.vic.boot.base.vo.BooleanWithMsg;
import pers.vic.sso.server.config.SsoConfiguration;
import pers.vic.sso.server.service.AppService;

import javax.annotation.Resource;

/**
 * 描述:
 *
 * @author Vic.xu
 * @date 2021-10-29 17:31
 */
@Service
public class AppServiceImpl implements AppService {

    @Resource
    private SsoConfiguration ssoConfiguration;


    /**
     *  检验appId
     */
    @Override
    public boolean checkAppId(String appId) {
        for (SsoConfiguration.AppClient client : ssoConfiguration.getClients()) {
            if (StringUtils.equals(appId, client.getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 检测appId 和对应的秘钥是否正确
     */
    @Override
    public BooleanWithMsg validate(String appId, String appSecret) {
        for (SsoConfiguration.AppClient client : ssoConfiguration.getClients()) {
            if (StringUtils.equals(appId, client.getId())) {
                if (StringUtils.equals(appSecret, client.getSecret())) {
                    return BooleanWithMsg.success();
                } else {
                    BooleanWithMsg.fail("appId对应的密码错误");
                }
            }
        }
        return BooleanWithMsg.fail("不合法的sso client");
    }
}
