package pers.vic.sso.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import pers.vic.boot.util.JsonUtil;
import pers.vic.sso.common.model.AuthorizationCode;
import pers.vic.sso.server.config.SsoConfiguration;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@Rollback(true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SsoServerTest {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Value("${sso.storageStrategy:local}")
    private String storageStrategy;

    @Resource
    private SsoConfiguration ssoConfiguration;

    @Test
    public void storageStrategy(){
        System.out.println(ssoConfiguration);
    }


    @Test
    public void  test(){
        String key = "test-123";
        AuthorizationCode code = new AuthorizationCode("1233", true,"baidu.com");
        String text = JsonUtil.toJson(code);
        stringRedisTemplate.opsForValue().set(key, text, 120, TimeUnit.SECONDS);

        String text2 = stringRedisTemplate.opsForValue().get(key);
        System.out.println(text2);
        AuthorizationCode code2 = JsonUtil.jsonToObject(text2, AuthorizationCode.class);
        System.out.println(code2);
    }

    public static void main(String[] args) {
        String text2  = "{\"tgt\":\"1233\",\"notifyLogout\":true,\"redirectUri\":\"baidu.com\"}";
        AuthorizationCode code2 = JsonUtil.jsonToObject(text2, AuthorizationCode.class);
        System.out.println(code2);
    }

}
