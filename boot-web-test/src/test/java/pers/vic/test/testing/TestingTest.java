/**
 * 
 */
package pers.vic.test.testing;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.util.JsonUtil;
import pers.vic.test.BaseTest;
import pers.vic.test.testing.model.Testing;
import pers.vic.test.testing.service.TestingService;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年9月15日下午5:04:56
 */
public class TestingTest extends BaseTest {

    @Autowired
    private TestingService testingService;

    /*
     * @Autowired private EsDocumentHelper esDocumentHelper;
     * 
     * @Autowired private EsSearchHelper esSearchHelper;
     */

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void objectList() {
        String url = "/testing/testing/objectList";
        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("name[0]", "zhangsan");
        postParameters.add("age[0]", 12);
        postParameters.add("name[1]", "lisi");
        postParameters.add("age[1]", 13);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        HttpEntity<MultiValueMap<String, Object>> r = new HttpEntity<>(postParameters, headers);
        BaseResponse<?> postForObject = restTemplate.postForObject(url, r, BaseResponse.class);
        System.out.println(postForObject.getData());

    }

    @Test
    public void object() {
        String url = "/testing/testing/object";
        Testing request = new Testing();
        request.setName("zhangsan");
        request.setAge(12);
        @SuppressWarnings("rawtypes")
        ResponseEntity<BaseResponse> postForEntity = restTemplate.postForEntity(url, request, BaseResponse.class);
        System.out.println(postForEntity.getBody());

        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("name", "zhangsan");
        postParameters.add("age", 12);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        HttpEntity<MultiValueMap<String, Object>> r = new HttpEntity<>(postParameters, headers);
        BaseResponse<?> postForObject = restTemplate.postForObject(url, r, BaseResponse.class);
        System.out.println(postForObject.getData());

    }

    @Test
    public void list() {
        testingService.list(new Testing()).forEach(JsonUtil::printJson);
    }

    /*
     * @Test public void saveToEs() { testingService.list(new Testing()).forEach(t
     * -> { EsTesting e = new EsTesting(t.getId(), t.getName(), t.getRemark()); try
     * { esDocumentHelper.save(e, "testing"); } catch (IOException e1) {
     * e1.printStackTrace(); } }); }
     * 
     * @Test public void search() throws IOException { EsLookup<EsTesting> lookup =
     * EsLookup.build("testing", EsTesting.class).keyword("张").page(1, 2)
     * .addHighlightFieldAndSetFunctionString("name", (model, name) -> {
     * model.setName(name); }).addHighlightFieldAndSetFunctionString("remark",
     * (model, name) -> { model.setName(name); }).end();
     * 
     * PageInfo<EsTesting> datas = esSearchHelper.search(lookup);
     * System.out.println(datas.getTotal()); JsonUtil.printJson(datas.getDatas()); }
     */
    public static void main(String[] args) {
        //
        try {

            // Jackson2JsonRedisSerializer s = new Jackson2JsonRedisSerializer<Unit>(Unit.class);
            Date d1 = DateUtils.parseDate("2020-12-12 11:10:08", "yyyy-mm-dd hh:MM:ss");
            Date d2 = DateUtils.parseDate("2020-12-12 11:10:08", "yyyy-mm-dd hh:MM:ss");
            int truncatedCompareTo = DateUtils.truncatedCompareTo(d1, d2, Calendar.DATE);
            System.out.println(truncatedCompareTo);
            System.out.println(DateUtils.isSameDay(d1, d2));
        } catch (Exception e) {
        }

    }

}

class Unit {
    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
