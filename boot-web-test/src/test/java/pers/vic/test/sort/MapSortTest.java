package pers.vic.test.sort;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Vic.xu
 * @date 2021/09/09
 */
public class MapSortTest {

    public static void main(String[] args) {
        test();
    }

    public static void test() {
        Map<String, String> map = new HashMap<>();
        map.put("e", "777");
        map.put("b", "123");

        map.put("a", "12343");

        map.put("d", "444");

        LinkedHashMap<String, String> map2 = new LinkedHashMap<String, String>(map);
        map.entrySet().stream().map(e -> e.getKey()).sorted().forEach(key -> {
            map2.put(key, map.get(key));
        });
        map2.forEach((k, v) -> {
            System.out.println(k + "  = " + v);
        });
    }
}
