package pers.vic.test.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 描述:
 * @author Vic.xu
 * @date 2021-12-02 10:12
 */
public class TestExecutorService {
    public static void main(String[] args) {
        ExecutorService es = Executors.newSingleThreadExecutor();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Runnable线程处理开始...");
                int a = 0;
                int b = 3;
//                System.out.println("除以0的结果为：" + b / a);
                System.out.println("Runnable线程处理结束...");
            }
        };
        es.execute(runnable);
        es.shutdown();
        System.out.println("after shutdown");

    }
}
