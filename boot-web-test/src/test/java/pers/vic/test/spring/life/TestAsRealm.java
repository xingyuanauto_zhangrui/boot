/**
 * 
 */
package pers.vic.test.spring.life;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


import pers.vic.test.BaseTest;


/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月15日下午3:59:26
 */
public class TestAsRealm extends BaseTest{

	
	@Autowired
	private AsRelm asRelm;
	
	@Test
	public void t1() {
		int id =12;
		asRelm.testCache(id);
		asRelm.testCache(id);
	}
}
