/**
 *
 */
package pers.vic.test.spring.life;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import pers.vic.test.BaseTest;

import java.util.Map;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月14日下午5:23:19
 */
public class SpringLifeTest extends BaseTest {


    @Autowired(required = false)
    private TestSpringLifeCycle testSpringLifeCycle;

    @Value("#{${test.map:{}}}")
    public Map<String, Integer> map;

    @Test
    public void map() {

        System.out.println("map");
        System.out.println(map);
    }

    @Test
    public void cache() {
        int id = 123;
        int r1 = testSpringLifeCycle.cache(id);
        System.out.println("r1 = " + r1);
        int r2 = testSpringLifeCycle.cache(id);
        System.out.println("r2 = " + r2);
    }

}
