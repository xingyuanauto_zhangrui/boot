package pers.vic.test.activiti;

import pers.vic.test.BaseTest;

/**
 * @description: 流程实例
 * @author: Vic.xu
 * @date: 2020年4月3日 下午4:36:10
 */
public class TestProcessInstanceActiviti extends BaseTest {
	/*
	 * -- 正在执行的流程实例 SELECT * FROM act_ru_execution;
	 * 
	 * -- 流程实例的历史表 SELECT * FROM act_hi_procinst; -- 执行 流程就一个 流程对象有多个
	 * 
	 * -- 正在执行的任务表：只有节点是UserTask 该表中才会存在数据 SELECT * FROM act_ru_task;
	 * 
	 * -- 任务历史表 SELECT * FROM act_hi_taskinst;
	 * 
	 * -- 所有活动节点的历史表 SELECT * FROM act_hi_actinst;
	 * 
	 */
	/*
	 * @Resource private ProcessEngine processEngine;
	 *//**
		 * 启动流程实例
		 * 
		 * @description:
		 * @author: Vic.xu
		 * @date: 2020年4月3日 下午4:39:47
		 */
	/*
	 * @Test public void start() { String key = "first"; ProcessInstance instance =
	 * processEngine.getRuntimeService() // 与正在执行的流程实例和执行对象相关的service
	 * .startProcessInstanceByKey(key); System.out.println(instance.getId());
	 * System.out.println(instance.getProcessDefinitionId());//流程定义id
	 * 
	 * }
	 * 
	 *//**
		 * 查询当前人的个人任务
		 */
	/*
	 * @Test public void findPersonalTask() { String assignee = "张三"; List<Task>
	 * list = processEngine.getTaskService() .createTaskQuery()
	 * .taskAssignee(assignee) // .taskCandidateUser(candidateUser)//组任务办理人 //排序
	 * .list(); list.forEach(t->{ System.out.println(t.getId());
	 * System.out.println(t.getName()); System.out.println(t.getCreateTime());
	 * System.out.println(t.getAssignee());
	 * System.out.println(t.getProcessInstanceId());
	 * System.out.println(t.getExecutionId());
	 * System.out.println(t.getProcessDefinitionId());
	 * System.out.println("----------"); });
	 * 
	 * }
	 * 
	 *//**
		 * 完成我的任务
		 * 
		 * @description:
		 * @author: Vic.xu
		 * @date: 2020年4月3日 下午5:15:05
		 *//*
			 * @Test public void completeTask() { String taskId = "20004";
			 * processEngine.getTaskService() .complete(taskId);
			 * 
			 * }
			 */
}
