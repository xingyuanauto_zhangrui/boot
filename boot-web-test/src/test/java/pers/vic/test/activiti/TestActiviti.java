package pers.vic.test.activiti;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description:
 * @author: Vic.xu
 * @date: 2020年4月2日 下午5:54:01
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(true)
public class TestActiviti {
	/*
	 * -- 部署对象表
		SELECT *  FROM act_re_deployment;
		-- 流程定义表
		SELECT * FROM act_re_procdef;
		-- 资源文件表
		SELECT * FROM act_ge_bytearray;
		-- 主键生成策略表
		SELECT * FROM act_ge_property;

	 */
	/**
	@Resource
	private ProcessEngineConfiguration processEngineConfiguration;

	@Resource
	private ProcessEngine processEngine;

	@Test
	public void createByBoot() {
		processEngineConfiguration.buildProcessEngine();
	}

	 * 使用代码创建工作流需要的23张表
	@Test
	public void createTable() {
		String jdbcDriver = "com.mysql.jdbc.Driver";
		String jdbcUrl = "jdbc:mysql://localhost:3306/openwebflow?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC&nullCatalogMeansCurrent=true";
		String jdbcUsername = "root";
		String jdbcPassword = "12122035xx";
		ProcessEngineConfiguration configuration = ProcessEngineConfiguration
				.createStandaloneProcessEngineConfiguration();
		configuration.setJdbcDriver(jdbcDriver);
		configuration.setJdbcUrl(jdbcUrl);
		configuration.setJdbcUsername(jdbcUsername);
		configuration.setJdbcPassword(jdbcPassword);
		configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
		ProcessEngine engine = configuration.buildProcessEngine();
		System.out.println(engine);
	}

	@Test
	public void engine() {
		System.out.println(processEngine.getName());
	}

	 * 流程定义是不能修改的TODO 控制修改：升级版本 但是在启动就是最新的流程了

	 * 部署流程定义-从classpath
	@Test
	public void deployment() {
		String resource = "diagram/first.bpmn";
		String pngResource = "diagram/first.png";
		Deployment deployment = processEngine.getRepositoryService() // 获取仓库对象
				.createDeployment().name("流程定义First").addClasspathResource(resource).addClasspathResource(pngResource)
				.deploy();
		System.out.println(deployment.getId());
		System.out.println(deployment.getName());
		/*
		 * 6) 这一步在数据库中将操作三张表： a) act_re_deployment（部署对象表） 存放流程定义的显示名和部署时间，每部署一次增加一条记录 b)
		 * act_re_procdef（流程定义表） 存放流程定义的属性信息，部署每个新的流程定义都会在这张表中增加一条记录。
		 * 注意：当流程定义的key相同的情况下，使用的是版本升级 c) act_ge_bytearray（资源文件表）
		 * 存储流程定义相关的部署信息。即流程定义文档的存放地。每部署一次就会增加两条记录，一条是关于bpmn规则文件的，一条是图片的（
		 * 如果部署时只指定了bpmn一个文件，activiti会在部署时解析bpmn文件内容自动生成流程图）。两个文件不是很大，都是以二进制形式存储在数据库中。
		 * 
	}

	 * 部署流程定义-从zip
	@Test
	public void deploymentByZip() {
		ZipInputStream zipInputStream = new ZipInputStream(
				getClass().getClassLoader().getResourceAsStream("diagram/first.zip"));
		Deployment deployment = processEngine.getRepositoryService() // 获取仓库对象
				.createDeployment().name("流程定义First").addZipInputStream(zipInputStream).deploy();
		System.out.println(deployment.getId());
		System.out.println(deployment.getName());
	}

	 * 查询流程定义
	@Test
	public void find() {
		List<ProcessDefinition> list = processEngine.getRepositoryService() // 与流程定义个部署对象相关的service
				.createProcessDefinitionQuery()
//		.deploymentId("5001")//部署id
//		.processDefinitionId("")//流程定义id
				.processDefinitionKey("first")
				// .processDefinitionNameLike("")
				// 排序
				.orderByProcessDefinitionVersion().desc()
				// 结果集
				// .singleResult();
//		.listPage(firstResult, maxResults) // 分页查询
				.list();
		print(list);

	}

	 * 查询最新版本的流程定义列表
	@Test
	public void findLastVersion() {
		List<ProcessDefinition> list = processEngine.getRepositoryService().
				createProcessDefinitionQuery()
				.latestVersion()
				.list();
		print(list);
	}

	 * 删除流程定义
	@Test
	public void deleteDeploment() {
		// 使用部署id 删除
		String deploymentId = "7501";
		// 不带级联的删除 只能删除没有启动的流程， 若启动则报错
		processEngine.getRepositoryService().deleteDeployment(deploymentId);
		// 级联删除，不管流程是否启动
		processEngine.getRepositoryService().deleteDeployment(deploymentId, true);
	}
	 * 删除流程定义：key相同的所有不同版本
	@Test
	public void deleteDeplomentByKey() {
		// 根据key找到流程list
		//遍历删除
		String key = "first";
		processEngine.getRepositoryService()
			.createProcessDefinitionQuery()
			.processDefinitionKey(key)
			.list().forEach(p->{
				String id = ((ProcessDefinition)p).getDeploymentId();
				processEngine.getRepositoryService().deleteDeployment(id, true);
			});
	}

	 * 查看流程图
	 * 
	 * @throws IOException
	@Test
	public void viewPic() throws IOException {
		String deploymentId = "2501";
		String resourceName = "";
		List<String> names = processEngine.getRepositoryService().getDeploymentResourceNames(deploymentId);
		for (String name : names) {
			if (name.endsWith(".png")) {
				resourceName = name;
				break;
			}
		}
		// 生成的图片放在文件夹下
		InputStream in = processEngine.getRepositoryService().getResourceAsStream(deploymentId, resourceName);
		File png = new File("d:", resourceName);
		FileUtils.copyInputStreamToFile(in, png);

	}

	private void print(List<ProcessDefinition> list) {
		Optional.ofNullable(list).orElse(new ArrayList<>()).forEach(d -> {
			System.out.println("-----");
			System.out.println(d.getId());// key + version + 随机数
			System.out.println(d.getName());
			System.out.println(d.getKey());
			System.out.println(d.getResourceName());
			System.out.println(d.getDiagramResourceName());
			System.out.println(d.getDeploymentId());
			System.out.println(d.getVersion());
			System.out.println("-----");
		});
		
		 * first:2:2504 
		 * firstProcess 
		 * first 
		 * diagram/first.bpmn 
		 * diagram/first.png 
		 * 2501
		 * 1
		 * 
	}
	*/
	
}
