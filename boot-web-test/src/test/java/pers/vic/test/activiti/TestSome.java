/**
 * 
 */
package pers.vic.test.activiti;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年4月30日下午5:46:56
 */
public class TestSome {

	public static void main(String[] args) {
		Test t = new Test();
		t.a();t.a();
	}
}

class Test {
	boolean a = false;
	
	public long a() {
		boolean b = !this.a || 1==2;
		System.out.println(b);
		return Thread.currentThread().getId();
	}
}
