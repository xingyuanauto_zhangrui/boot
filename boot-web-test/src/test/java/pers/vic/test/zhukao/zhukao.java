/**
 * 
 */
package pers.vic.test.zhukao;

/**
 * @description:
 * @author Vic.xu
 * @date: 2021年3月3日上午10:35:01
 */
public class zhukao {

	// 目标70分
	// 分数组成： 助考 40% + 统考 60%
	// 助考分数： 平时分 60 + 统考*0.4
	// 如果想考到目标分值: 则机考和统考的分数应该是多少

	// [(60 + 0.4x)] * 0.4 +0.6 y = target
	// 24 + 0.16x + 0.6 y = target
	public static final void print(int target) {
		System.out.println("目标: " + target);
		for (int i = 60; i <= 100; i++) {
			

			// 助考结果
			int zkScore = (int) (24 + 0.16 * i);
			// 需要的统考分数
			int part = (int) ((target - zkScore) / 0.6);
			System.out.print("[机：" + i + " 总助：" + zkScore + "  统:" + part + "]\t");
			if ((i+1) % 5 == 0) {
				System.out.println();
			}
			
		}
		System.out.println("");
	}

	public static final void print(int target, int b) {

	}

	public static void main(String[] args) {
		print(60);
		print(70);
	}

}
