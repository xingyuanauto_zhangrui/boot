package pers.vic.test.encode;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author Vic.xu
 * @date 2021/09/07
 */
public class Base64Test {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String source = "hxz#20210906$";

        Base64.Encoder encoder = Base64.getEncoder();
        String s1 = encoder.encodeToString(source.getBytes());
        String s2 = encoder.encodeToString(source.getBytes("GBK"));
        String s3 = encoder.encodeToString(source.getBytes("utf-8"));
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

    }

}
