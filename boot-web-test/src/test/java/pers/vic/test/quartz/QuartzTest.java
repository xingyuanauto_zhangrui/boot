/**
 * 
 */
package pers.vic.test.quartz;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import pers.vic.boot.util.JsonUtil;
import pers.vic.test.BaseTest;
import pers.vic.test.quartz.job.FirstJob;
import pers.vic.test.quartz.model.QuartzCustomerJob;
import pers.vic.test.quartz.service.QuartzCustomerJobService;
import pers.vic.test.quartz.util.QuartzManager;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月28日下午5:10:10
 */
public class QuartzTest extends BaseTest{

	
	@Resource
	private QuartzManager quartzManager;
	
	@Autowired
	private QuartzCustomerJobService quartzCustomerJobService;
	
	@Test
	public void test2() {
		quartzCustomerJobService.list(new QuartzCustomerJob()).forEach(JsonUtil::printJson);
		
	}
	
	@Test
	public void test() {
		QuartzCustomerJob info = new QuartzCustomerJob();
		info.setCode("jdbcFirst");
		info.setGroup("firstGroup");
		info.setClazz(FirstJob.class.getName());
		String cron = "0/20 * * * * ?";
		info.setCron(cron);
		
		quartzManager.addJob(info);
		
	}
}
