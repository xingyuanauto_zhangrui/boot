/**
 * 
 */
package pers.vic.test.delay;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;

import pers.vic.test.BaseTest;
import pers.vic.test.redis.redisson.RedisDelayedQueue;
import pers.vic.test.redis.redisson.TaskBodyDTO;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2021年4月14日下午3:43:37
 */
public class RedisDelayedQueueTest extends BaseTest{


	@Resource
	private RedisDelayedQueue redisDelayedQueue;
	
	@Test
	public void test() {
		 //添加需要延迟的DTO
		System.out.println(123456);
        TaskBodyDTO taskBody = new TaskBodyDTO();
        taskBody.setBody("测试DTO实体类的BODY,3秒之后执行");
        taskBody.setName("测试DTO实体类的姓名,3秒之后执行");
        //添加队列3秒之后执行
        redisDelayedQueue.addQueue(taskBody, 3, TimeUnit.SECONDS);
        taskBody.setBody("测试DTO实体类的BODY,10秒之后执行");
        taskBody.setName("测试DTO实体类的姓名,10秒之后执行");
        //添加队列10秒之后执行
        redisDelayedQueue.addQueue(taskBody, 10, TimeUnit.SECONDS);
        taskBody.setBody("测试DTO实体类的BODY,20秒之后执行");
        taskBody.setName("测试DTO实体类的姓名,20秒之后执行");
        //添加队列20秒之后执行
        redisDelayedQueue.addQueue(taskBody, 20, TimeUnit.SECONDS);
        try {
			TimeUnit.SECONDS.sleep(20);
			System.out.println(654321);
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
}
