package pers.vic.test;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@Rollback(true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseTest {

	public static void main(String[] args) {
		for (int i = 0,s=0; i < 5; i++) {
			s += ++i;
			System.out.print(i);		
		}
		System.out.println();
		
		char[] c = new char[5];
		System.out.println(c.length);
		int x = 53;
		System.out.println(1.0 + x/2);
		double d = 0.0d/0.0d ;
		System.out.println(d);
		System.out.println(Double.isNaN(d));
		
		System.out.println((Float.POSITIVE_INFINITY / 0));
		
		Set<String> set = new HashSet<String>();
		set = new LinkedHashSet<String>();
		set.add("b1111");
		set.add("a2222");
		set.add("4444");
		set.add("3333");
		System.out.println(set);
		Date d1 = new Date();
		
		Date d2 = DateUtils.addMonths(d1, 22);
		Calendar c1 = Calendar.getInstance();
		c1.setTime(d1);
		
		Calendar c2 = Calendar.getInstance();
		c2.setTime(d2);
		String f = "yyyy-MM-dd";
		System.out.println(DateFormatUtils.format(c1, f));
		System.out.println(DateFormatUtils.format(c2, f));
		int a = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);
		int year = c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR);
		System.out.println(year*12 + a);
	}
}
