/**
 * 
 */
package pers.vic.test.url;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 *  @description:  配置上这个会导致/generator >..... ServletRequestWrapper.isAsyncStart...死循环 
 *  @author Vic.xu
 *  @date: 2020年5月22日下午2:05:37
 */
//@Configuration
public class CustomerUrlMartchConfig extends WebMvcConfigurationSupport{

	@Override
	protected RequestMappingHandlerMapping createRequestMappingHandlerMapping() {
		Set<String> names = new HashSet<String>();
		names.add("test");
		names.add("test1");
		return new CustomerRequestMappingHandlerMapping(names);
	}
	
}
