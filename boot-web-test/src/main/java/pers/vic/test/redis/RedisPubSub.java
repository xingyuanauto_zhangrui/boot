/**
 * 
 */
package pers.vic.test.redis;

import redis.clients.jedis.JedisPubSub;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月28日上午8:53:53
 */
public class RedisPubSub extends JedisPubSub{

	@Override
	public void onMessage(String channel, String message) {
		System.out.println("channel: " + channel );
		System.out.println("message: " + message );
	}
	
	

}
