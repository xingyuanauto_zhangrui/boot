/**
 *
 */
package pers.vic.test.redis;

import pers.vic.boot.util.redis.JedisUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年9月28日上午8:56:10
 */
public class Sub {

    public static String channel = "test:one";

    public static String channel2 = "test:two";

    public static String[] channels = new String[]{
            channel, channel2

    };

    public static void main(String[] args) {
        System.out.println("sub............start");
        Jedis jedis = JedisUtil.getResource();
        jedis.subscribe(new JedisPubSub() {

            @Override
            public void onMessage(String channel, String message) {
                System.out.println("channel: " + channel);
                System.out.println("message: " + message);
            }

        }, channels);

        System.out.println("sub............end");
    }
}
