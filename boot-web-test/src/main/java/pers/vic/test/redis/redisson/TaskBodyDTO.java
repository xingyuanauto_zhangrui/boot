/**
 * 
 */
package pers.vic.test.redis.redisson;

import java.io.Serializable;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2021年4月14日下午3:45:13
 */
public class TaskBodyDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	String body;
	
	
	String name;


	public String getBody() {
		return body;
	}


	public void setBody(String body) {
		this.body = body;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	

}
