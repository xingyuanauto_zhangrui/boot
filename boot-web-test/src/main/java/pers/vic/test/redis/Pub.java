/**
 *
 */
package pers.vic.test.redis;

import pers.vic.boot.util.redis.JedisUtil;
import redis.clients.jedis.Jedis;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年9月28日上午9:19:18
 */
public class Pub {

    public static void main(String[] args) throws InterruptedException {
        Jedis jedis = JedisUtil.getResource();
        String message = "我是消息啊";


        for (int i = 0; i < 5; i++) {

            TimeUnit.SECONDS.sleep(3);
            jedis.publish(Sub.channel, message + i);
            System.out.println("Sub.channel sended" + i);
        }
        System.out.println("...................");
        for (int i = 0; i < 5; i++) {
            TimeUnit.SECONDS.sleep(3);
            jedis.publish(Sub.channel2, message + LocalTime.now());
            System.out.println("Sub.channe2 sended" + i);
        }
    }
}
