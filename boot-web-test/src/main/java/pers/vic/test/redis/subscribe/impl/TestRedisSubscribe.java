/**
 *
 */
package pers.vic.test.redis.subscribe.impl;

import org.springframework.stereotype.Service;
import pers.vic.test.redis.subscribe.BaseRedisSubscribe;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年9月28日上午10:50:08
 */
@Service
public class TestRedisSubscribe extends BaseRedisSubscribe {

    @Override
    public String channelname() {
        return "config";
    }


    @Override
    public void onMessage(String message) {
        System.out.println("TestRedisSubscribe   on msg   :" + message);
    }

}
