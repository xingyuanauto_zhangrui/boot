/**
 *
 */
package pers.vic.test.redis;

import pers.vic.boot.util.redis.JedisUtil;
import pers.vic.boot.util.thread.CustomThreadFactory;
import redis.clients.jedis.Jedis;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.concurrent.*;

/**
 * @description: 模拟 测试jedis 资源耗尽
 * @author Vic.xu
 * @date: 2020年11月30日上午9:44:33
 */
public class TestJedisResource {

    public static void main(String[] args) throws Exception {
        Operation.print();
        Field field = Operation.class.getDeclaredField("a");
        field.setAccessible(true);
        field.set(null, "lisi");
        Operation.print();


        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                return;
//				break;
            }
            int seq = i;
            new Thread(() -> {
                System.out.println("will getResource " + seq);
                try {
                    Jedis resource = JedisUtil.getResource();
                    System.out.println("getResourced " + seq);
                    Thread.sleep(5001);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

        }
        Jedis resource = JedisUtil.getResource();
        Jedis resource2 = JedisUtil.getResource();
        System.out.println("will  getResource ");
        try {
            Jedis resource3 = JedisUtil.getResource();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("end0...................1");
        System.out.println("restart ");
        JedisUtil.init();
        System.out.println("will  getResource 4");
        Jedis resource4 = JedisUtil.getResource();
        System.out.println(" getResourced 4");
//		test(20);
    }

    public static void test(int nums) throws InterruptedException {

        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService executorService = new ThreadPoolExecutor(nums, nums,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(10), new CustomThreadFactory("BatchExecteOperation"));
        ;
        for (int i = 0; i < nums; i++) {
            final int seq = i;
            executorService.submit(() -> {
                try {
                    latch.await();
                    Operation.maybeDoSth(seq);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        latch.countDown();
//		executorService.shutdown();
    }

}

class Operation {

    public static void maybeDoSth(int seq) {
        try {
            int timeMillis = ThreadLocalRandom.current().ints(5000, 8000).distinct().limit(1).findFirst().getAsInt();
            String seqStr = "第[" + seq + "]件事情";
            String desc = MessageFormat.format("将开始做{0}，它将耗时[{1}]毫秒.........", seqStr, timeMillis);
            System.out.println(desc);
            String end = seqStr + "做完了";
            Jedis jedis = JedisUtil.getResource();
            System.out.println(seq + "  getResourced");
            jedis.set("do-" + seq, seq + "");
            jedis.expire("do-" + seq, 120);
            Thread.sleep(timeMillis);
            System.out.println(end);
            jedis.expire("do-" + seq, 120);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String a = "zhangsan";

    public static void print() {
        System.out.println(a);
    }
}
