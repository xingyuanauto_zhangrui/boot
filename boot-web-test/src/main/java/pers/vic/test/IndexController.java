package pers.vic.test;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vic.xu
 * @date 2021/08/23
 */
@Controller
@RequestMapping
public class IndexController {

    static {
//        JSON.DEFAULT_GENERATE_FEATURE |= SerializerFeature.DisableCircularReferenceDetect.getMask();
    }

    @RequestMapping("/login")
    public String login(String username, String password) {

        return "";
    }

    public static void main(String[] args) {
        Map<String, Object> a = new HashMap<String, Object>();
        a.put("a", "1");

        Map<String, Object> b = new HashMap<String, Object>();
        b.put("b", "2");

        Map<String, Object> c = new HashMap<String, Object>();
        c.put("c", "3");

        a.put("b", b);
        b.put("c", c);
        c.put("a", a);
        String s = JSONObject.toJSONString(a);
        System.out.println(s);
    }

}
