/**
 * 
 */
package pers.vic.test.svn;

import pers.vic.boot.util.poi.export.ExcelField;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年6月11日上午11:21:53
 */
public class SvnlogModel {
	
	//提交的id
	@ExcelField(sort = 1, title = "id")
	public String id;
	
	//作者
	@ExcelField(sort = 2, title = "作者")
	public String author;
	
	//日期字符串
	@ExcelField(sort = 3, title = "日期")
	public String date;
	
	//提交的信息
	@ExcelField(sort = 4, title = "信息")
	public String message;
	
	//jira 号
	@ExcelField(sort = 5, title = "jira")
	public String jira;
	
	//涉及到哪些文件
	@ExcelField(sort = 6, title = "文件")
	public String files;
	
	
	public void addMsg(String msg) {
		if(this.message != null) {
			this.message = this.message + msg + " \n";
		}else {
			this.message = msg + " \n";
		}
		
	}
	
	public void addFile(String file) {
		if(this.files != null) {
			this.files = this.files + file + " \n";
		}else {
			this.files = file + " \n";
		}
	}
	

	@Override
	public String toString() {
		System.out.println("---------------------");
		return "SvnlogModel [id=" + id + ", author=" + author + ", date=" + date + ", message=" + message + ", jira="
				+ jira + ", files=" + files + "]";
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the jira
	 */
	public String getJira() {
		return jira;
	}

	/**
	 * @param jira the jira to set
	 */
	public void setJira(String jira) {
		this.jira = jira;
	}

	/**
	 * @return the files
	 */
	public String getFiles() {
		return files;
	}

	/**
	 * @param files the files to set
	 */
	public void setFiles(String files) {
		this.files = files;
	}
	
	
	
	
	

}
