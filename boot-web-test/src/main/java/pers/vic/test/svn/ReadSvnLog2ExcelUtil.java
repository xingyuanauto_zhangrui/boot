/**
 * 
 */
package pers.vic.test.svn;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import pers.vic.boot.util.RegexUtil;
import pers.vic.boot.util.poi.export.ExportExcel;

/**
 *  @description: 从TXT格式的SVN提交信息中读取到指定的信息
 *  @author Vic.xu
 *  @date: 2020年6月11日上午11:13:56
 */
public class ReadSvnLog2ExcelUtil {
	
	//D:\desk
	
	/*
	 每一次提交的数据格式类似:
	    Revision: 108346
		Author: huxiaolin
		Date: 2020年6月9日 17:21:26
		Message:
		ICBCSTL-43601
		----
		Modified : /sourcecode/branches/gongyin/gongyin_202006/src/com/kjlink/icbcstl/report/service/ReportService.java
		Modified : /sourcecode/branches/gongyin/gongyin_202006/web/js/report/intenalReport.js
		Modified : /sourcecode/branches/gongyin/gongyin_202006/web/pages/report/intenalReport/ifSatisfyrequireReport.html
		Added : /sourcecode/branches/gongyin/gongyin_202006/web/pages/report/intenalReport/scalePositionReport.html 
	
	 分析: 
	 1. 开始判断:Revision  从中可以提取到提交id
	 2. 作者 提取:  Author
	 3. 提交日期提取:  Date
	 4 . 提交日志 Message  并可以从中提取到jira号
	 5. 改变的文件: ---- 之后的
	 
	 */
	
	// id 行开始标志
	private static String ID_START = "Revision:";
	
	//作者行开始标志
	private static String AUTHOR_START = "Author:";
	
	//日期行开始标志
	private static String DATE_START = "Date:";
	
	//message行开始标志
	private static String MESSAGE_START = "Message:";
	
	//修改的文件行开始标志
	private static String FILES_START = "----";
	
	//提取JIRA号的正则
	private static String JIRA_REGEX = "([A-Z]{4,8}-[0-9]{4,8})";
	
	// test export
	public static void main(String[] args) throws FileNotFoundException, IOException {
		String file = "D:/desk/logs.txt";
		String outFile = "D:/desk/1.xlsx";
		List<SvnlogModel> list = readSvnlog(file);
		export(list, outFile);
	}
	
	public static void export(List<SvnlogModel> list, String fileName) throws FileNotFoundException, IOException {
		ExportExcel e = new ExportExcel("svnlog", SvnlogModel.class);
		e.setDataList(list);
		e.writeFile(fileName);
	}
	
	public static List<SvnlogModel> readSvnlog(String file) {
		List<SvnlogModel> list = new ArrayList<SvnlogModel>();
	    SvnlogModel model = null;
	    //是否开始读取message
	    boolean msg = false;
	    //是否开始读取文件
	    boolean files = false;
		try(Stream<String> stream = Files.lines(Paths.get(file))) {
			List<String> lines = stream.collect(Collectors.toList());
			for(String line : lines) {
				if(StringUtils.isEmpty(line)) {
					continue;
				}
				if(line.startsWith(ID_START)) {
					if(model != null) {
						//提取jira
						extractJira(model);
						list.add(model);
					}
					msg = false;
					files = false;
					model = new SvnlogModel();
					model.setId(line.substring(ID_START.length()));
					continue;
				}
				if(line.startsWith(AUTHOR_START)) {
					model.setAuthor(line.substring(AUTHOR_START.length()));
					continue;
				}
				if(line.startsWith(DATE_START)) {
					model.setDate(line.substring(DATE_START.length()));
					continue;
				}
				//读取到msg行
				if(line.startsWith(MESSAGE_START)) {
					msg = true;
					files = false;
					continue;
				}
				
				
				if(line.startsWith(FILES_START)) {
					files = true;
					msg = false;
					continue;
				}
				
				// 两个方在最后 防止数据为空时候的误判断
				if(msg) {
					model.addMsg(line);
					continue;
				}
				if(files) {
					model.addFile(line);
					continue;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 根据msg提取jira号
	 * @param model
	 */
	public static void extractJira(SvnlogModel model) {
		if(model != null && StringUtils.isNotEmpty(model.getMessage())) {
			String msg = model.getMessage();
			String jira = RegexUtil.getFirstString(msg, JIRA_REGEX, 1);
			model.setJira(jira);
			
		}
	}
}
