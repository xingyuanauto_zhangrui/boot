/**
 * 
 */
package pers.vic.test.temp;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月29日上午9:28:58
 */
/**
 * Copyright 2020 bejson.com 
 */
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
* Auto-generated: 2020-09-29 9:27:26
*
* @author bejson.com (i@bejson.com)
* @website http://www.bejson.com/java2pojo/
*/
public class WorkDaily {

	/**
	 * 日志类型
	 */
   private String orderType;
   //工作日期
   private long workDt;
   // 提交日志
   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
   @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
   private Date submitDt;
   //工作时间 分钟
   private int workMin;
   //备注
   private String remark;
   // my name
   private String userName;
   // 所在项目
   private String pro;
   
   private String userCode;
   private String deptname;
   private int ROW_ID;
   private String applyId;
   private String projectNo;
   private String auditStatus;
   private String id;
   private String userType;
   //项目名
   private String projectName;
   private String auditStatusName;
   private String placeName;
   private String baseName;
   //工作内容
   private String workContents;
   private String locationPath;
   public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    public String getOrderType() {
        return orderType;
    }

   public void setWorkDt(long workDt) {
        this.workDt = workDt;
    }
    public long getWorkDt() {
        return workDt;
    }

   public void setSubmitDt(Date submitDt) {
        this.submitDt = submitDt;
    }
    public Date getSubmitDt() {
        return submitDt;
    }

   public void setWorkMin(int workMin) {
        this.workMin = workMin;
    }
    public int getWorkMin() {
        return workMin;
    }

   public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getRemark() {
        return remark;
    }

   public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserName() {
        return userName;
    }

   public void setPro(String pro) {
        this.pro = pro;
    }
    public String getPro() {
        return pro;
    }

   public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    public String getUserCode() {
        return userCode;
    }

   public void setDeptname(String deptname) {
        this.deptname = deptname;
    }
    public String getDeptname() {
        return deptname;
    }

   public void setROW_ID(int ROW_ID) {
        this.ROW_ID = ROW_ID;
    }
    public int getROW_ID() {
        return ROW_ID;
    }

   public void setApplyId(String applyId) {
        this.applyId = applyId;
    }
    public String getApplyId() {
        return applyId;
    }

   public void setProjectNo(String projectNo) {
        this.projectNo = projectNo;
    }
    public String getProjectNo() {
        return projectNo;
    }

   public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
    public String getAuditStatus() {
        return auditStatus;
    }

   public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

   public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getUserType() {
        return userType;
    }

   public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    public String getProjectName() {
        return projectName;
    }

   public void setAuditStatusName(String auditStatusName) {
        this.auditStatusName = auditStatusName;
    }
    public String getAuditStatusName() {
        return auditStatusName;
    }

   public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
    public String getPlaceName() {
        return placeName;
    }

   public void setBaseName(String baseName) {
        this.baseName = baseName;
    }
    public String getBaseName() {
        return baseName;
    }

   public void setWorkContents(String workContents) {
        this.workContents = workContents;
    }
    public String getWorkContents() {
        return workContents;
    }

   public void setLocationPath(String locationPath) {
        this.locationPath = locationPath;
    }
    public String getLocationPath() {
        return locationPath;
    }

}
