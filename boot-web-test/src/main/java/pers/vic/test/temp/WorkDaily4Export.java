/**
 * 
 */
package pers.vic.test.temp;

import org.apache.commons.lang3.time.DateFormatUtils;

import pers.vic.boot.util.poi.export.ExcelField;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月29日上午10:04:23
 */
public class WorkDaily4Export {
	// 时间  类型 项目  内容  时长
	
	@ExcelField(title = "日期")
	public String day;
	@ExcelField(title = "类型")
	public String type;
	@ExcelField(title = "项目编号")
	public String projectNo;
	@ExcelField(title = "项目名")
	public String projectName;
	@ExcelField(title = "时长")
	public int duration;
	@ExcelField(title = "内容")
	public String context;
	
	public WorkDaily4Export(WorkDaily d) {
		this.day = DateFormatUtils.format(d.getWorkDt(), "yyyy-MM-dd");
		this.type = d.getOrderType();
		this.projectNo = d.getProjectNo();
		this.projectName = d.getProjectName();
		this.duration = d.getWorkMin();
		this.context = d.getWorkContents();
	
	}

	public WorkDaily4Export() {
		super();
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(String projectNo) {
		this.projectNo = projectNo;
	}
	
	
	
	
	

}
