/**
 * 
 */
package pers.vic.test.temp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.core.type.TypeReference;

import pers.vic.boot.base.craw.BaseCrawl;
import pers.vic.boot.util.JsonUtil;
import pers.vic.boot.util.poi.export.ExportExcel;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月29日上午9:20:23
 */
public class Temp extends BaseCrawl{

	@Override
	protected int getTimeout() {
		return 50000;
	}

	
	public static void main(String[] args) throws IOException {
		parse();
		// 时间  类型 项目  内容  时长
	}
	
	public static void parse() throws IOException {
		String file = "D:/desk/datas.txt";
		String text = FileUtils.readFileToString(new File(file), "UTF-8");
		System.out.println(text);
		
		List<WorkDaily> list = JsonUtil.jsonToObject(text, new TypeReference<ArrayList<WorkDaily>>() {
		});
		list = list.stream().sorted(Comparator.comparing(WorkDaily::getWorkDt)).collect(Collectors.toList());
		System.out.println(list.size());
		List<WorkDaily4Export> list2 = list.stream().map(WorkDaily4Export::new).collect(Collectors.toList());
		System.out.println(list2.size());
		ExportExcel excel = new ExportExcel("工作日志",WorkDaily4Export.class);
		excel.setDataList(list2);
		excel.writeFile("D:/desk/datas.xlsx").dispose();
		System.out.println("");
	}
	
	public static void fetchData() throws IOException {
		Temp t = new Temp();
		String url = "http://oa.kjlink.com/KjlinkPmsBackend/tbGrProjectDayDetai/selectUserProjectDay?current=1&pageSize=1000&startDt=&endDt=&orderType=&auditStatus=ALL&projectNo=&userType=ALL";
		String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0aGlzIGlzIHRlc3QgdG9rZW4iLCJhdWQiOiJBUFAiLCJuYmYiOjE2MDEzMzgzMjEsImxvZ2luTmFtZSI6IntcInVzZXJJZFwiOlwiMjAxMDg4XCIsXCJ1c2VyTmFtZVwiOlwi6K6456eL5YasXCJ9IiwiaXNzIjoiU0VSVklDRSIsImV4cCI6MTYwMTM0OTEyMSwiaWF0IjoxNjAxMzM4MzIxfQ.4n2A34nGarInxWi7561TW-jXZwAN3hw4hVKsqRbLppY";
		String text = t.con(url).header("Token", token).getBodyText();
		System.out.println(text);
	}
}
