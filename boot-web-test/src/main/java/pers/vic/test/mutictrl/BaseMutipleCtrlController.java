/**
 * 
 */
package pers.vic.test.mutictrl;

import org.springframework.web.bind.annotation.GetMapping;

import pers.vic.boot.base.model.BaseResponse;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月13日下午4:27:47
 */
public class BaseMutipleCtrlController {

	@GetMapping("/t1")
	public  BaseResponse<?> t1() {
		return BaseResponse.success(11);
	}
	@GetMapping("/t2")
	public  BaseResponse<?> t2() {
		return BaseResponse.success(11);
	}
	
}
