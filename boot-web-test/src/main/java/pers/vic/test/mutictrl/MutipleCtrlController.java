/**
 *
 */
package pers.vic.test.mutictrl;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.model.BaseResponse;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月13日下午4:27:47
 */
@RestController
@RequestMapping("tests")
public class MutipleCtrlController extends BaseMutipleCtrlController {

    @Override
    @GetMapping("/t3")
    public BaseResponse<?> t1() {
        return BaseResponse.success(3);
    }

    @Override
    @GetMapping("/t4")
    public BaseResponse<?> t2() {
        return BaseResponse.success(44);
    }

}
