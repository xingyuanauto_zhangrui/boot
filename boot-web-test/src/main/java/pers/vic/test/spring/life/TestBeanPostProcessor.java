/**
 * 
 */
package pers.vic.test.spring.life;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月12日下午4:30:14
 */
public class TestBeanPostProcessor implements BeanPostProcessor,PriorityOrdered {

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if("aaatestSpringLifeCycle".equals(beanName)) {
			System.out.println("postProcessBeforeInitialization===="+beanName);
			TestSpringLifeCycle t = (TestSpringLifeCycle) bean;
			t.some();		}	
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if("aaatestSpringLifeCycle".equals(beanName)) {
			System.out.println("postProcessAfterInitialization===="+beanName);
		}
		return bean;
	}

	@Override
	public int getOrder() {
		return LOWEST_PRECEDENCE;
	}
	
}
