/**
 * 
 */
package pers.vic.test.spring.life;

import org.springframework.context.annotation.Bean;

/**
 *  @description: 测试生命周期的 config
 *  {@link https://blog.csdn.net/varyall/article/details/82257202}
 *  @author Vic.xu
 *  @date: 2020年8月12日下午4:35:48
 */
//@Configuration
public class TestSpringConfig {

	@Bean(name ="aaatestSpringLifeCycle", initMethod = "initBean", destroyMethod = "destroyBean")
	public TestSpringLifeCycle testSpringLifeCycle() {
		return new TestSpringLifeCycle();
	}
	@Bean
	public TestBeanPostProcessor testBeanPostProcessor() {
		return new TestBeanPostProcessor();
	}
	
	@Bean
	public MinePostProcessor minePostProcessor() {
		return new MinePostProcessor();
	}
	
	@Bean
	public AsRelm asRelm() {
		return new AsRelm();
	}
}
