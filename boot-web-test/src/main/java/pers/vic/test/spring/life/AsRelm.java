/**
 * 
 */
package pers.vic.test.spring.life;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月15日上午9:39:56
 */
public class AsRelm extends AuthorizingRealm  {

	@Autowired
	MineCacheService mineCacheService;
	
	
	
	public void testCache(int id) {
		System.out.println("开始调用缓存方法  id = " + id);
		int r = mineCacheService.cache(id);
		System.out.println("cache 返回结果为   " + r);
		
	}
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return null;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		return null;
	}
}
