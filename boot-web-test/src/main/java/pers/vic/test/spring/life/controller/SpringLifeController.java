/**
 * 
 */
package pers.vic.test.spring.life.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.test.spring.life.TestSpringLifeCycle;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月15日上午10:37:34
 */
//@RestController
//@RequestMapping("/springlife")
public class SpringLifeController {

	
	@Autowired
	private TestSpringLifeCycle testSpringLifeCycle;
	
	
	@GetMapping("/t1")
	public BaseResponse<?> t1(int id) {
		int data = testSpringLifeCycle.cache(id);
		int data2 = testSpringLifeCycle.cache(id);
		return BaseResponse.success(data + " --- " +  data2);
	}
}
