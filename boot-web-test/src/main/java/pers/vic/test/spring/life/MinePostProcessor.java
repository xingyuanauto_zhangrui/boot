/**
 * 
 */
package pers.vic.test.spring.life;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月12日下午4:30:14
 */
public class MinePostProcessor implements BeanPostProcessor,PriorityOrdered {
	
	public int order = 1;
	

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if("asRelm".equals(beanName)) {
			System.err.println("asRelm .........init in postProcessBefore");
			
			AsRelm t = (AsRelm) bean;
			t.init();
		}	
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if("asRelm".equals(beanName)) {
		}
		return bean;
	}
	
	public MinePostProcessor() {
        this(LOWEST_PRECEDENCE);
    }

	/**
	 * @param lowestPrecedence
	 */
	public MinePostProcessor(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return LOWEST_PRECEDENCE;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}
	
	
	
}
