/**
 * 
 */
package pers.vic.test.spring.life;

import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.cache.annotation.Cacheable;

/**
 * @description: 测试加载顺序
 * @author Vic.xu
 * @date: 2020年8月12日下午4:20:51
 */
public class TestSpringLifeCycle implements InitializingBean, DisposableBean , SmartInitializingSingleton{
	
	public  void some() {
		System.out.println("todo something");
	}
	
	/*
	 *执行顺序如 方法注释
	  1. 构造方法
	  	1.1 postProcessBeforeInitialization
	  2. @PostConstruct
	  3. InitializingBean#afterPropertiesSet
	  4. 指定的init函数
	  	4.1 postProcessAfterInitialization
	  	
	  5. SmartInitializingSingleton#afterSingletonsInstantiated	
	  6. DisposableBean#destroy
	  7  指定的销毁方法
	 */
	//001
	public TestSpringLifeCycle() {
		print("进入构造函数");
	}
	//002
	@PostConstruct
	public void post() {
		print("@PostConstruct");
	}
	//003
	@Override
	public void afterPropertiesSet() throws Exception {
		print("InitializingBean#afterPropertiesSet");
	}
	//004
	public void initBean() {
		print("init");
	}
	
	//005
	@Override
	public void afterSingletonsInstantiated() {
		print("SmartInitializingSingleton afterSingletonsInstantiated....");
		
	}
	//006
	@Override
	public void destroy() throws Exception {
		print("DisposableBean#destroy");

	}
	//007	
	public void destroyBean() {
		print("destroyBean");
	}

	public void print(String msg) {
		System.out.println("TestSpringLifeCycle................" + msg);
	}
	
	/******************************************************/
	
	@Cacheable(value = "testpost", key = "#id")
	public int cache(int id) {
		int result = id   + new Random().nextInt(20);
		System.err.println("into cache  inner  :" + result);
		return result;
		
	}
	
	
	
}
