/**
 * 
 */
package pers.vic.test.spring.life;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月15日上午10:19:49
 */
@Service
public class MineCacheService {
	
	@Cacheable(value = "minecache", key="#id")
	public int cache(int id) {
		System.err.println("进入了cache内部");
		return id + 10;
		
	}

}
