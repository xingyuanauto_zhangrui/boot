/**
 * 
 */
package pers.vic.test.reg;

import pers.vic.boot.util.RegexUtil;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2021年1月15日上午11:43:18
 */
public class RegTest {
	
	public static void main(String[] args) {
		String str = "<td class='123'>asd\t\r\n123</td>";
		String reg = "(<td .*?>)([\\s\\S]*?)(</td>)";
		System.out.println(str.matches(reg));
		String s = str.replaceAll(reg, "$1@@@@@@$2@@@@$3");
		
		String ss = RegexUtil.replacementAllhanderString(s, "@@@@@@(.*?)@@@@", src->{
			System.out.println();
			return src.replaceAll("\r|\n|\t", "哈");
		}) ;
		
		
		System.out.println("s = " + s);
		System.out.println("ss = " + ss);
		test();
	}
	
	public static void test() {
		String s = "中英文mix在一起";
		s = s.replaceAll("((?<=[^\\x00-\\xff])[a-zA-Z]+)|([a-zA-Z]+(?=[^\\x00-\\xff]))", " $0 ");
		System.out.println(s);
	}

}
