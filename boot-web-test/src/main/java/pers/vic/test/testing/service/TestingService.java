package pers.vic.test.testing.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.base.service.BaseService;
import pers.vic.test.testing.mapper.TestingMapper;
import pers.vic.test.testing.model.Testing;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;

/**
 * @description: Service
 * @author Vic.xu
 * @date: 2020-09-15 16:53
 */
@Service
public class TestingService extends BaseService<TestingMapper, Testing>{
	
	
	@Resource
	private TestingService self;

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    
    
    public PageInfo<Testing> page2(Lookup lookup) throws InterruptedException {
    	System.out.println(Thread.currentThread().getId() + " into  page2.....");
		startPage(lookup.getPage(), lookup.getSize());
		List<Testing> datas = mapper.list(lookup);
		Thread.sleep(20010);
		System.out.println(Thread.currentThread().getId() + " left  page2.....");
		return PageInfo.instance(datas);
	}
    
    public PageInfo<Testing> page3(Lookup lookup) throws InterruptedException {
    	System.out.println(Thread.currentThread().getId() + " into  page3.....");
		startPage(lookup.getPage(), lookup.getSize());
		List<Testing> datas = mapper.list(lookup);
		Thread.sleep(23020);
		System.out.println(Thread.currentThread().getId() + " left  page3.....");
		return PageInfo.instance(datas);
    }
    
    public PageInfo<Testing> page4(Lookup lookup) throws InterruptedException {
    	System.out.println(Thread.currentThread().getId() + " into  page4.....");
		startPage(lookup.getPage(), lookup.getSize());
		List<Testing> datas = mapper.list(lookup);
		Thread.sleep(30030);
		System.out.println(Thread.currentThread().getId() + " left  page4.....");
		return PageInfo.instance(datas);
	}
    
    public void mult(Lookup lookup) throws InterruptedException {
    	Random r  = new Random();
    	for (int i = 0; i < 100; i++) {
    		Thread.sleep(1000);
    		int value = r.nextInt(4);
    		if(value == 1) {
    			
    			new Thread(()->{
    				try {
						self.page2(lookup);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    			}) .start();
    		}else if(value == 2) {
    			new Thread(()->{
    				try {
						self.page3(lookup);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    			}) .start();
    		}else if(value == 3) {new Thread(()->{
				try {
					self.page4(lookup);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}) .start(); 
    		}else {
    			new Thread(()->{
    				try {
						self.page2(lookup);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    			}) .start();
    		}
    		
    		
    	}
    }
	    
}
