package pers.vic.test.testing.model;

import pers.vic.boot.base.model.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * 实体类
 * 
 * @author Vic.xu
 */
public class Testing extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * name
	 */
	private String name;

	/**
	 * remark
	 */
	private String remark;

	/**
	 * age
	 */
	private Integer age;
	
	@NumberFormat(pattern="###,###.###",style = NumberFormat.Style.NUMBER)
	private double money;

	/**
	 * birthday
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;

	/***************** set|get start **************************************/
	/**
	 * set：name
	 */
	public Testing setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：name
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：remark
	 */
	public Testing setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * set：age
	 */
	public Testing setAge(Integer age) {
		this.age = age;
		return this;
	}

	/**
	 * get：age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * set：birthday
	 */
	public Testing setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	/**
	 * get：birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}
	
	
	/***************** set|get end **************************************/
}
