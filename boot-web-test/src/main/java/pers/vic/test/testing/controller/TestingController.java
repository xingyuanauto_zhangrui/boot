package pers.vic.test.testing.controller;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.test.testing.service.TestingService;
import pers.vic.test.testing.model.Select2;
import pers.vic.test.testing.model.Testing;

/**
 * @description: 控制层
 * @author Vic.xu
 * @date: 2020-09-15 16:53
 */
@Controller
@RequestMapping(value = "/testing/testing")
public class TestingController extends BaseController<TestingService, Testing>{
	
	
	@GetMapping("/select2")
	@ResponseBody
	@CrossOrigin
	public BaseResponse<?> select2(String prefix) {
		List<Select2> list = Select2.mock(prefix);
		if(StringUtils.isBlank(prefix)) {
			list.clear();
		}
		return BaseResponse.success(list);
	}


	@PostMapping("/object")
	@ResponseBody
	public BaseResponse<Testing> object(Testing test) {
		return BaseResponse.success(test);
	}
	
	@PostMapping("/objectList")
	@ResponseBody
	@CrossOrigin
	public BaseResponse<?> objectList(@RequestBody ArrayList<Testing> tests) {
		return BaseResponse.success(tests);
	}
	
	@GetMapping("/home")
	@ResponseBody
	public BaseResponse<Object> home(String name){
		
		return BaseResponse.success(name + "  " +LocalTime.now());
	}
	
	@GetMapping("/forward")
	public String forword(String name, HttpServletResponse response,HttpServletRequest request) throws IOException{
		return "forward:/testing/testing/home";
	}
	
	@CrossOrigin
	@RequestMapping("/seg")
	public void seg(HttpServletResponse response, HttpServletRequest request) throws IOException{
		System.out.println("123");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().print("<h2>" +LocalTime.now() + "</h2>");
	
	}
	@CrossOrigin
	@RequestMapping("/redirect2")
	public String toSeg(HttpServletResponse response, HttpServletRequest request) throws IOException{
		return "redirect:seg";
	}
	
	@GetMapping("/redirect")
	public String redirect(String name){
		
		return "redirect:/testing/testing/home?name=" + "redirect" + name;
	}
	

}
