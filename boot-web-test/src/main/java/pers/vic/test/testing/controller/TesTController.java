package pers.vic.test.testing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.test.testing.model.Testing;
import pers.vic.test.testing.service.TestingService;

/**
 * @description: 控制层
 * @author Vic.xu
 * @date: 2020-09-15 16:53
 */
@RestController
@RequestMapping(value = "/test/testing")
public class TesTController extends BaseController<TestingService, Testing> {

    /**
     * 列表
     * 
     * @return
     */
    @GetMapping(value = "list")
    @Override
    public BaseResponse<?> list(Testing lookup) {
        PageInfo<Testing> list = service.page(lookup);
        return BaseResponse.success(list);
    }

    @GetMapping(value = "number")
    public BaseResponse<?> number(Testing lookup) {
        return BaseResponse.success(lookup);
    }

    @PostMapping(value = "upload")
    @CrossOrigin
    public BaseResponse<?> upload(@RequestParam("file") MultipartFile multipartFile,
        HttpServletRequest servletRequest) {

        if (servletRequest instanceof MultipartHttpServletRequest) {
            System.out.println("instanceof");
            MultipartHttpServletRequest httpServletRequest = (MultipartHttpServletRequest)servletRequest;
            MultiValueMap<String, MultipartFile> multiFileMap = httpServletRequest.getMultiFileMap();
            System.out.println(multiFileMap.size());
        } else {
            System.out.println("not  MultipartHttpServletRequest");
        }

        return BaseResponse.success(multipartFile.getOriginalFilename());
    }

}
