/**
 * 
 */
package pers.vic.test.testing.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import pers.vic.boot.util.JsonUtil;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2021年3月16日上午11:16:44
 */
public class Select2 {
	
	private int id;
	
	private String text;
	
	
	public static List<Select2> mock(String prefix){
		List<Select2> list = new ArrayList<Select2>();
		int count = RandomUtils.nextInt(5, 10);
		while(count>0) {
			list.add(new Select2(count, prefix +  RandomStringUtils.random(5, 0x4e00, 0x9fa5, false,false)));
			count--;
		}
			
		
		return list;
	}
	
	
	public static void main(String[] args) {
		JsonUtil.printJson(mock("123"));
		String aa = String.format("%sasdasd%s", "111","2222");
		System.out.println(aa);
	}
	
	public Select2() {
		super();
	}

	/**
	 * @param id
	 * @param text
	 */
	public Select2(int id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
}
