package pers.vic.test.testing.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.test.testing.model.Testing;
/**
 * @description: Mapper
 * @author Vic.xu
 * @date: 2020-09-15 16:53
 */
public interface TestingMapper extends BaseMapper<Testing> {
	
}
