/**
 * 
 */
package pers.vic.test.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import lombok.Data;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年5月27日上午9:13:32
 */
public class TestJSON {
	public static void main(String[] args) {
		Person person = new Person();
		person.name = "blue";
		person.length = 18;

		String s = JSONObject.toJSONString(person);
		String s1 = JSONObject.toJSONString(person, SerializerFeature.WriteClassName);

		System.out.println(s);
		System.out.println(s1);

		Object parse = JSON.parse(s);
		Object parse1 = JSON.parse(s1);

		System.out.println("type:" + parse.getClass().getName() + " " + parse);
		System.out.println("type:" + parse1.getClass().getName() + " " + parse1);
	}

}

@Data
class Person {
	String name;
	int length;
	
}
