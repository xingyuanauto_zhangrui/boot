/**
 * 
 */
package pers.vic.test.json.jackson;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import pers.vic.boot.util.JsonUtil;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月14日下午2:13:58
 */
public class TestJackson {

	
	public static void main(String[] args) throws Exception {
		Model model = getModel();
		ObjectMapper mapper = new ObjectMapper();
	//	mapper =new CustomerObjectMapper();
		String json = mapper.writeValueAsString(model);
		System.out.println(json);
		
		
		Model model2 = JsonUtil.jsonToObject(json, Model.class);
		JsonUtil.printJson(model2);
		
		
	}
	
	
	public static Model getModel() {
		Model model = new Model();
		model.setId(123);
		long l = System.currentTimeMillis();
		model.setDate(new java.sql.Date(l));
		model.setTime(new java.sql.Timestamp(l));
		return model;
	}
}

class Model{
	int id;
	
	Date date;
	
	Date time;
	
	@JsonFormat(pattern = "MM:dd HH:mm")
	Date normal = new Date();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getNormal() {
		return normal;
	}

	public void setNormal(Date normal) {
		this.normal = normal;
	}
	
	
	
	
	
	
}
