/**
 * 
 */
package pers.vic.test.json.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年9月14日下午2:13:02
 */
public class CustomerObjectMapper extends ObjectMapper {

	private static final long serialVersionUID = 1L;

	public CustomerObjectMapper() {
		super();
		this.setDateFormat(new CustomerDateFormat());
		this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

}
