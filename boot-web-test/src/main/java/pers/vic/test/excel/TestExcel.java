/**
 *
 */
package pers.vic.test.excel;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Vic.xu
 *
 */
public class TestExcel {

    public static void main(String[] args) throws IOException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFCellStyle style = workbook.createCellStyle();

        HSSFSheet sheet = workbook.createSheet("sheet");

        HSSFRow row0 = sheet.createRow(0);
        HSSFCell cell00 = row0.createCell(0);
        cell00.setCellStyle(style);
        cell00.setCellValue("日期");
        HSSFCell cell01 = row0.createCell(1);
        cell01.setCellStyle(style);
        cell01.setCellValue("午别");

        HSSFRow row1 = sheet.createRow(1);
        HSSFCell cell10 = row1.createCell(0);
        cell10.setCellStyle(style);
        cell10.setCellValue("20180412");
        HSSFCell cell11 = row1.createCell(1);
        cell11.setCellStyle(style);
        cell11.setCellValue("上午");

        HSSFRow row2 = sheet.createRow(2);
        HSSFCell cell21 = row2.createCell(1);
        cell21.setCellStyle(style);
        cell21.setCellValue("下午");

        // 合并日期占两行(4个参数，分别为起始行，结束行，起始列，结束列)
        // 行和列都是从0开始计数，且起始结束都会合并
        // 这里是合并excel中日期的两行为一行
        CellRangeAddress region = new CellRangeAddress(1, 2, 0, 0);
        sheet.addMergedRegion(region);

        File file = new File("d:/desk/demo.xls");
        FileOutputStream fout = new FileOutputStream(file);
        workbook.write(fout);
        fout.close();
    }

}