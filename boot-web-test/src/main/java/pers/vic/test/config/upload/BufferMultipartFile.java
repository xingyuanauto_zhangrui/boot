/**
 * 
 */
package pers.vic.test.config.upload;

import org.apache.commons.fileupload.FileItem;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月24日上午10:29:02
 */
public class BufferMultipartFile extends CommonsMultipartFile{
	
	/**
	 * @param fileItem
	 */
	public BufferMultipartFile(FileItem fileItem) {
		super(fileItem);
	}

	private static final long serialVersionUID = 1L;

}
