/**
 * 
 */
package pers.vic.test.config.redis;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年11月16日上午10:39:45
 */
@Configuration
public class RedissonConfig {
	@Autowired
	private Environment env;

	@Bean
	public RedissonClient redissonClient() {
		Config config = new Config();
		String address = "redis://" + env.getProperty("spring.redis.host") + ":" + env.getProperty("spring.redis.port");
		config.useSingleServer().setAddress(address)
				.setPassword(env.getProperty("spring.redis.password"))
				.setDatabase(Integer.parseInt(env.getProperty("spring.redis.database")));
		RedissonClient client = Redisson.create(config);
		return client;
	}

}
