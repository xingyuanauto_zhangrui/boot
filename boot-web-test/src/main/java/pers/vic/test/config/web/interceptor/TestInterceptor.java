package pers.vic.test.config.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pers.vic.boot.base.tool.Tools;

/**
 * @author Vic.xu
 * @date 2021/08/11
 */
public class TestInterceptor extends HandlerInterceptorAdapter {

    public static String LAST_URL = "last_request_get_url";

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
        ModelAndView modelAndView) throws Exception {
        System.out.println(Tools.getRequestUrl(request));
        if (request instanceof MultipartHttpServletRequest) {
            System.out.println("instanceof+++++++++++++++++++++++++++++++++");
            MultipartHttpServletRequest httpServletRequest = (MultipartHttpServletRequest)request;
            MultiValueMap<String, MultipartFile> multiFileMap = httpServletRequest.getMultiFileMap();
            System.out.println(multiFileMap.size());
        } else {
            System.out.println("not  MultipartHttpServletRequest");
        }

        super.postHandle(request, response, handler, modelAndView);
    }

}