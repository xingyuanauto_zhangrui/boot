/**
 * 
 */
package pers.vic.test.config.upload;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *  @description: 文件上传   ，修改StandardServletMultipartResolver 为CommonsMultipartResolver
 *  @see  org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration
 *  @author Vic.xu
 *  @date: 2020年8月24日上午8:41:50
 */
@Configuration
public class FileUploadConfig {
	
	//显示声明CommonsMultipartResolver为mutipartResolver
    @Bean(name = DispatcherServlet.MULTIPART_RESOLVER_BEAN_NAME)
    public MultipartResolver multipartResolver() {
//        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
    	MineFileResolver resolver = new MineFileResolver();
        resolver.setDefaultEncoding("UTF-8");
        //resolveLazily属性启用是为了推迟文件解析，以在在UploadAction中捕获文件大小异常
        resolver.setResolveLazily(false);
        try {
			resolver.setUploadTemp("d:/temp");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        resolver.setMaxInMemorySize(1);
        //上传文件大小 5M 5*1024*1024
        resolver.setMaxUploadSize(5 * 1024 * 1024);
        return resolver;
    }


}
