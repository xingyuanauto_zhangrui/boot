/**
 * 
 */
package pers.vic.test.config.spel;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月18日下午5:40:35
 */
@Configuration
@ConditionalOnExpression("${attachment.type:1}==2")
public class SpringElConfig {
	
	@Bean
	@ConditionalOnExpression("${attachment.type:1}==1")
	public Object objet1() {
		System.out.println("SpringElConfig:1111111111111111111111");
		return new Object();
	}
	
	@Bean
	@ConditionalOnExpression("!(${attachment.type:1}==1)")
	public Object objet2() {
		System.out.println("SpringElConfig:22222222222222222222");
		return new Object();
	}
	
	@Configuration
	static class TestInner{
		
		@Bean
		@ConditionalOnExpression("${attachment.type:1}==2 && ${attachment.enable}")
		public Object objet3() {
			System.out.println("SpringElConfig:TestInner  3333333");
			return new Object();
		}
	}

}
