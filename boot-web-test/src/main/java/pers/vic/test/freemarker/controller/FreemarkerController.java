/**
 * 
 */
package pers.vic.test.freemarker.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Vic.xu
 *
 */
@Controller
@RequestMapping("/freemarker")
public class FreemarkerController {

	
	@GetMapping("/home")
	public String htmlParse(Model model) {
		List<String> interestList = new ArrayList<String>();
		interestList.add("书法");
		interestList.add("诗歌");
		interestList.add("dota");
		model.addAttribute("interestList", interestList);
		model.addAttribute("name", "张三");
		model.addAttribute("title", "&lt;h2 &gt;&lt;span style=\\&quot;color:red\\&quot;&gt;title&lt;/span&gt;&lt;/h2&gt;");
		return "home";
	}
}
