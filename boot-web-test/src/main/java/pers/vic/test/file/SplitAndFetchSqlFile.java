/**
 *
 */
package pers.vic.test.file;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import pers.vic.boot.util.RegexUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月17日下午5:08:52
 */
public class SplitAndFetchSqlFile {


    //需要提取的sql类型
    public static Set<String> fetch_Sql = new HashSet<>(Arrays.asList("insert,update,delete".split(",")));
    // 提取sql字符串的  前缀：  fetch_Sql_suffix + insert|update|delete
    static String fetch_Sql_suffix = ")]";

    //  截取的时间的前缀 被包含  ：根据需要修改
    static String time_split_start = "2020-08-16";
    //截取的时间的后缀  不包含
    static String time_split_end = "[http-nio-8001-exec-";
    /**
     * 暂时没用正则提取
     */
    @Deprecated
    static String time_reg = "(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})";

    public static void main(String[] args) {
        String fileName = "D:/desk/stl-sql_c.log";

        String resultFile = "D:/desk/result.log";
        test();

        fetch(fileName, resultFile);


    }

    public static void test() {
        String line = "INFO  : 3E2663F8F861393A0C48652A868DBD3E[lbxy01] : 2020-08-16 16:45:30[http-nio-8001-exec-3][p6spy.logSQL(62)]update t_stl_s_provisional_rent set enable_flag=true, last_update_date='16-Aug-20', last_update_user_id='lbxy01', capital='5059.55', capital_vat='0.00', instalment_number='36', interest='47.74', interest_vat='2.86', original_happened_date=NULL, remained_capital='0.00', rent='5110.15', happened_date='25-Aug-23', interest_receivable='50.60', rent_receivable='5110.15' where id='ff80808173e65a8e0173f551e78e0af0'";
        line = "INFO  : D92E7630DFD8CF1F2CC0D82BBD05797E[yb01] : 2020-08-16 16:17:13[http-nio-8001-exec-5][p6spy.logSQL(62)]delete from t_stl_s_md_attachment where material_document_id='ff808081739e2bda0173c7a31a6c24d9'";
        System.out.println(RegexUtil.getFirstString(line, time_reg, 1));
        System.out.println(getTime(line));
        System.out.println(getSql(line));

    }

    public static String getTime(String str) {
        if (StringUtils.isBlank(str)) {
            return "";
        }
        int start = str.indexOf(time_split_start);
        int end = str.indexOf(time_split_end);
        if (start > -1 && end > -1) {
            return str.substring(start, end);
        }
        return "";
    }

    public static String getSql(String line) {
        if (StringUtils.isBlank(line)) {
            return "";
        }
        for (String sqlType : fetch_Sql) {
            int startIndex = line.indexOf(fetch_Sql_suffix + sqlType);
            if (startIndex > -1) {
                return line.substring(startIndex + 2);
            }
        }

        return "";
    }


    static Map<String, String> map = new LinkedHashMap<>();
    static List<String> lines = new ArrayList<String>();
    static int count = 0;

    public static void fetch(String fileName, String resultFile) {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(line -> {
                String sql = getSql(line);
                if (StringUtils.isNotEmpty(sql)) {
                    String time = getTime(line);
                    System.out.println(time);
                    System.out.println(sql);
                    count++;
                    lines.add(time + "\t" + sql + "\n");
                }
                /*
                 */
            });
            File file = new File(resultFile);
            FileUtils.writeLines(file, lines, false);
            FileUtils.write(file, "total: " + count, true);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}

/**
 * 暂时没用数据类型
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月17日下午6:02:55
 */
class Sql {
    String time;

    String type;

    String sql;

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the sql
     */
    public String getSql() {
        return sql;
    }

    /**
     * @param sql the sql to set
     */
    public void setSql(String sql) {
        this.sql = sql;
    }


}
