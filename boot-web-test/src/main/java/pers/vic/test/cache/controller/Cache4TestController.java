/**
 * 
 */
package pers.vic.test.cache.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.test.cache.service.Cache4TestService;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月27日上午8:36:16
 */
@RestController
@RequestMapping("/test")
public class Cache4TestController {

	@Autowired
	private Cache4TestService cache4TestService;
	
	
	@GetMapping("/cache1/{id}")
	public BaseResponse<?> cache1(@PathVariable int id) {
		String id1 = cache4TestService.cache1(id);
		String id2 = cache4TestService.cache1(id);
		return BaseResponse.success(id1 + "   " + id2);
	}
	
	@GetMapping("/cache2/{id}")
	public BaseResponse<?> cache2(@PathVariable int id) {
		String id1 = cache4TestService.cache2(id);
		String id2 = cache4TestService.cache2(id);
		return BaseResponse.success(id1 + "   " + id2);
	}
}
