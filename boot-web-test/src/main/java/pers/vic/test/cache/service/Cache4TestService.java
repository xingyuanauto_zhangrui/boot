/**
 *
 */
package pers.vic.test.cache.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Random;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月27日上午8:33:05
 */
@Service
public class Cache4TestService {

    @Cacheable(value = "cache1", key = "#id")
    public String cache1(int id) {
        System.err.println("into cache1  " + id);
        return id + "  " + new Random().nextInt(10);
    }

    @Cacheable(value = "cache2", key = "#id")
    public String cache2(int id) {
        System.err.println("into cache1  " + id);
        return id + "  " + new Random().nextInt(10);
    }

    @CacheEvict(value = "cache2", key = "#id")
    public void evict(int id) {

    }

    public static void main(String[] args) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("a", "123");
        map.add("a", "234");
        List<String> list = map.get("a");
        System.out.println(list);
    }

}
