/**
 *
 */
package pers.vic.test.http;

import pers.vic.boot.base.craw.BaseCrawl;
import pers.vic.boot.base.craw.CrawlConnect;

import java.io.IOException;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年11月2日下午1:31:35
 */
public class ChangeIp extends BaseCrawl {

    @Override
    protected int getTimeout() {
        return 20000;
    }

    /*
     * Allow from 218.22.63.14
Allow from 61.50.118.146
Allow from 106.37.216.18
Allow from 82.205
Allow from 86.117.175
     */
    public static void main(String[] args) throws IOException {
        String url = "http://localhost:81/lcxm/lcxm_login.html";
        url = "http://127.0.0.1:10083/test/mybatis/other";
        url = "https://cplease.icbcleasing.com/cpleaseMobile/icbcstl/monitoring";
        ChangeIp c = new ChangeIp();
        CrawlConnect con = c.con(url);
        con.header("X-Forwarded-For", "82.205.216.12");
        con.validateTlsCertificates();
        String text = con.getHtml();
        System.out.println(text);

    }

}
