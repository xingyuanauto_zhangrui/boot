package pers.vic.test.http.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import pers.vic.boot.base.model.BaseResponse;

/**
 * @author Vic.xu
 * @date 2021/07/29
 */
public class HttpClientTest {

    public static void main(String[] args) throws IOException, Exception {
        test();
    }

    /**
     * 无参get
     * 
     * @throws Exception
     * @throws IOException
     */
    public static void test() throws Exception, IOException {
        String url = "https://baidu.com";
        HttpClient client = HttpClients.createDefault();

        HttpGet get = new HttpGet(url);

        HttpResponse httpResponse = client.execute(get);

        HttpEntity entity = httpResponse.getEntity();
        String string = EntityUtils.toString(entity, "UTF-8");
        System.out.println("-------------------");
        System.out.println(string);
        System.out.println("-------------------");
    }

    /**
     * 有参get
     * 
     * @throws Exception
     * @throws IOException
     */
    public static void test2() throws Exception, IOException {
        String url = "https://baidu.com";
        HttpClient client = HttpClients.createDefault();
        // 带参数的
        URIBuilder builder = new URIBuilder(url);
        builder.addParameter("wd", "xuqiudong");
        // builder.addParameters(List <NameValuePair> nvps);
        URI uri = builder.build();
        HttpResponse httpResponse2 = client.execute(new HttpGet(uri));
        HttpEntity entity2 = httpResponse2.getEntity();
        String string2 = EntityUtils.toString(entity2, "UTF-8");
        System.out.println("-------------------");
        System.out.println(string2);
        System.out.println("-------------------");
    }

    /**
     * post
     * 
     * @throws UnsupportedEncodingException
     */
    public static void testPost() throws Exception {
        HttpClient client = HttpClients.createDefault();
        String url = "";
        HttpPost post = new HttpPost(url);

        // 1 请求头传参 和get一致 URIBuilder builder = new URIBuilder(url);
        // 2 请求体传参
        StringEntity entity = new StringEntity("name=123");
        entity = new StringEntity("{}", "application/json");
        entity.setContentEncoding("UTF-8");
        post.setEntity(entity);
        String string = EntityUtils.toString(client.execute(post).getEntity(), "UTF-8");

        System.out.println(string);

        //
        ObjectMapper objectMapper = new ObjectMapper();
        JavaType constructParametricType =
            objectMapper.getTypeFactory().constructParametricType(List.class, BaseResponse.class);
        BaseResponse readValue = objectMapper.readValue("", constructParametricType);
    }

}
