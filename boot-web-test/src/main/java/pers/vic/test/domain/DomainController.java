/**
 * 
 */
package pers.vic.test.domain;

import java.time.LocalTime;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.model.BaseResponse;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年12月30日下午5:35:22
 */

@RestController
@RequestMapping("/test/domain")
public class DomainController {

	/*
	 * https://www.iteye.com/topic/1000776
	 */
	@RequestMapping(value = { "", "/" })
	public BaseResponse<String> domain(HttpServletRequest request, HttpServletResponse response) {
		
		Cookie cookie = new Cookie("vic-xu", "123456789");
		cookie.setDomain("baidu.com");
		cookie.setPath("/");
		response.addCookie(cookie);
		/*
		 * Cookie cookie2 = new Cookie("vic-xu2", "987654321");
		 * cookie.setDomain("127.0.0.1"); response.addCookie(cookie2);
		 */
		return BaseResponse.success(LocalTime.now() + "");
	}
}
