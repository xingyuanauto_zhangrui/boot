/**
 * 
 */
package pers.vic.test.condition;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
//@PropertySource("classpath:datasource-config.properties")
//@ImportResource(locations = {"classpath:spring-mybatis.xml", "classpath:spring-redis.xml"})
//@Conditional(value = { null })
//ConditionalOnProperty
@Profile(value = "redis")
public class ConditionTestConfig2 {

	@PostConstruct
	public void post() {
		System.out.println("ConditionTestConfig2...........redis.....................");
	}
}
