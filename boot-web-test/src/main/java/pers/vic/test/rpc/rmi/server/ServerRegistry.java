package pers.vic.test.rpc.rmi.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import pers.vic.test.rpc.rmi.FirstInterface;

/**
 * LocateRegistry 在创建的时候 会自动启动一个子线程， 并升级为守护线程
 * 
 * @author Vic.xu
 * @date 2021/08/02
 */
public class ServerRegistry {

    // 注册服务
    public static void main(String[] args) {

        try {
            FirstInterface firstInterface = new FirstRMIImpl();
            LocateRegistry.createRegistry(9999);
            // 绑定一个服务到注册中心 若重复 则抛出异常
            Naming.bind("rmi://localhost:9999/first", firstInterface);

            Naming.rebind("rmi://localhost:9999/first", firstInterface);
            System.out.println("服务器启动完毕");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
