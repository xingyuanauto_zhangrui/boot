package pers.vic.test.rpc.rmi.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.joda.time.LocalTime;

import pers.vic.test.rpc.rmi.FirstInterface;

/**
 * 实现远程服务接口 必须是Remote直接或间接的实现类 如果不会创建基于RMI的服务标准实现 可以继承UnicastRemoteObject
 * 
 * @author Vic.xu
 * @date 2021/08/02
 */
public class FirstRMIImpl extends UnicastRemoteObject implements FirstInterface {
    private static final long serialVersionUID = 1L;

    protected FirstRMIImpl() throws RemoteException {
        super();
    }

    @Override
    public String first(String name) throws RemoteException {
        System.out.println("客户端请求参数是：" + name);
        return name + " " + LocalTime.now();
    }

}
