package pers.vic.test.rpc.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 定义一个远程服务接口 rmi强制要求继承Remote
 * 
 * @author Vic.xu
 * @date 2021/08/02
 */
public interface FirstInterface extends Remote {

    /**
     * 所有远程服务方法必须抛出RemoteException
     * 
     * @return
     * @throws RemoteException
     */
    String first(String name) throws RemoteException;

}
