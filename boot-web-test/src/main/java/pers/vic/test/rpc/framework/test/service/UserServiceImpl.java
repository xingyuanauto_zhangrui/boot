package pers.vic.test.rpc.framework.test.service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.joda.time.LocalTime;

/**
 * @author Vic.xu
 * @date 2021/08/10
 */
public class UserServiceImpl extends UnicastRemoteObject implements UserService {

    private static final long serialVersionUID = 1L;

    /**
     * @throws RemoteException
     */
    public UserServiceImpl() throws RemoteException {
        super();
    }

    @Override
    public String getUser(String name) throws RemoteException {
        return name + " " + LocalTime.now();
    }

}
