package pers.vic.test.rpc.zk;

import java.io.IOException;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

/**
 * @author Vic.xu
 * @date 2021/08/03
 */
public class TestZookeeper {

    public static void main(String[] args) throws Exception {
        // create();
        // list();

        delete();
    }

    /**
     * java连接zk： <br />
     * 1. 创建客户端 <br />
     * 2 使用客户端发送命令 <br />
     * 3. 返回处理结果 <br />
     * 4 关闭连接
     * 
     * @throws IOException
     * @throws InterruptedException
     * @throws KeeperException
     */
    public static void create() throws IOException, KeeperException, InterruptedException {
        // 1 创建客户端对象
        ZooKeeper keeper = new ZooKeeper("localhost:2181", 20000, new Watcher() {

            @Override
            public void process(WatchedEvent event) {
                System.out.println("WatchedEvent 执行");
            }
        });
        // 创建一个节点
        String path = "/parent";
        byte[] data = "parent data".getBytes();
        // 第三个参数： 权限
        // 第四个参数：节点的类型
        String create = keeper.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

        System.out.println(create);
        // 创建一个临时节点
        String create2 = keeper.create("/parent/temp", data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
        System.out.println(create2);

        // 创建一个带序号的节点
        String create3 =
            keeper.create("/parent/sequence", data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT_SEQUENTIAL);
        System.out.println(create3);
        Thread.sleep(10000);
        keeper.close();
    }

    /**
     * 查询节点相当于遍历key
     * 
     * @throws Exception
     */
    public static void list() throws Exception {
        // 1 创建客户端对象
        ZooKeeper keeper = new ZooKeeper("localhost:2181", 20000, new Watcher() {

            @Override
            public void process(WatchedEvent event) {
                System.out.println("WatchedEvent 执行");
            }
        });
        List<String> children = keeper.getChildren("/", true);
        children.forEach(System.out::println);
        System.out.println("-------------------------");
        for (String path : children) {
            List<String> childrens = keeper.getChildren("/" + path, true);
            byte[] data = keeper.getData("/" + path, false, null);
            if (data != null) {
                System.out.println("data:--------------------------------" + new String(data));
            }

            childrens.forEach(System.out::println);
        }
        // 查询数据

        keeper.close();
    }

    /**
     * 删除节点 前 需要先查询节点的状态（cversion）
     */
    public static void delete() throws Exception {
        ZooKeeper keeper = new ZooKeeper("localhost:2181", 20000, new Watcher() {

            @Override
            public void process(WatchedEvent event) {
                System.out.println("WatchedEvent 执行");
            }
        });
        String path = "/first/node1";
        Stat stat = new Stat();
        keeper.getData(path, false, stat);
        // stat.getCversion() 是data version
        int version = stat.getCversion();
        System.out.println("version: " + version);
        // version 用于校验删除 delete -v 10 node
        keeper.delete(path, version);
        keeper.close();
    }

}
