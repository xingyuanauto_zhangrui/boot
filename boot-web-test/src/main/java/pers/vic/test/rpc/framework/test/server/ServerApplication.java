package pers.vic.test.rpc.framework.test.server;

import pers.vic.test.rpc.framework.RpcFactory;
import pers.vic.test.rpc.framework.test.service.UserService;
import pers.vic.test.rpc.framework.test.service.UserServiceImpl;

/**
 * 服务端启动
 * 
 * @author Vic.xu
 * @date 2021/08/11
 */
public class ServerApplication {

    public static void main(String[] args) throws Exception {
        UserService userService = new UserServiceImpl();
        RpcFactory.registerService(UserService.class, userService);
        System.out.println(8080);

    }

}
