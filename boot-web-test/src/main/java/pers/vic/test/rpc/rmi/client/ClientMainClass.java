package pers.vic.test.rpc.rmi.client;

import java.rmi.Naming;

import pers.vic.test.rpc.rmi.FirstInterface;

/**
 * @author Vic.xu
 * @date 2021/08/02
 */
public class ClientMainClass {
    public static void main(String[] args) throws InterruptedException {
        FirstInterface firstInterface;
        try {

            firstInterface = (FirstInterface)Naming.lookup("rmi://localhost:9999/first");
            System.out.println(firstInterface.first("123"));
            System.out.println(firstInterface.getClass().getName());
        } catch (Exception e) {
            // TODO: handle exception
        }
        Thread.sleep(10);
    }

}
