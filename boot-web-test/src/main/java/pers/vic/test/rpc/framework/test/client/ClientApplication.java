package pers.vic.test.rpc.framework.test.client;

import java.io.IOException;
import java.rmi.NotBoundException;

import org.apache.zookeeper.KeeperException;

import pers.vic.test.rpc.framework.RpcFactory;
import pers.vic.test.rpc.framework.test.service.UserService;

/**
 * 测试自定义RPC框架的客户端开发
 * 
 * @author Vic.xu
 * @date 2021/08/13
 */
public class ClientApplication {

    public static void main(String[] args)
        throws KeeperException, InterruptedException, IOException, NotBoundException {
        // 通过自定义框架 连接zk ， 获取接口的代理对象
        UserService serviceProxy = RpcFactory.getServiceProxy(UserService.class);
        String user = serviceProxy.getUser("who are you");
        System.out.println(user);
    }
}
