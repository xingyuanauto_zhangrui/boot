package pers.vic.test.rpc.framework;

import java.io.IOException;

import org.apache.zookeeper.ZooKeeper;

/**
 * 专门提供ZooKeeper连接
 * 
 * @author Vic.xu
 * @date 2021/08/09
 */
public class ZkConnection {

    // zk 地址：ip:port
    private String zkServer;

    // 保存会话超时
    private int sessionTimeout;

    private ZooKeeper keeper;

    /**
     * 
     */
    public ZkConnection() {
        this("localhost:2181", 10000);
    }

    /**
     * @param zkServer
     * @param sessionTimeout
     */
    public ZkConnection(String zkServer, int sessionTimeout) {
        super();
        this.zkServer = zkServer;
        this.sessionTimeout = sessionTimeout;
    }

    public ZooKeeper getConnction() throws IOException {
        if (keeper == null) {
            keeper = new ZooKeeper(zkServer, sessionTimeout, null);
        }
        return keeper;

    }

}
