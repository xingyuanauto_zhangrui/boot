package pers.vic.test.rpc.framework.test.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 定义一个服务接口
 * 
 * @author Vic.xu
 * @date 2021/08/10
 */
public interface UserService extends Remote {

    String getUser(String name) throws RemoteException;
}
