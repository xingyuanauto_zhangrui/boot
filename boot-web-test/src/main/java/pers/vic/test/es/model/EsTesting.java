package pers.vic.test.es.model;

import java.util.Date;

import pers.vic.elasticsearch.model.EsBaseModel;
import pers.vic.test.testing.model.Testing;

/**
 * EsTesting 
 * @see Testing
 *
 */
public class EsTesting extends EsBaseModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


    private String name;


    private String remark;
    
    private Date updateTime;
    


	/**
	 * @param id
	 * @param name
	 * @param remark
	 */
	public EsTesting(Object id, String name, String remark) {
		super();
		this.id = String.valueOf(id);
		this.name = name;
		this.remark = remark;
		this.updateTime = new Date();
	}


	/**
	 * 
	 */
	public EsTesting() {
		super();
	}






	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Date getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
    
    

}
