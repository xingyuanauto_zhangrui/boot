package pers.vic.test.attachment.modle;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月29日 下午1:46:20
 */
public class FileModel {
	
	private String name;
	
	private long size;
	
	
	

	public FileModel(String name, long size) {
		super();
		this.name = name;
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public long getSize() {
		return size/1024;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSize(long size) {
		this.size = size;
	}
	
	

}
