package pers.vic.test.quartz.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.test.quartz.mapper.QuartzCustomerJobMapper;  
import pers.vic.test.quartz.model.QuartzCustomerJob;

/**
 * @description: Service
 * @author Vic.xu
 * @date: 2020-08-28 16:57
 */
@Service
public class QuartzCustomerJobService extends BaseService<QuartzCustomerJobMapper, QuartzCustomerJob>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }
    

}
