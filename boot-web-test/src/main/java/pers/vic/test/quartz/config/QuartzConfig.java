/**
 * 
 */
package pers.vic.test.quartz.config;

import java.time.LocalTime;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;

import pers.vic.test.quartz.job.FirstJob;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月28日上午10:55:07
 */
//@Configuration
public class QuartzConfig {
	
	@Scheduled(cron = "0/15 * * * * ?")
	public void testScheduled() {
		System.out.println("testScheduled   "      + LocalTime.now());
	}
	
    @Bean
    public JobDetail jobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(FirstJob.class)
                .withIdentity("first", "firstGroup")
                .storeDurably()
                .build();
        return jobDetail;
    }

    @Bean
    public Trigger trigger() {
        Trigger trigger = TriggerBuilder.newTrigger()
                .forJob(jobDetail())
                .withIdentity("first", "firstGroup")
                .usingJobData("msg", "Hello Quartz")//关联键值对
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("5/30 * * * * ?"))
                .build();
        return trigger;
    }
    
}