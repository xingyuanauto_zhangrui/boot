package pers.vic.test.quartz.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.test.quartz.model.QuartzCustomerJob;
/**
 * @description: Mapper
 * @author Vic.xu
 * @date: 2020-08-28 16:57
 */
public interface QuartzCustomerJobMapper extends BaseMapper<QuartzCustomerJob> {
	
}
