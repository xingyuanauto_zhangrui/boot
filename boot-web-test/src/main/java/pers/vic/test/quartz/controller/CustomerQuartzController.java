/**
 * 
 */
package pers.vic.test.quartz.controller;

import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.test.quartz.job.FirstJob;
import pers.vic.test.quartz.model.QuartzCustomerJob;
import pers.vic.test.quartz.util.QuartzManager;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月28日下午5:19:00
 */
@RestController
@RequestMapping("/quartz")
public class CustomerQuartzController {

	@Autowired
	private QuartzManager quartzManager;
	@GetMapping("/add")
	public String add() {
		QuartzCustomerJob info = new QuartzCustomerJob();
		info.setCode("jdbcFirst");
		info.setGroup("firstGroup");
		info.setClazz(FirstJob.class.getName());
		String cron = "0/20 * * * * ?";
		info.setCron(cron);
		
		quartzManager.addJob(info);
		return "ok " + LocalTime.now();
	}
	
	@GetMapping("/pause")
	public String pause() {
		QuartzCustomerJob info = new QuartzCustomerJob();
		info.setCode("jdbcFirst");
		info.setGroup("firstGroup");
		info.setClazz(FirstJob.class.getName());
		String cron = "0/20 * * * * ?";
		info.setCron(cron);
		
		quartzManager.pauseJob(info);
		return "ok " + LocalTime.now();
	}
	
	@GetMapping("/resume")
	public String resume() {
		QuartzCustomerJob info = new QuartzCustomerJob();
		info.setCode("jdbcFirst");
		info.setGroup("firstGroup");
		info.setClazz(FirstJob.class.getName());
		String cron = "0/20 * * * * ?";
		info.setCron(cron);
		
		quartzManager.resumeJob(info);
		return "ok " + LocalTime.now();
	}
	
	@GetMapping("/del")
	public String del() {
		QuartzCustomerJob info = new QuartzCustomerJob();
		info.setCode("first");
		info.setGroup("firstGroup");
		info.setClazz(FirstJob.class.getName());
		String cron = "0/20 * * * * ?";
		info.setCron(cron);
		
		quartzManager.removeJob(info);
		return "ok " + LocalTime.now();
	}
	
	
	@GetMapping("/start")
	public String start() {
		quartzManager.startJobs();
		return "ok " + LocalTime.now();
	}
}
