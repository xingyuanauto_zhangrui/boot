/**
 * 
 */
package pers.vic.test.quartz;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年7月30日下午6:01:01
 */
public class Quartz001 {

	public static void main(String[] args) throws InterruptedException {
		
        try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            dosome(scheduler);
            Thread.sleep(100000);
            scheduler.shutdown();

        } catch (SchedulerException se) {
            se.printStackTrace();
        }
    }
	
	
	public static void dosome(Scheduler scheduler) throws SchedulerException {
		// define the job and tie it to our HelloJob class
		  JobDetail job = JobBuilder.newJob(PrintPropsJob.class)
		      .withIdentity("job1", "group1")
		      .usingJobData("test", "i am job data")
		      .withDescription("我是一个测试job")
		      .build();

		  // Trigger the job to run now, and then repeat every 40 seconds
		  Trigger trigger = TriggerBuilder.newTrigger()
		      .withIdentity("trigger1", "group1")
		      .startNow()
		            .withSchedule(SimpleScheduleBuilder.simpleSchedule()
		              .withIntervalInSeconds(5)
		              .repeatForever())            
		      .build();

		  // Tell Quartz001 to schedule the job using our trigger
		  scheduler.scheduleJob(job, trigger);
	}
}

