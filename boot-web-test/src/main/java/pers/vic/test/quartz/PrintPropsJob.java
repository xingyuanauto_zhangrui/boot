/**
 * 
 */
package pers.vic.test.quartz;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月3日下午1:38:02
 */
public class PrintPropsJob implements Job {

	public PrintPropsJob() {
		// Instances of Job must have a public no-argument constructor.
	}

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobDetail jobDetail = context.getJobDetail();
		
		System.out.println(jobDetail.getKey() + "  "  
				+jobDetail.getDescription());
		JobDataMap data = context.getMergedJobDataMap();
		System.out.println("test = " + data.getString("test"));
		System.out.println("123213");
	}


}