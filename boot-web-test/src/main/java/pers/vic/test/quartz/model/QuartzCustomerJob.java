package pers.vic.test.quartz.model;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 实体类
 * 
 * @author Vic.xu
 */
public class QuartzCustomerJob extends BaseEntity {

	private static final long serialVersionUID = 1L;
	/**
	 * 任务code
	 */
	private String code;

	/**
	 * 任务名称
	 */
	private String name;

	/**
	 * 任务组
	 */
	private String group;

	/**
	 * 任务类全路径
	 */
	private String clazz;

	/**
	 * cron表达式
	 */
	private String cron;

	/**
	 * 执行中 暂停 删除
	 */
	private Integer status;

	/**
	 * 成功次数
	 */
	private Integer succeed;

	/**
	 * 失败次数
	 */
	private Integer fail;

	/**
	 * 其他说明
	 */
	private String remark;

	/***************** set|get start **************************************/

	/**
	 * set：任务code
	 */
	public QuartzCustomerJob setCode(String code) {
		this.code = code;
		return this;
	}

	/**
	 * get：任务code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * set：任务名称
	 */
	public QuartzCustomerJob setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：任务名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：任务组
	 */
	public QuartzCustomerJob setGroup(String group) {
		this.group = group;
		return this;
	}

	/**
	 * get：任务组
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * set：任务类全路径
	 */
	public QuartzCustomerJob setClazz(String clazz) {
		this.clazz = clazz;
		return this;
	}

	/**
	 * get：任务类全路径
	 */
	public String getClazz() {
		return clazz;
	}

	/**
	 * set：cron表达式
	 */
	public QuartzCustomerJob setCron(String cron) {
		this.cron = cron;
		return this;
	}

	/**
	 * get：cron表达式
	 */
	public String getCron() {
		return cron;
	}

	/**
	 * set：执行中 暂停 删除
	 */
	public QuartzCustomerJob setStatus(Integer status) {
		this.status = status;
		return this;
	}

	/**
	 * get：执行中 暂停 删除
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * set：成功次数
	 */
	public QuartzCustomerJob setSucceed(Integer succeed) {
		this.succeed = succeed;
		return this;
	}

	/**
	 * get：成功次数
	 */
	public Integer getSucceed() {
		return succeed;
	}

	/**
	 * set：失败次数
	 */
	public QuartzCustomerJob setFail(Integer fail) {
		this.fail = fail;
		return this;
	}

	/**
	 * get：失败次数
	 */
	public Integer getFail() {
		return fail;
	}

	/**
	 * set：其他说明
	 */
	public QuartzCustomerJob setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：其他说明
	 */
	public String getRemark() {
		return remark;
	}
	/***************** set|get end **************************************/
}
