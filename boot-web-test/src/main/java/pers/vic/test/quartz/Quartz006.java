/**
 * 
 */
package pers.vic.test.quartz;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *  @description: CronTriggers
 *   http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/tutorial-lesson-06.html
 *  @author Vic.xu
 *  @date 2020年8月25日 17:17:29
 */
public class Quartz006 {
	/*
		import static org.quartz.TriggerBuilder.*;
		import static org.quartz.CronScheduleBuilder.*;
		import static org.quartz.DateBuilder.*:
	 */

	public static void main(String[] args) throws InterruptedException {
        try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            dosome(scheduler);
            Thread.sleep(100000);
            scheduler.shutdown();

        } catch (SchedulerException se) {
            se.printStackTrace();
        }
    }
	
	
	public static void dosome(Scheduler scheduler) throws SchedulerException {
		 JobDetail job = JobBuilder.newJob(PrintPropsJob.class)
			      .withIdentity("job1", "group1")
			      .usingJobData("test", "i am job data")
			      .withDescription("我是一个测试job")
			      .build();
		 
		Trigger trigger = TriggerBuilder.newTrigger()
			    .withIdentity("trigger3", "group1")
			    .withSchedule(CronScheduleBuilder.cronSchedule("0/20 0/1 * * * ?"))
			    .forJob("job1", "group1")
			    .build();
		/*
		 * trigger = TriggerBuilder.newTrigger() .withIdentity("trigger3", "group1")
		 * .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(10, 42))
		 * .forJob("myJob") .build();
		 */
		scheduler.scheduleJob(job, trigger);
		
	}
}
