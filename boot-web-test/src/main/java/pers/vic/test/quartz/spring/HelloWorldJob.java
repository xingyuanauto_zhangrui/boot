/**
 * 
 */
package pers.vic.test.quartz.spring;

import java.time.LocalTime;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月28日上午9:32:56
 */
public class HelloWorldJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("This is a first spring combine quartz !");
		System.out.println(LocalTime.now() + "\t go!!!!");
		
	}

	
}
