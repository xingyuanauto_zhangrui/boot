/**
 * 
 */
package pers.vic.test.quartz;

import java.util.Date;

import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;

/**
 *  @description: SimpleTriggers
 *   Here are various examples of defining triggers with simple schedules, read through them all, 
 *  as they each show at least one new/different point:
 *  @author Vic.xu
 *  @date: 2020年8月12日上午9:05:09
 */
public class Quartz005 {
	/*
	 * import static org.quartz.TriggerBuilder.*;
	   import static org.quartz.SimpleScheduleBuilder.*;
	   import static org.quartz.DateBuilder.*:
	 */

	public static void main(String[] args) {
		 JobDetail job = JobBuilder.newJob(PrintPropsJob.class)
			      .withIdentity("job1", "group1")
			      .usingJobData("test", "i am job data")
			      .withDescription("我是一个测试job")
			      .build();

		
		//001 Build a trigger for a specific moment in time, with no repeats:
		SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
			    .withIdentity("trigger1", "group1")
			    .startAt(new Date()) // some Date
			    .forJob("job1", "group1") // identify job with name, group strings
			    .build();
		//002 Build a trigger for a specific moment in time, then repeating every ten seconds ten times:
		trigger = TriggerBuilder.newTrigger()
			    .withIdentity("trigger3", "group1")
			    .startAt(new Date())  // if a start time is not given (if this line were omitted), "now" is implied
			    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
			        .withIntervalInSeconds(10)
			        .withRepeatCount(10)) // note that 10 repeats will give a total of 11 firings
			    .forJob(job) // identify job with handle to its JobDetail itself                   
			    .build();
		//003Build a trigger that will fire once, five minutes in the future:
		trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
			    .withIdentity("trigger5", "group1")
			    .startAt(DateBuilder.futureDate(5, IntervalUnit.MINUTE)) // use DateBuilder to create a date in the future
			    .forJob("job1") // identify job with its JobKey
			    .build();
		// 004 Build a trigger that will fire now, then repeat every five minutes, until the hour 22:00:
		trigger = TriggerBuilder.newTrigger()
			    .withIdentity("trigger7", "group1")
			    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
			        .withIntervalInMinutes(5)
			        .repeatForever())
			    .endAt(DateBuilder.dateOf(22, 0, 0))
			    .build();
		// 005 Build a trigger that will fire at the top of the next hour, then repeat every 2 hours, forever:
		trigger = TriggerBuilder.newTrigger()
			    .withIdentity("trigger8") // because group is not specified, "trigger8" will be in the default group
			    .startAt(DateBuilder.evenHourDate(null)) // get the next even-hour (minutes and seconds zero ("00:00"))
			    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
			        .withIntervalInHours(2)
			        .repeatForever())
			    // note that in this example, 'forJob(..)' is not called
			    //  - which is valid if the trigger is passed to the scheduler along with the job  
			    .build();
		
		
			//Misfire Instruction Constants of SimpleTrigger
		/*
		    MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY
			MISFIRE_INSTRUCTION_FIRE_NOW
			MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT
			MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_REMAINING_REPEAT_COUNT
			MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT
			MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT
		 */
		//When building SimpleTriggers, you specify the misfire instruction as part of the simple schedule (via SimpleSchedulerBuilder):
		trigger = TriggerBuilder.newTrigger()
			    .withIdentity("trigger7", "group1")
			    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
			        .withIntervalInMinutes(5)
			        .repeatForever()
			        .withMisfireHandlingInstructionNextWithExistingCount())
			    .build();


//			    scheduler.scheduleJob(trigger, job);
		
	}
}
