/**
 * 
 */
package pers.vic.test.quartz;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.HolidayCalendar;

/**
 *  @description:  Calendar Example
 *  @author Vic.xu
 *  @date: 2020年8月12日上午8:28:01
 */
public class Quartz004 {

	
	public static void main(String[] args) throws InterruptedException {
		try {
            // Grab the Scheduler instance from the Factory
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();
            dosome(scheduler);
            Thread.sleep(100000);
            scheduler.shutdown();

        } catch (SchedulerException se) {
            se.printStackTrace();
        }
		
	}

	/**
	 * @param scheduler
	 * @throws SchedulerException 
	 */
	private static void dosome(Scheduler scheduler) throws SchedulerException {
		
		
		  JobDetail job = JobBuilder.newJob(PrintPropsJob.class)
			      .withIdentity("job1", "group1")
			      .usingJobData("test", "i am job data")
			      .withDescription("我是一个测试job")
			      .build();
		HolidayCalendar cal = new HolidayCalendar();
		cal.addExcludedDate( new Date() );
		cal.addExcludedDate( new Date() );

		scheduler.addCalendar("myHolidays", cal, false, false);


		Trigger t = TriggerBuilder.newTrigger()
		    .withIdentity("myTrigger")
		    .forJob("myJob")
		    .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(9, 30)) // execute job daily at 9:30
		    .modifiedByCalendar("myHolidays") // but not on holidays
		    .build();

		// .. schedule job with trigger

		Trigger t2 = TriggerBuilder.newTrigger()
		    .withIdentity("myTrigger2")
		    .forJob("myJob2")
		    .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(11, 30)) // execute job daily at 11:30
		    .modifiedByCalendar("myHolidays") // but not on holidays
		    .build();
		scheduler.scheduleJob(job, t2);
		 scheduler.scheduleJob(job, t);

		
	}

	
}
