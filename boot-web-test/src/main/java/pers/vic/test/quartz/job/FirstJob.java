/**
 * 
 */
package pers.vic.test.quartz.job;

import java.time.LocalTime;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import pers.vic.boot.util.JsonUtil;
import pers.vic.test.quartz.model.QuartzCustomerJob;
import pers.vic.test.quartz.service.QuartzCustomerJobService;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月28日上午10:57:04
 */
//@Component
public class FirstJob extends QuartzJobBean  {
   //假装注入了一些service 或者set方式注入
	
	@Autowired
	private QuartzCustomerJobService  customerJobService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext)
            throws JobExecutionException {
    	JobDetail jobDetail = jobExecutionContext.getJobDetail();
    	JobDataMap map = jobDetail.getJobDataMap();
    	 // 任务的具体逻辑
    	System.out.println(jobDetail.getKey() + " into   FirstJob  executeInternal  at " + LocalTime.now() + "   data => " );
    	map.forEach((k,v)->{
    		System.out.println("\t" + k + " ->" + v);
    	});
    	customerJobService.list(new QuartzCustomerJob()).forEach(JsonUtil::printJson);
       
    }
}
