/**
 *
 */
package pers.vic.test.enums;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年8月13日下午2:02:06
 */
public enum TestEnum2 {


    /**red*/
    red("aa"),
    /**green*/
    green("bb"),
    /**blue*/
    blue("cc"),
    ;


    private String text;

    /**
     * @param string
     */
    TestEnum2(String text) {
        this.text = text;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }


}
