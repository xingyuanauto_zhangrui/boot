/**
 * 
 */
package pers.vic.test.enums;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月14日上午10:40:08
 */
public class TestAnnotationWithEnum {

}

class One {
	
	@AnnotionWithEnum(TestEnum.dept)
	private String name;
}
