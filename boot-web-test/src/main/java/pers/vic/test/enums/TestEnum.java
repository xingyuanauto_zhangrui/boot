/**
 *
 */
package pers.vic.test.enums;

/**
 *  @description:
 *  @author Vic.xu
 *  @date: 2020年5月14日上午10:16:00
 */
public enum TestEnum implements HandlerType {

    /**dept*/
    dept {
        @Override
        public String handlerSql(String sql, String column) {
            return this.name();
        }

    },

    /**test*/
    test {
        @Override
        public String handlerSql(String sql, String column) {
            return this.name();
        }

    };

    public static void main(String[] args) {
        HandlerType h = TestEnum.dept;
        System.out.println(h.handlerSql("", ""));
    }
}
