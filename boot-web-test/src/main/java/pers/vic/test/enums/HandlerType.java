/**
 * 
 */
package pers.vic.test.enums;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月14日上午10:15:12
 */
public interface HandlerType {
	
	public abstract String handlerSql(String sql, String column);

}
