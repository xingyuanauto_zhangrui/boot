/**
 * 
 */
package pers.vic.test.enums;

import java.math.BigDecimal;

import pers.vic.boot.util.reflect.ReflectionUtils;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年8月13日下午2:05:42
 */
public class EnumReflection {
	
	private String id;
	
	private TestEnum2 enum2;
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the enum2
	 */
	public TestEnum2 getEnum2() {
		return enum2;
	}

	/**
	 * @param enum2 the enum2 to set
	 */
	public void setEnum2(TestEnum2 enum2) {
		this.enum2 = enum2;
	}
	
	
	
	public static void main(String[] args) {
		EnumReflection f = new EnumReflection();
		f.setId("1");
		f.setEnum2(TestEnum2.green);
		
		System.out.println(String.valueOf(TestEnum2.green));
		
		Object obj = ReflectionUtils.getFieldValue(f, "enum2");
		
		boolean b = obj.getClass().isEnum();
		
		boolean c = (obj instanceof Enum);
		
		Object od = ReflectionUtils.getFieldValue(obj, "ordinal");
		
		Object text = ReflectionUtils.getFieldValue(obj, "text");
		
		
		System.out.println("text :" + text);
		System.out.println(od);
		System.out.println(b);
		System.out.println(c);
		
		Double a =12.0;
		System.out.println(a instanceof Number);
		
		BigDecimal d = new BigDecimal("1.234");
		System.out.println(d.toString());
		d = d.setScale(2, BigDecimal.ROUND_HALF_UP);
		System.out.println(d.toString());
		
		
	}

}
