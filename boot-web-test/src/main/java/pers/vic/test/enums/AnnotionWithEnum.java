/**
 * 
 */
package pers.vic.test.enums;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月14日上午10:38:49
 */
@Retention (RetentionPolicy.RUNTIME)
@Target({  ElementType.FIELD })
public @interface AnnotionWithEnum {

	TestEnum value();
}
