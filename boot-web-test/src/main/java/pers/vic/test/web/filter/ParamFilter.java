/**
 * 
 */
package pers.vic.test.web.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月4日上午11:44:23
 */
//@WebFilter(urlPatterns = "/*")
public class ParamFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println(".ParamFilter..");
		chain.doFilter(new ParameterRequestWrapper((HttpServletRequest)request), response);
		
	}

	
	static class ParameterRequestWrapper extends HttpServletRequestWrapper{
		
		static String SUFFIX = "-123";

		/**
		 * @param request
		 */
		public ParameterRequestWrapper(HttpServletRequest request) {
			super(request);
		}

		@Override
		public String getParameter(String name) {
			return super.getParameter(name) + SUFFIX;
		}

		@Override
		public Map<String, String[]> getParameterMap() {
			Map<String, String[]> map = new HashMap<String, String[]>();
			super.getParameterMap().forEach((k,v)->{
				for(int i=0; i<v.length;i++) {
					v[i] = v[i] + SUFFIX;
				}
				map.put(k, v);
			});
			return map;
		}

		@Override
		public Enumeration<String> getParameterNames() {
			return super.getParameterNames();
		}

		@Override
		public String[] getParameterValues(String name) {
			String[] arrs = super.getParameterValues(name);
			for (int i = 0; i < arrs.length; i++) {
				arrs[i] = arrs[i] + SUFFIX;
			}
			return arrs;
		}
		
		
		
		
		
	}
}
