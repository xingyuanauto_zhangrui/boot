
DROP TABLE IF EXISTS `t_auth_application`;

CREATE TABLE `t_auth_application` (
  `ID` varchar(32) NOT NULL,
  `APPLICATION_NAME` varchar(255) DEFAULT NULL,
  `APPLICATION_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_identify` */

DROP TABLE IF EXISTS `t_auth_identify`;

CREATE TABLE `t_auth_identify` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `FK_ORG_ID` varchar(32) DEFAULT NULL COMMENT '组织机构表主键',
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FK_APPLICATION_ID` varchar(32) DEFAULT NULL COMMENT '应用表主键，比如CPLEASE、APLEASE是两个独立的应用',
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_identify_role` */

DROP TABLE IF EXISTS `t_auth_identify_role`;

CREATE TABLE `t_auth_identify_role` (
  `ID` varchar(32) NOT NULL,
  `IDENTIFY_ID` varchar(32) DEFAULT NULL,
  `ROLE_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_org_indentify` */

DROP TABLE IF EXISTS `t_auth_org_indentify`;

CREATE TABLE `t_auth_org_indentify` (
  `ID` varchar(32) DEFAULT NULL,
  `ORGANIZATION_ID` varchar(32) DEFAULT NULL,
  `INDENTIFY_ID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL COMMENT '1：联系部门；2：管理部门；3：授权子公司',
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_orgnization` */

DROP TABLE IF EXISTS `t_auth_orgnization`;

CREATE TABLE `t_auth_orgnization` (
  `ID` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL COMMENT '1：集团；2、子公司；3：部门',
  `PID` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_position` */

DROP TABLE IF EXISTS `t_auth_position`;

CREATE TABLE `t_auth_position` (
  `ID` varchar(32) NOT NULL,
  `TYPE` varchar(32) DEFAULT NULL COMMENT '1：技术岗位；2：管理岗；',
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FK_ORG_ID` varchar(32) DEFAULT NULL,
  `FK_APPLICATION_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_position_indentify` */

DROP TABLE IF EXISTS `t_auth_position_indentify`;

CREATE TABLE `t_auth_position_indentify` (
  `ID` varchar(32) NOT NULL,
  `IDENTIFY_ID` varchar(32) DEFAULT NULL,
  `POSITION_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_resource` */

DROP TABLE IF EXISTS `t_auth_resource`;

CREATE TABLE `t_auth_resource` (
  `ID` varchar(32) NOT NULL COMMENT '权限表主键',
  `NAME` varchar(32) DEFAULT NULL COMMENT '资源名称',
  `DESCRIPTION` varchar(32) DEFAULT NULL COMMENT '资源描述',
  `TYPE` varchar(32) DEFAULT NULL COMMENT '资源类型，1：菜单；2：按钮',
  `PARENT_ID` varchar(32) DEFAULT NULL COMMENT '父资源ID，如果该字段为空，表示该资源为根节点',
  `ICON` varchar(32) DEFAULT NULL COMMENT '资源在页面上显示的图标路径',
  `URL` varchar(32) DEFAULT NULL COMMENT '资源请求到后端的URL地址',
  `SORT` varchar(32) DEFAULT NULL COMMENT '资源的序号（用于排序显示）',
  `FK_APPLICATION_ID` varchar(32) DEFAULT NULL COMMENT '应用表主键，比如CPLEASE、APLEASE是两个独立的应用',
  `PERMISSION_FLAG` varchar(32) DEFAULT NULL COMMENT '用于shiro进行鉴权的标识',
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_role` */

DROP TABLE IF EXISTS `t_auth_role`;

CREATE TABLE `t_auth_role` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `FK_APPLICATION_ID` varchar(32) DEFAULT NULL COMMENT '应用表主键，比如CPLEASE、APLEASE是两个独立的应用',
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_role_resource` */

DROP TABLE IF EXISTS `t_auth_role_resource`;

CREATE TABLE `t_auth_role_resource` (
  `ID` varchar(32) NOT NULL,
  `ROLE_ID` varchar(32) DEFAULT NULL,
  `RESOURCE_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL,
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_user` */

DROP TABLE IF EXISTS `t_auth_user`;

CREATE TABLE `t_auth_user` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `LOGIN_NAME` varchar(255) DEFAULT NULL,
  `LOGIN_PASSWD` varchar(255) DEFAULT NULL,
  `SALT` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL COMMENT '用户表主键ID',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL COMMENT '用户表主键ID',
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL COMMENT '0：无效；1：有效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_auth_user_identify` */

DROP TABLE IF EXISTS `t_auth_user_identify`;

CREATE TABLE `t_auth_user_identify` (
  `ID` varchar(32) NOT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `IDENTIFY_ID` varchar(32) DEFAULT NULL,
  `CREATE_USER_ID` varchar(32) DEFAULT NULL COMMENT '用户表主键ID',
  `CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL COMMENT '用户表主键ID',
  `LAST_UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ENABLE_FLAG` char(1) DEFAULT NULL COMMENT '0：无效；1：有效',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


