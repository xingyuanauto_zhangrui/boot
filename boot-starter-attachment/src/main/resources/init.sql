CREATE TABLE `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '附件表id',
  `absolute_path` varchar(255) DEFAULT NULL COMMENT '绝对地址',
  `relative_path` varchar(255) DEFAULT NULL COMMENT '相对地址',
  `temporary` tinyint(1) DEFAULT '0' COMMENT '是否是临时的',
  `module` varchar(32) DEFAULT NULL COMMENT '所属模块',
  `content_type` varchar(128) DEFAULT NULL COMMENT 'mimetype',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `filename` varchar(128) DEFAULT NULL COMMENT '文件名',
  `original_name` varchar(128) DEFAULT NULL COMMENT '原文件名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件表';