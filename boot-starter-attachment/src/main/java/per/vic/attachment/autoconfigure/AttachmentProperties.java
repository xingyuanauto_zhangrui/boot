package per.vic.attachment.autoconfigure;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** 
 * @description: 附件配置项
 * @author: Vic.xu
 * @date: 2019年12月11日 上午9:33:21
 */
@Configuration
@ConfigurationProperties(prefix = AttachmentProperties.ATTACHMENT_PREFIX)
public class AttachmentProperties {
	
	public static final String ATTACHMENT_PREFIX = "attachment";
	
	public static final String DEFAULT_ATTACHMENT_REG = "/attachment/(download|visit)/(\\d+)\"";
	
	/**请求头检验name*/
	public static final String HEADER_CHECK_NAME = "password";
	
	/**
	 * 类型：
	 * 1-服务端 ：开启接口 (默认)
	 * 2-客户端 ：开启上传到服务端的sdk（AttachmentClientService）
	 */
	private Integer type = 1;
	/**
	 * 附件存放的位置
	 */
	private String position;
	
	/**
	 * 附件访问的地址的主机. 绝对地址：URL= host + 相对路径
	 */
	private String host;
	
	/**
	 *  附件访问地址：相对于项目; 附件地址 = attachment.visit + /attachment//visit/{id}
	 */
	private String visit;
	
	/**
	 * type = 2(作为客户端的时候)时附件服务端的地址，调用接口的时候
	 */
	private String server;
	
	/**
	 * 空的附件的相对地址
	 */
	private String broken;
	
	/**
	 * 启动时是否检测table存在
	 */
	private boolean checkTable;
	
	/**
	 * 接口header中的安全标识  客户端需 和服务端一致
	 */
	private String password;
	
	/**
	 * 从富文本的附件中获取附件对应的id的正则表达式
	 */
	public static  String textAttachmentReg = DEFAULT_ATTACHMENT_REG;
	
	/**
	 * 是否开启定时清除临时态附件
	 */
	private boolean cleaner = true;

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getBroken() {
		return broken;
	}

	public void setBroken(String broken) {
		this.broken = broken;
	}

	public boolean isCleaner() {
		return cleaner;
	}

	public void setCleaner(boolean cleaner) {
		this.cleaner = cleaner;
	}

	public String getTextAttachmentReg() {
		return textAttachmentReg;
	}

	@SuppressWarnings("static-access")
	public void setTextAttachmentReg(String textAttachmentReg) {
		if(StringUtils.isNotBlank(textAttachmentReg)) {
			this.textAttachmentReg = textAttachmentReg;
		}
	}

	public boolean isCheckTable() {
		return checkTable;
	}

	public void setCheckTable(boolean checkTable) {
		this.checkTable = checkTable;
	}

	public String getVisit() {
		return visit;
	}
	public void setVisit(String visit) {
		this.visit = visit;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}
	
	/**是否是客户端*/
	public boolean isClient() {
		return type != null && 2 == type.intValue();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
