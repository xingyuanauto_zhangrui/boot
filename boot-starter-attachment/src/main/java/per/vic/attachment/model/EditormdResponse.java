package per.vic.attachment.model;

/** 
 * @description: Editor.md 编辑器文件上传返回对象
 * @author: Vic.xu
 * @date: 2019年12月19日 上午11:28:42
 */
public class EditormdResponse {
	/*
	success : 0 | 1,           // 0 表示上传失败，1 表示上传成功
    message : "提示的信息，上传成功或上传失败及错误信息等。",
    url     : "图片地址"        // 上传成功时才返回
    */
	
	private int success;
	
	private String message;
	
	private String url;
	
	
	public static EditormdResponse success(String url) {
		return new EditormdResponse(1, null, url);
	}
	
	public static EditormdResponse error(String message) {
		return new EditormdResponse(0, message, null);
	}
	
	

	private EditormdResponse(int success, String message, String url) {
		super();
		this.success = success;
		this.message = message;
		this.url = url;
	}

	public int getSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public String getUrl() {
		return url;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
