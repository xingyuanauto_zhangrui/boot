package per.vic.attachment.constant;

/**
 * 描述:
 * @author Vic.xu
 * @date 2021-12-03 14:21
 */
public class AttachmentConstant {

    /**
     * IDS  regex
     */
    public static final String IDS_REG = "(\\d+|\\d+-|\\d+-\\d+)+";

    /**
     * upfile  name
     */
    public static final String FILE_NAME = "upfile";
}
