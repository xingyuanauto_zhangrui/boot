/**
 *
 */
package per.vic.attachment.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import per.vic.attachment.annotation.AttachmentFlag;
import per.vic.attachment.autoconfigure.AttachmentProperties;
import pers.vic.boot.util.CommonUtils;
import pers.vic.boot.util.RegexUtil;
import pers.vic.boot.util.reflect.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  @description: 根据注解提交实体对象中的附件信息 工具类
 *  @author Vic.xu
 *  @date: 2020年9月21日上午11:21:08
 */
public class AttachmentFetchByAnnotationUtil {

    private static Logger logger = LoggerFactory.getLogger(AttachmentFetchByAnnotationUtil.class);

    /**
     * 从对象中获取附件的id集合
     *
     * @see AttachmentFlag
     */
    public static <T> List<Integer> getAttachmentIds(T t) {
        List<Integer> list = new ArrayList<Integer>();
        if (t == null) {
            return list;
        }
        try {
            Class<?> clazz = t.getClass();
            for (; clazz != Object.class; clazz = clazz.getSuperclass()) {// 向上遍历父类

                Field[] fileds = clazz.getDeclaredFields();
                for (Field f : fileds) {
                    if (f.isAnnotationPresent(AttachmentFlag.class)) {
                        AttachmentFlag flag = f.getAnnotation(AttachmentFlag.class);
//						PropertyDescriptor pd = new PropertyDescriptor(f.getName(), t.getClass());
//						Method method = pd.getReadMethod();// 获得get方法
                        //修改为直接通过get方法获取  而不再是自省的方式
                        ReflectionUtils.makeAccessible(f);
                        Object value = f.get(t);
                        if (value == null || "".equals(value.toString())) {
                            continue;
                        }
                        switch (flag.value()) {
                            case SIGN:
                                if ((value.toString()).matches("\\d+")) {
                                    list.add(Integer.parseInt(value.toString()));
                                }
                                break;
                            case SIGNS:
                                list.addAll(CommonUtils.toIntList(value.toString().split(",")));
                                break;
                            case CONTENT:
                                Integer[] arr = getAttachmentIds(value.toString());
                                if (arr != null && arr.length > 0) {
                                    list.addAll(new ArrayList<Integer>(Arrays.asList(arr)));
                                }
                                break;
                            default:
                                logger.info("反射{}字段{}获得的值为{}", new Object[]{t.getClass(), f.getName(), value});
                                break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
        return list;
    }

    /** 获得文本内容中的附件id数组 */
    public static Integer[] getAttachmentIds(String content) {
        List<String> list = RegexUtil.getList(content, AttachmentProperties.textAttachmentReg, 1);
        if (list.isEmpty()) {
            return new Integer[]{};
        }
        List<Integer> intList = CommonUtils.toIntList(list.toArray(new String[0]));
        int size = intList.size();
        Integer[] arr = new Integer[size];
        for (int i = 0; i < size; i++) {
            arr[i] = intList.get(i);
        }
        return arr;
    }

    public static void main(String[] args) {
        String content = "/attachment/visit/1\"";
        content += "/attachment/download/2\"";
        String defaultAttachmentReg = "/attachment/(download|visit)/(\\d+)\"";
        System.out.println(content.matches(defaultAttachmentReg));
        List<String> list = RegexUtil.getList(content, defaultAttachmentReg, 2);
        String ids = list.stream().map(String::valueOf).collect(Collectors.joining(","));
        System.out.println(ids);
    }


}
