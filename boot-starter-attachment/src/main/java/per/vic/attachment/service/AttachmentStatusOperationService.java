/**
 * 
 */
package per.vic.attachment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import per.vic.attachment.autoconfigure.AttachmentProperties;
import per.vic.attachment.dao.AttachmentDao;
import per.vic.attachment.util.AttachmentFetchByAnnotationUtil;
import pers.vic.boot.base.service.AttachmentStatusOperationServiceI;
import pers.vic.boot.util.collections.CalcDiffCollection;

/**
 * @description: 提供对附件状态操作的service
 * @author Vic.xu
 * @date: 2020年9月21日上午11:23:45
 */
@Service
public class AttachmentStatusOperationService implements AttachmentStatusOperationServiceI {

	@Autowired
	private AttachmentProperties attachmentProperties;

	@Autowired(required = false)
	private AttachmentDao attachmentDao;

	@Autowired(required = false)
	private AttachmentClientService attachmentClientService;

	/**
	 * 新增对象中的全部附件
	 */
	@Override
	public <T> boolean addAttachmengFromObj(T t) {
		List<Integer> idList = AttachmentFetchByAnnotationUtil.getAttachmentIds(t);
		updateTemporary(false, idList);
		return true;
	}

	/**
	 * 删除对象中的附件
	 */
	@Override
	public <T> void deleteAttachmengFromObj(T t) {
		updateTemporary(true, AttachmentFetchByAnnotationUtil.getAttachmentIds(t));
	}
	
	/**
	 * 批量删除附件
	 */
	@Override
	public <T> void deleteAttachmengFromObj(List<T> ts) {
		if (ts == null) {
			return;
		}
		List<Integer> list = new ArrayList<Integer>();
		for (T t : ts) {
			list.addAll(AttachmentFetchByAnnotationUtil.getAttachmentIds(t));
		}
		updateTemporary(true, list);		
	}

	/**
	 * 对比对象修改前后的附件 分别做新增和删除处理
	 */
	@Override
	public <T> void handleOldAndNowAttachment(T old, T now) {
		CalcDiffCollection<Integer> data = CalcDiffCollection.instance(
				AttachmentFetchByAnnotationUtil.getAttachmentIds(old),
				AttachmentFetchByAnnotationUtil.getAttachmentIds(now));
		updateTemporary(true, data.getOnlyInOld());
		updateTemporary(false, data.getOnlyInNew());
	}

	/*****************************************************************************************************/

	/** 修改附件状态 */
	public void updateTemporary(boolean temporary, List<Integer> ids) {
		if (CollectionUtils.isEmpty(ids)) {
			return;
		}
		// 如果是客户端则调用SDK，否则直接操作数据库
		if (isClient()) {
			attachmentClientService.updateTemporary(temporary, ids.toArray(new Integer[ids.size()]));
		} else {
			String idsStr = ids.stream().map(String::valueOf).collect(Collectors.joining(","));
			attachmentDao.updateTemporary(idsStr, temporary);
		}

	}

	/**
	 * 当前是否是附件客户端
	 * 
	 * @return
	 */
	private boolean isClient() {
		Integer type = attachmentProperties.getType();
		return type != null && 2 == type.intValue();

	}

}
