### 003-编码代码生成starter

#### 目标

1. 根据配置的不同数据源：mysql/oracle，根据编写的模板生成代码，包括：实体、mapper、service、controller（以及前端的list 和form等）
2. 此starter提供页面
3. [boot-starter-generator](https://gitee.com/xuqiudong/boot/tree/master/boot-starter-generator)   

>  

#### 如何自定义一个starter

> [springboot官网提供的starter](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using.build-systems.starters)
>
> [Spring Boot Creating Your Own Starter](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.developing-auto-configuration.custom-starter)

#### 模块划分：

- autoconfigure 模块：自动装配模块
- starter 模块：一个空模块提供依赖的模块,

> 我们编写的时候可以合并在一起

#### 命名规范：

- **官网**：spring-boot-starter-*
- **自定义**：*-spring-boot-starter

#### `properties`提示文件：additional-spring-configuration-metadata.json

>  引入spring-boot-configuration-processor依赖 可自动生成META-INF/additional-spring-configuration-metadata.json

但是要在打包的是移除掉对它的依赖

```xml
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <excludes>
            <exclude>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-autoconfigure-processor</artifactId>
            </exclude>
        </excludes>
    </configuration>
</plugin>
```



#### 一 生成代码的基本思路

1. 连接数据库获取数据库表的全部信息(暂时实现：mysql和oracle)
   - 支持多种数据库,定义dao接口,不同的数据库做不同的实现,根据配置文件指定Primary
2. 解析表的基本信息,列的基本信息映射成对象
3. 编写各层模板(本项目使用的是**velocity**),根据解析的数据对模板进行填充
4. 导出填充后的模板

#### 二 [boot-starter-generator](https://gitee.com/lcxm/boot/tree/master/boot-starter-generator) 的简单说明

##### 1 配置文件 参考  `generator-config.properties`

```properties
#代码生成器，配置信息
#包名
generator.packageName=pers.vic.boot.console
#模块名
generator.moduleName=system
#作者
generator.author=Vic.xu
#Email
generator.email=xuduochoua@163.com
#表前缀(类名不会包含表前缀)
generator.tablePrefix=t_

#实体中忽略的字段(在基类中的字段) 另:列名转换成属性名的时候 is_delete格式会变成delete
#generator.ignores=id,is_enable,is_delete,create_time,update_time
generator.ignores=ID,CREATE_USER_ID,CREATE_TIME,LAST_UPDATE_USER_ID,LAST_UPDATE_TIME,ENABLE_FLAG


#需要填充的模板
generator.templates[0]=generator/templates/Entity.java.vm
generator.templates[1]=generator/templates/Mapper.java.vm
generator.templates[2]=generator/templates/Mapper.xml.vm
generator.templates[3]=generator/templates/Service.java.vm
generator.templates[4]=generator/templates/Controller.java.vm

#数据类型转换，配置信息:数据库类型->java类型
generator.dataTypeConvert.tinyint=Integer
generator.dataTypeConvert.smallint=Inteer
generator.dataTypeConvert.mediumint=Integer
generator.dataTypeConvert.int=Integer
generator.dataTypeConvert.integer=Integer
generator.dataTypeConvert.bigint=Long
generator.dataTypeConvert.float=Float
generator.dataTypeConvert.double=Double
generator.dataTypeConvert.decimal=BigDecimal
generator.dataTypeConvert.bit=Boolean
generator.dataTypeConvert.enum=String

generator.dataTypeConvert.char=String
generator.dataTypeConvert.varchar=String
generator.dataTypeConvert.tinytext=String
generator.dataTypeConvert.text=String
generator.dataTypeConvert.mediumtext=String
generator.dataTypeConvert.longtext=String

generator.dataTypeConvert.date=Date
generator.dataTypeConvert.time=Date
generator.dataTypeConvert.datetime=Date
generator.dataTypeConvert.timestamp=Date
```



##### 2 代码生成starter 入口 `GeneratorAutoconfigigure`

- 根据配置的`generator.database` 数据库类型， 注册不同的GeneratorDao实现类，本项目暂时已经实现了mysql版本的GeneratorDao；dao的对数据库的查询操作使用的是`jdbcTemplate`

```
/**
 * @description: 代码生成starter 入口
 * @author: Vic.xu
 * @date: 2020年3月10日 上午10:54:39
 */
@Configuration
@ConditionalOnClass(value = {JdbcTemplate.class})
@ConditionalOnWebApplication
@ConditionalOnExpression("${generatir.enabled:true}")
@EnableConfigurationProperties({GeneratorProperties.class})
public class GeneratorAutoconfigigure {

    private static Logger logger = LoggerFactory.getLogger(GeneratorAutoconfigigure.class);

    private GeneratorProperties generatorProperties;

    private JdbcTemplate jdbcTemplate;

    public GeneratorAutoconfigigure(GeneratorProperties generatorProperties, JdbcTemplate jdbcTemplate) {
        this.generatorProperties = generatorProperties;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Bean
    @ConditionalOnMissingBean
    public GeneratorDao generatorDao() {
        DatabaseType type = DatabaseType.getByName(generatorProperties.getDatabase());
        Assert.notNull(type, "当前项目未支持" + type + "类型的数据库代码生成");
        GeneratorDao generatorDao = null;
        switch (type) {
            case oracle:
                generatorDao = new OracleGeneratorDao(jdbcTemplate);
                break;
            case mysql:
                generatorDao = new MysqlGeneratorDao(jdbcTemplate);
            default:
                break;
        }
        logger.info("代码自动生成当前配置的数据库类型为：{}, 使用的dao为{}", type, generatorDao.getClass().getSimpleName());
        logger.info("自动生成相关配置如下：{}", generatorProperties);
        return generatorDao;
    }

    @Bean
    @ConditionalOnMissingBean
    public GeneratorService generatorService(GeneratorDao generatorDao, GeneratorProperties generatorProperties) {
        return new GeneratorService(generatorDao, generatorProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public GeneratorController generatorController(GeneratorService generatorService) {
        return new GeneratorController(generatorService);
    }

    /**
     * 把static HTML所在的文件夹设置为静态资源
     * 
     * @description:
     * @author Vic.xu
     * @date: 2020年5月15日下午5:33:38
     */
    @Configuration
    static class StaticWebMvcConfigurerAdapter implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            // registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        }
    }

}


```

- 配置类`GeneratorProperties`

```java
/**
 * @description: 附件配置项
 * @author: Vic.xu

 */
@Configuration
@ConfigurationProperties(prefix = GeneratorProperties.GENERATOR_PREFIX)
@PropertySource("classpath:config/generator-config.properties")
public class GeneratorProperties implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String GENERATOR_PREFIX = "generator";

    /**
     * 数据库类型，默认mysql
     */
    @Value("${generator.database:mysql}")
    private String database;

    /**
     * 作者
     */
    private String author;

    /**
     * 表前缀 若存在则生成的实体名会去掉前缀
     */
    private String tablePrefix;

    /**
     * 实体中忽略的表字段
     */
    private Set<String> ignores;
    /**
     * 数据库中哪些数据类型表示大文本，(这样是数据不应出现在list页面)
     */
    private Set<String> textTypes;

    /**
     * 需要填充的数据模板
     */
    private List<String> templates;

    /**
     * 数据类型转换 数据库类型--> java 类型
     */
    private Map<String, String> dataTypeConvert;

    @PostConstruct
    private void post() {
        mergerDataType();
    }

    // 把默认的数据转换和配置的数据转换合并起来
    private void mergerDataType() {
        if (MapUtils.isEmpty(dataTypeConvert)) {
            dataTypeConvert = new HashMap<String, String>();
            dataTypeConvert.putAll(DEFAULT_DATA_TYPE_CONVERT);
            return;
        }

        DEFAULT_DATA_TYPE_CONVERT.forEach((k, v) -> {
            if (!dataTypeConvert.containsKey(k)) {
                dataTypeConvert.put(k, v);
            }
        });
    }

    @Override
    public String toString() {
        return "GeneratorProperties [database=" + database + ", author=" + author + ", tablePrefix=" + tablePrefix
            + ", ignores=" + ignores + ", textTypes=" + textTypes + ", templates=" + templates + ", dataTypeConvert="
            + dataTypeConvert + "]";
    }

    public String getAuthor() {
        return author;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public Set<String> getIgnores() {
        return ignores;
    }

    public List<String> getTemplates() {
        return templates;
    }

    public Map<String, String> getDataTypeConvert() {
        return dataTypeConvert;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public void setIgnores(Set<String> ignores) {
        this.ignores = ignores;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    public void setDataTypeConvert(Map<String, String> dataTypeConvert) {
        this.dataTypeConvert = dataTypeConvert;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

    /**
     * @param database
     *            the database to set
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * @return the textTypes
     */
    public Set<String> getTextTypes() {
        return textTypes;
    }

    /**
     * @param textTypes
     *            the textTypes to set
     */
    public void setTextTypes(Set<String> textTypes) {
        this.textTypes = textTypes;
    }

    /**
     * 默认的数据库类型和java类型的转换关系
     */
    public static Map<String, String> DEFAULT_DATA_TYPE_CONVERT = new HashMap<String, String>();

    static {
        DEFAULT_DATA_TYPE_CONVERT.put("tinyint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("smallint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("mediumint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("int", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("integer", "Integer");

        DEFAULT_DATA_TYPE_CONVERT.put("bigint", "Long");
        DEFAULT_DATA_TYPE_CONVERT.put("float", "Float");
        DEFAULT_DATA_TYPE_CONVERT.put("double", "Double");
        DEFAULT_DATA_TYPE_CONVERT.put("decimal", "");
        DEFAULT_DATA_TYPE_CONVERT.put("bit", "BigDecimal");
        DEFAULT_DATA_TYPE_CONVERT.put("enum", "String");

        DEFAULT_DATA_TYPE_CONVERT.put("char", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("varchar", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("varchar2", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("tinytext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("mediumtext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("longtext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("blob", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("clob", "String");

        DEFAULT_DATA_TYPE_CONVERT.put("date", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("time", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("datetime", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("timestamp", "Date");
    }

}

```



##### 3 测试代码生成`GeneratorTest`

> 暂时生成的zip中的文件是放在一个文件夹下，并未分包，可根据需要修改此逻辑

```
//查询当前数据库的表列表
public void list() {}
// 某个表的详情
public void detail() {}
// 导出某个或者多个表生成的
public void export() {}
```

##### 4 生成的文件包含

1. 实体依赖BaseEntity等

   ```java
   public class DevelopPlan extends BaseEntity {}
   ```

2. mybatis的xml，包含页面列查询，详情查询，新增，修改，删除等基本操作，参见BaseMapper接口

3. mapper：依赖BaseMapper

   ```java
   public interface DevelopPlanMapper extends BaseMapper<DevelopPlan> {
   }
   ```

   

4. service依赖BaseService

   ```java
   @Service
   public class DevelopPlanService extends BaseService<DevelopPlanMapper, DevelopPlan>{}
   ```

   

5. controller依赖BaseController

   ```java
   @RestController
   @RequestMapping("/develop/plan")
   public class DevelopPlanController extends BaseController<DevelopPlanService, DevelopPlan>{}
   ```

   

#### 三   自动生成的starter的页面 ， 使用`thymeleaf`编写,简单内置页面导出功能

> 后台管理地址：console.xuqiudong.cn
>
> ![image-20210917162635503](https://gitee.com/xuqiudong/images/raw/master/typora/202109171626383.png)



[本项目gitee源码地址](https://gitee.com/lcxm/boot/tree/master/boot-starter-generator)

本项目主要生成的模板包括：mapper，mapper.xml， service， controller， 以及前端的list.html和form.html, 可以根据吱声需要 重写generator.templates下的模板文件



>  2020-04
>
>  2021-10