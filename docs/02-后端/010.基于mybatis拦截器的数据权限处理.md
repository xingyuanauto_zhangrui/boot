[TOC]

## 010.基于mybatis拦截器的数据权限处理

### 一. mybatis拦截器原理

> [参见官方文档](https://mybatis.org/mybatis-3/zh/configuration.html#plugins),

#### 原理的简略说明:

> Mybatis拦截器只能拦截Mybatis的四大对象: Executor 对象，StatementHandler 对象，ParameterHandler 对象，ResultHandler对象。

> 在自定义的拦截器中，该拦截器实现了父类Interceptor的三个方法，其中setProperties
>
> 方法获取在配置文件中为该拦截器配置的属性，如果有配置的话。
>
> plugin方法是在statementHandler对象创建时被调用的，在plugin的方法实
>
> 现中，一定要调用Plugin类中的wrap方法，该方法的作用是生成并返回statementHandler
>
> 对象的代理类（因为这里要拦截的对象是statementHandler对象）。如果所要拦截对象方法被调用时，代理类会调用自定义拦截器的intercept方法;
>
> intercept方法可以实现你所要处理的逻辑。最后该方法调用innovation.proceed（）说明调用所拦截的方法。

> Mybatis 四大对象的创建过程都是在Configuration类中实现的，并且在创建各类对象的过程中会判断配置文件是否注册了该类对象的拦截器，如果注册了该类拦截器，生成的对象是代理对象



### 二基于mybatis拦截器的数据权限处理

#### 数据权限说明

​     本系统中的数据权限暂时主要包括部门权限/岗位权限/个人权限等, 具体的局限类型可根据实际的业务规则动态的扩展, 对于数据权限的处理主要是基于sql动态过滤, 比如部门权限, 则是在sql中动态的加入对当前用户的部门跟业务数据表中的部门比较过滤.

#### 数据权限的设计思路

   基于当前数据权限的需求,以及本系统采用mybatis作为orm框架, 故使用mybatis的拦截器实现.  并且借鉴PagerHelper分页插件的设计理念.  通过编写`AuthorityHelper`拦截器,拦截StatementHandler对象的prepare ,然后重写SQL 加入where条件过滤.  在service层通过手动开启权限拦截的方式,以及根据传入的权限类型动态加入sql的过滤.  权限类型`AuthorityType`枚举中实现了handlerSql, 动态改变sql.

#### 拦截器代码概要

##### 1. `AuthorityMethod`拦截器的基类

> 把需要拦截的数据类型和关联字段保存到当前线程

```java
public abstract class AuthorityMethod {	
	/**
	 * 当前线程中需要拦截的权限字段数据
	 */
	protected static final ThreadLocal<AuthorityData> LOCAL_AUTHORITY_DATA = new ThreadLocal<AuthorityData>();
    /**
     * 设置 AuthorityData 参数
     *
     * @param AuthorityData
     */
    protected static void setLocalAuthorityData(AuthorityData authorityData) {
        LOCAL_AUTHORITY_DATA.set(authorityData);
    }

    /**
     * 获取 AuthorityData 参数
     *
     * @return
     */
    public static AuthorityData getLocalAuthorityData() {
        return LOCAL_AUTHORITY_DATA.get();
    }

    /**
     * 移除本地变量
     */
    public static void clearAuthorityData() {
        LOCAL_AUTHORITY_DATA.remove();
    }
    
    /**
     * 
     * @description: 开启某个类型的权限拦截
     * @author: Vic.xu
     * @date: 2020年2月11日 下午4:20:41
     * @param type  类型
     * @param column 对应字段
     */
    public static void start(AuthorityType type, String column) {
    	AuthorityData data = getLocalAuthorityData();
    	if(data == null) {
    		data = new AuthorityData();
    	}
    	FilterColumn filterColumn = new FilterColumn(type, column);
    	data.addFilterColumn(filterColumn);
    	setLocalAuthorityData(data);
    }
}
```
##### 2 `AuthorityData` 存储在当前线程中用于权限处理拦截器的基本数据

   ```java
public class AuthorityData {
	
	/**
	 * 需要过滤的权限字段
	 */
	private Set<FilterColumn> filterColumns = new HashSet<AuthorityData.FilterColumn>();
	
	/**
	 * 新增要过滤的权限字段
	 */
	public AuthorityData addFilterColumn(FilterColumn column) {
		this.filterColumns.add(column);
		return this;
	}
	public Set<FilterColumn> getFilterColumns() {
		return filterColumns;
		
	}



	/**
	 * @description: 需要过滤的权限列字段; 当type一致 就认为是一个对象
	 * @author VIC.xu
	 * @date: 2020年2月11日 下午3:47:17
	 */
	static class FilterColumn {
		//权限类型
		private AuthorityType type;
		
		//过滤的列 如a.dept_id
		private String column;
		
		

		public FilterColumn(AuthorityType type, String column) {
			super();
			this.type = type;
			this.column = column;
		}
		
		

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}



		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FilterColumn other = (FilterColumn) obj;
			if (type != other.type)
				return false;
			return true;
		}

		public AuthorityType getType() {
			return type;
		}

		public String getColumn() {
			return column;
		}

		public void setType(AuthorityType type) {
			this.type = type;
		}

		public void setColumn(String column) {
			this.column = column;
		}

		
	}
	
}
   ```

##### 3 `AuthorityType` 权限过滤类型

> 权限过滤类型, 并在每个类型中处理SQL的追加

```java

public enum AuthorityType {

	DEPT {
		public String handlerSql(String sql, String column) {
			try {
				Select select = (Select) CCJSqlParserUtil.parse(sql);
				//TODO 此处的18 应该从当前线程的token中获取
				String append = column + " like '%18' ";
				Expression where = CCJSqlParserUtil.parseCondExpression(append);
				((PlainSelect) select.getSelectBody()).setWhere(where);
				String handledSql = select.toString();
				return handledSql;
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			return sql;
		}
	},
	OWER{
		public String handlerSql(String sql, String column) {
			return sql + "test";
		}	
	};
	// 这个抽象方法由不同枚举值提供不同的实现
	public abstract String handlerSql(String sql, String column);

}
```

##### 4 `AuthorityHelper` 权限拦截器

> 权限拦截器 ,  拦截StatementHandler对象的prepare ,然后重写SQL 加入where条件过滤;
>
> 根据service中是否开启权限拦截进行处理, 可处理多个权限过滤(即start多次).

```java
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class AuthorityHelper extends AuthorityMethod implements Interceptor {

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		Object target = invocation.getTarget();
        StatementHandler statementHandler = (StatementHandler) target;
        //追加处理权限的sql
        handlerAuthoritySql(statementHandler);
		Object result = invocation.proceed();
		return result;
	}
	
	/**
	 * @description: 追加处理权限的SQL
	 * @author: Vic.xu
	 * @date: 2020年2月11日 下午3:30:41
	 * @param statementHandler
	 * @throws Throwable
	 */
	private void handlerAuthoritySql(StatementHandler statementHandler) throws Throwable {
 		BoundSql boundSql = statementHandler.getBoundSql();
        String sql = boundSql.getSql();
        AuthorityData authorityData = getLocalAuthorityData();
        if(authorityData == null) {
        	return;
        }
        
        Set<FilterColumn> columns = authorityData.getFilterColumns();
        if(CollectionUtils.isEmpty(columns)) {
        	return;
        }
        for(FilterColumn column :  columns) {
        	AuthorityType type =  column.getType();
        	//处理SQL 追加权限
        	sql = type.handlerSql(sql, column.getColumn());
        }
		System.err.println("intercept..............................\n" + sql);
		//通过反射把SQL设置回去
		Field field = boundSql.getClass().getDeclaredField("sql");
        field.setAccessible(true);
        field.set(boundSql, sql);
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		
	}

}

```

#### 使用方式

在service中手动开启数据权限过滤,类似`PageHelper.startPage(pageNum, pageSize);`

代码 :AuthorityHelper.start(AuthorityType.DEPT, "a.dept_id");

参数分别为`过滤类型`, `数据权限字段`

