[TOC]

## 001-新建springboot父子项目



### 1-为什么新建父子项目

​		为了方便放模块的时候统一管理依赖

### 2-新建过程(Eclipse)

#### 2.1 新建父项目

* new maven project( create a simple project)
* version 为1.0.0



pom.xml

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<groupId>pers.vic</groupId>
	<artifactId>boot</artifactId>
	<version>1.0.0</version>
	<packaging>pom</packaging>

	<name>Vic.xu springboot parent</name>
	<description>bootparent preoject</description>

	<modules>
		<module>boot-jar-util</module>
		<module>boot-war-web</module>
	</modules>


	<properties>
		<java.version>1.8</java.version>
		<maven-jar-plugin.version>3.1.1</maven-jar-plugin.version>
		<spring.boot.version>2.1.7.RELEASE</spring.boot.version>
	</properties>

	<!-- Manage dependent versions of all subprojects -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${spring.boot.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
		</dependency>

	</dependencies>


	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>

			<!-- mybatis generator plugin -->
			<plugin>
				<groupId>org.mybatis.generator</groupId>
				<artifactId>mybatis-generator-maven-plugin</artifactId>
				<version>1.3.2</version>
				<configuration>
					<configurationFile>${basedir}/src/main/resources/generator/generatorConfig.xml</configurationFile>
					<overwrite>true</overwrite>
					<verbose>true</verbose>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>
```

#### 2.2 新建jar子项目

* new maven module (maven-archetype-quickstart)

#### 2.3 新建springboot子项目

* new maven module (maven-archetype-webapp)

* pom.xml

```xml
<?xml version="1.0"?>
<project
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>pers.vic</groupId>
		<artifactId>boot</artifactId>
		<version>1.0.0</version>
	</parent>

	<artifactId>boot-war-web</artifactId>
	<packaging>war</packaging>
	<name>boot-war-web Maven Webapp</name>
	<url>http://maven.apache.org</url>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<finalName>boot-war-web</finalName>
	</build>
</project>

```

* 启动类

  ```java
  @SpringBootApplication
  public class ConsoleApplication {
  
  	public static void main(String[] args) {
  		SpringApplication.run(ConsoleApplication.class, args);
  	}
  }
  
  ```

* resources下新增配置文件application.properties,激活真正的配置文件

  ```properties
  #spring.profiles.active=dev
  spring.profiles.include=dev
  ```

  * 其实yaml和properties文件是一样的,但是yaml文件空格错误的时候,个人觉得难以察觉

* config文件夹下新增application-dev.properties文件

  ```properties
  # 防止读取乱码
  spring.http.encoding.charset=UTF-8
  # 项目启动端口
  server.port=10083
  ```

* 配置文件优先级

  * 当前项目的根目录/config/                   # 最高优先级
  * 当前项目的根目录/                                # 第二优先级
  * 类路径(在resources目录下)/config/     # 第三优先级
  * 类路径(在resources目录下)/                # 第四优先级

#### 2.4-junit测试基类

```java
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(true)  
public class ConsoleAppl1icationTests {
	@Test
	public void contextLoads() {
	}
    
    @Autowired
    private TestRestTemplate restTemplate;
	
	@Test
    public void testIndex(){
		BaseResponse result = 			restTemplate.getForObject("/test/mybatis/list",BaseResponse.class);
		System.out.println(result.toJson());
    }
}
```

如果项目中已经引入test相关依赖,但是引入不了,可能是项目结构问题,修改`classpath` 文件中关于test目录的说明

```xml
<!-- <classpathentry kind="src" path="src/test/java"/> -->
<classpathentry kind="src" output="target/test-classes" path="src/test/java">
    <attributes>
        <attribute name="optional" value="true"/>
        <attribute name="maven.pomderived" value="true"/>
        <attribute name="test" value="true"/>
    </attributes>
</classpathentry>
```

