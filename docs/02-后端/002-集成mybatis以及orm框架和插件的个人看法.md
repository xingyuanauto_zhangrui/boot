[TOC]

##  002-集成mybatis以及orm框架和插件的个人看法

> 2019-11-13

### 1-orm框架选择和插件的个人看法

​       在springboot中使用jpa已经非常的方便了,当然也需要额外的花点学习的成本. 但是我个人是不大喜欢JPA这套规范下的代码的,有太多的不可控因素,对于研究不深的人使用起来就是灾难.  曾使用hiberndate的时候就遇到过持久代持续增长和级联查询IO过频等问题,暂且不表. JPA唯一的好处,我个人看来是对单表的操作,当关联关系复杂时,从优化层面就难以控制了(限于本人水平有限). 当然JPA也支持原生的sql,但是和相比于已经很难看的xml里的sql, 注解里的sql的可读性就更差了,所以放弃了JAP.

​     选择mybatis时也有一点小小的纠结,就是是否需要使用tk.mybatis或者mybatis-plus等,想了很久,决定全部放弃.基于个人对二者了解程度有限,单纯觉得两者都要写实体上写注解,使DO(data obejct) 和VO界限不清,如果完全区分VO和DO 则又需要中间一层的转换,而使用DO作为VO,难免有需要对对象做一些调整.虽然二者都可以结合generator自动生成代码,但是个人还是不喜欢(大概是鸡蛋里挑骨头吧).

​       最终还是坚持选择了原生的mybatis,但是个人也会写一个代码生成器,生成简单的增删改查等根据业务需要使用的通用单表处理. 如此自主程度会更高. 至于mybatis的原生的缺点,比如表结构修改,比如xml文件的丑陋等,此处就不多做赘述了.



### 2-集成mybatis

#### 2.1-创建数据库(mysql)

```sql
create database databaseName default charset utf8;
GRANT ALL PRIVILEGES ON userName.* TO databaseName@'localhost' IDENTIFIED BY 'password';
```

#### 2.2-新增依赖

```xml
<!-- mysql连接类 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>

<!-- mybatis 依赖 -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.0.0</version>
</dependency>
```

#### 2.3-配置数据库和mybatis

```properties
# mysql
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/boot?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC&useSSL=true
spring.datasource.username=boot
spring.datasource.password=password

# mybatis 配置 
mybatis.config-location=classpath:config/mybatis-config.xml
mybatis.mapper-locations=classpath:mapper/**/*.xml

```

* 新增mybatis-config.xml文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>  
<!DOCTYPE configuration  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"  "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING" />
    </settings>
</configuratio
```

#### 2.4-启动类开启扫描mapper注解

* `@MapperScan(basePackages = "pers.vic.boot.console.**.mapper")`

#### 2.5 测试mybatis 

1. 建表

   ```sql
   -- ALTER TABLE test_mybatis   MODIFY COLUMN update_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;
   
     CREATE TABLE `test_mybatis` (
     `id` INT(11) NOT NULL AUTO_INCREMENT,
     `name` VARCHAR(64) NOT NULL,
     `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`)
   ) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='测试mybatis的表'
   ;
   ```

   2. 编写xml 略
   3. mapper略
   4. service 略

### 3-集成分页插件

1. 依赖

   ```xml
   <dependency>
       <groupId>com.github.pagehelper</groupId>
       <artifactId>pagehelper-spring-boot-starter</artifactId>
   </dependency>
   ```

2. 配置文件修改

   ```properties
   pagehelper.helper-dialect=mysql
   #pagehelper.reasonable=false
   #pagehelper.support-methods-arguments=false
   #pagehelper.params=count=countsql
   
   ```

3. 使用插件,在service中查询之前`PageHelper.startPage(pageNum, pageSize);`