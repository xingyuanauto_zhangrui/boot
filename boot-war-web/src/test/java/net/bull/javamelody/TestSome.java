package net.bull.javamelody;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;

import net.bull.javamelody.internal.common.I18N;
import net.bull.javamelody.internal.model.Counter;
import net.bull.javamelody.internal.model.CounterRequest;
import pers.vic.boot.util.RegexUtil;

/**
 * @description:
 * @author: Vic.xu
 * @date: 2019年11月22日 上午10:18:50
 */
public class TestSome {

	public static void main(String[] args) throws Exception {
		String path = "C:/Users/xuduo/AppData/Local/Temp/javamelody/_xuqiudong/";
		path = "D:\\desk\\183";
		String id = "httpdc50d0bd583645dbcbcbdd1a5f618ec5eb78ae0d";
		File[] files = new File(path).listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".ser.gz");
			}
		});
		List<Counter> counters = new ArrayList<Counter>();
		Map<String, CounterRequest> requestMap = new HashMap<String, CounterRequest>();
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			Counter counter = t3(file);
//			System.err.println(counter);
			counters.add(counter);

			counter.getRequests().forEach(r -> {
//				System.out.println("name: " + r.getName() + "\t\n  id : 	" + r.getId());
				String rId = r.getId();
				requestMap.put(rId, r);
				/*
				 * Map<String, Long> map = r.getChildRequestsExecutionsByRequestId();
				 * System.out.println("Child:"); map.keySet().forEach(k->{ System.out.print("\t"
				 * + k); });
				 */
			});
		}
		System.out.println(requestMap.containsKey(id));

		CounterRequest r = requestMap.get(id);
		if(r != null) {
			Map<String, Long> map = r.getChildRequestsExecutionsByRequestId();
			System.err.println(id + "  Child:   ");
			map.keySet().forEach(k -> {
				System.out.println("id:" + k);
				System.out.println("name:" + requestMap.get(k).getName());
			});
		}
		System.err.println("-------------------------------------");
		printSql(requestMap);

	}
	
	private static void printSql(Map<String, CounterRequest> requestMap) {
		for(Map.Entry<String, CounterRequest>entry : requestMap.entrySet()) {
			if(entry.getKey().startsWith("sql")) {
				System.out.println(entry.getKey());
				System.out.println(entry.getValue().getName());
			}
		}
		
		
	}

	public static Counter t3(File file) throws Exception {
		final FileInputStream in = new FileInputStream(file);
		try (ObjectInputStream input = new ObjectInputStream(new GZIPInputStream(new BufferedInputStream(in)))) {
			/*
			 * TransportFormat .createObjectInputStream(new GZIPInputStream(new
			 * BufferedInputStream(in)));
			 */
			return (Counter) input.readObject();
		}
	}

	public static String t2(String text) {
		return I18N.urlEncode(text);
	}

	public static void t1() throws IOException {
		List<String[]> result = new ArrayList<String[]>();
		String reg = "values \\('(\\d+)', '(.*?)', ";
		String f = "E:/1.sql";
		List<String> lines = FileUtils.readLines(new File(f));
		lines.forEach(str -> {
			List<String[]> list = RegexUtil.getList(str, reg, new int[] { 1, 2 });
			result.addAll(list);
		});
		System.out.println(result.size());
	}
}
