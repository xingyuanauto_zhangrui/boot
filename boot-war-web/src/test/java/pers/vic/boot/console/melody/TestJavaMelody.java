/**
 * 
 */
package pers.vic.boot.console.melody;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
import net.bull.javamelody.internal.model.Counter;
import net.bull.javamelody.internal.model.CounterRequest;

/**
 * @description: 测试javaMelody
 * @author Vic.xu
 * @date: 2020年6月2日下午3:39:29
 */
public class TestJavaMelody {

	public static void main(String[] args) throws Exception {
		String folder = "C:/Users/xuduo/AppData/Local/Temp/javamelody/_xuqiudong/";
		/*
		 * folder = "D:/desk/183"; if(counterRequestNumber(folder)) { return; }
		 */
		String graph = "httpdc50d0bd583645dbcbcbdd1a5f618ec5eb78ae0d";
		getSqlByHttpGraph(graph, folder);
	}
	
	public static boolean counterRequestNumber(String folder) throws Exception{
		File[] files = new File(folder).listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".ser.gz");
			}
		});
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			Counter counter = seriaFromFile(file);
			counter.getRequestsCount();
			System.out.println(file.getName() + " --> " +counter.getRequestsCount() + "条记录数");
		}
		return true;
	}

	/**
	 * 根据graph查询某个HTTP请求对应的SQL语句
	 * 如请求地址为:/monitoring?part=graph&graph=http23f5e2fb3d65f0e818b523518db67c54479f3f3f
	 * 则graph为:http23f5e2fb3d65f0e818b523518db67c54479f3f3f
	 * 
	 * @param graph
	 * @param folder
	 * @throws Exception
	 */
	public static void getSqlByHttpGraph(String graph, String folder) throws Exception {
		Map<String, CounterRequest> requestMap = getRequestMap(folder);

		CounterRequest request = requestMap.get(graph);
		Optional.ofNullable(request).ifPresent(r -> {
			r.getChildRequestsExecutionsByRequestId().keySet().forEach(key -> {
				System.err.println("id: " + key);
				CounterRequest counterRequest = requestMap.get(key);
				
				System.out.println("sql:\t" + counterRequest.getName() + " \n\t hits:" + counterRequest.getHits());
			});
		});

	}

	/**
	 * 查询文件夹下的序列化程Counter对象中的CounterRequest对象
	 * @param folder
	 * @return
	 * @throws Exception
	 */
	public static Map<String, CounterRequest> getRequestMap(String folder) throws Exception {
		Map<String, CounterRequest> requestMap = new HashMap<String, CounterRequest>();
		File[] files = new File(folder).listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".ser.gz");
			}
		});
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			Counter counter = seriaFromFile(file);
			System.err.println(file.getName() + " ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
			counter.getRequests().forEach(r -> {
				String rId = r.getId();
				requestMap.put(rId, r);
				System.err.println("id:" + r.getId() +":");
				System.out.println("name " + r.getName());
				System.out.println(r.getHits());
			});
			System.err.println(file.getName() + "↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");
		}
		return requestMap;
	}

	/**
	 * 从文件序列化出Counter 对象
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static Counter seriaFromFile(File file) throws Exception {
		final FileInputStream in = new FileInputStream(file);
		try (ObjectInputStream input = new ObjectInputStream(new GZIPInputStream(new BufferedInputStream(in)));) {
			/*
			 * TransportFormat .createObjectInputStream(new GZIPInputStream(new
			 * BufferedInputStream(in)));
			 */
			return (Counter) input.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
