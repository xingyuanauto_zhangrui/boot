/**
 * 
 */
package pers.vic.boot.console.student;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import pers.vic.boot.base.craw.CrawlConnect;
import pers.vic.boot.console.BaseTest;
import pers.vic.boot.console.student.model.ZblStudentBase;
import pers.vic.boot.console.student.model.ZblStudentScore;
import pers.vic.boot.console.student.service.ZblStudentScoreService;
import pers.vic.boot.util.JsonUtil;

/**
 * @description: 测试提交数据
 * @author Vic.xu
 * @date: 2020年6月30日下午1:12:22
 */
public class TestSubmitScore extends BaseTest {

	@Autowired
	private ZblStudentScoreService zblStudentScoreService;

	/*
	 * 平时：CHKPSCJ + 序号 平时 末考：CHKQMCJ + 序号 大作业 实训：CHKJNCJ + 序号 实训 综合：CHKZHCJ + 序号 综合
	 * 中考： CHKQZCJ + 序号
	 */
	

	/**
	 * 平时
	 */
	private static String DAILY_NAME_PREFIX = "CHKPSCJ";
	/** 大作业 */
	private static String TASK_NAME_PREFIX = "CHKQMCJ";
	/** 实训 */
	private static String PRACTICE_NAME_PREFIX = "CHKJNCJ";
	/** 综合 */
	private static String FINAL_NAME_PREFIX = "CHKZHCJ";;

	// 暂存请求地址
	private String url = "http://172.16.1.50:8888/jwweb/XSCJ/KCCJ_ADD_rpt_T.aspx?f=ok";

	public void buildConnect() throws IOException {
		String path = "D:/desk/zbl";
		String data = path + "/data.txt";
		String head = path + "/head.txt";
		Map<String, String> datas = readTxt(data);
		Map<String, String> heads = readTxt(head);
		String url = "http://172.16.1.50:8888/jwweb/XSCJ/KCCJ_ADD_rpt_T.aspx?f=ok";
		Connection conn = Jsoup.connect(url).ignoreContentType(true).timeout(50000);
		CrawlConnect c = new CrawlConnect(conn);

		String jsonStr = FileUtils.readFileToString(new File(path + "/json.txt"));
		@SuppressWarnings("unchecked")
		Map<String, String> json = JsonUtil.jsonToObject(jsonStr, HashMap.class);

		for (String key : heads.keySet()) {

			c.header(key, heads.get(key));
		}
		for (String key : datas.keySet()) {

			c.data(key, datas.get(key));
			if (json.containsKey(key)) {
			}
		}
	}

	@Test
	public void buildScoreForm() throws IOException {
		String clazz = "19电商2";
		ZblStudentBase lookup = new ZblStudentBase();
		lookup.setClazz(clazz);
		lookup.setSortColumn("number");
		lookup.setSortOrder("asc");
		List<ZblStudentScore> list = zblStudentScoreService.listByClazz(clazz, null);
		Map<String, String> score = buildName(list);
		
		String path = "D:/desk/zbl";
		String data = path + "/data.txt";
		String head = path + "/head.txt";
		Map<String, String> datas = readTxt(data);
		Map<String, String> heads = readTxt(head);
		int i = 0;
		for(String key : score.keySet()) {
			if(datas.containsKey(key)) {
				i++;
			}
		}
		System.out.println("包含的分数name数:" + i);
		datas.putAll(score);
		Connection conn = Jsoup.connect(url).ignoreContentType(true).timeout(50000);
		CrawlConnect c = new CrawlConnect(conn);


		for (String key : heads.keySet()) {

			c.header(key, heads.get(key));
		}
		for (String key : datas.keySet()) {

			c.data(key, datas.get(key));
		}
		
		String html = c.postHtml();
		System.out.println(html);
		

	}

	// 构建学号对应的 分数的name属性
	public Map<String, String> buildName(List<ZblStudentScore> list) {
		Map<String, String> result = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			int seq = i + 1;
			ZblStudentScore b = list.get(i);
			Map<String, String> param = new HashMap<String, String>();
			param.put(DAILY_NAME_PREFIX + seq, b.getDailyScore().toString());
			param.put(TASK_NAME_PREFIX + seq, b.getTaskScore().toString());
			param.put(PRACTICE_NAME_PREFIX + seq, b.getPracticeScore().toString());
			param.put(FINAL_NAME_PREFIX + seq, b.getFinalScore().toString());
			result.putAll(param);
		}
		System.out.println(list.size());
		System.out.println(result.size());
		return result;
	}

	public static Map<String, String> readTxt(String file) throws IOException {
		List<String> header = FileUtils.readLines(new File(file));
		Map<String, String> headMap = new HashMap<String, String>();
		header.forEach(l -> {
			String[] ls = l.split(": ");
			if (ls.length == 2) {
				headMap.put(ls[0], ls[1]);
			}
		});

		System.out.println(headMap);
		return headMap;
	}

	@Test
	public void scoreMap() {
		zblStudentScoreService.scoreMap("19电商1", null).forEach((k,v)->{
			System.out.println(k + " = " );
			JsonUtil.printJson(v);
		});
	}
	
}
