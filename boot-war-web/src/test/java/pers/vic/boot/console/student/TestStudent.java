/**
 * 
 */
package pers.vic.boot.console.student;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import pers.vic.boot.console.BaseTest;
import pers.vic.boot.console.student.model.ZblStudentBase;
import pers.vic.boot.console.student.model.ZblStudentScore;
import pers.vic.boot.console.student.service.ZblStudentBaseService;
import pers.vic.boot.console.student.service.ZblStudentScoreService;
import pers.vic.boot.util.JsonUtil;
import pers.vic.boot.util.poi.LargeDataRowCallback;
import pers.vic.boot.util.poi.LargeExcelImportFacade;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年6月29日上午9:59:32
 */
@Rollback(false)  
public class TestStudent extends BaseTest{
	
	@Autowired
	private ZblStudentBaseService studentBaseService;
	
	
	@Autowired
	private ZblStudentScoreService zblStudentScoreService;
	
	
	
	
	
	/**
	 * 导入分数
	 */
	/*
	 * 没有匹配到学号的姓名：
		张华健
		陈 宏
		王玉保
		张陈皓
		张旭
		周炎
		胡强鹏
		贺祥磊
	 */
	@Test
	public void score() {
		List<String> l2 = new ArrayList<String>();
		for(int i=2; i<=2; i++) {
			//19电商1班
//			String file = "D:/desk/期末考试/19电商1班/学生考勤情况统计-19电商1班.xlsx";
			String file = "D:/desk/期末考试/19电商"+i+"班/学生考勤情况统计-19电商"+i+"班.xlsx";
			int sheet = 5;
			int headNumer = 2;
			List<ZblStudentScore> list = new ArrayList<ZblStudentScore>();
			try(InputStream in = new FileInputStream(new File(file))){
				LargeExcelImportFacade.init(in).readSheet(sheet, headNumer, new LargeDataRowCallback() {
					
					@Override
					public void callback(Map<String, String> rowData) {
						ZblStudentScore score = new ZblStudentScore();
						String number = rowData.get("学号");
						String name =  rowData.get("姓名");
						BigDecimal s1 = toBigDecimal(rowData.get("平时成绩（40%）"));
						//实训成绩（30%）
						BigDecimal s2 = toBigDecimal(rowData.get("实训结果（30%）"));
						BigDecimal s3 = toBigDecimal(rowData.get("大作业成绩（30%）"));
						BigDecimal s4 = toBigDecimal(rowData.get("最终成绩"));
						score.setNumber(number);
						score.setDailyScore(s1);
						score.setPracticeScore(s2);
						score.setTaskScore(s3);
						score.setFinalScore(s4);
						score.setName(name);
						if(StringUtils.isAnyBlank(name, number)) {
							return;
						}
						list.add(score);
						
					}
				});
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			list.forEach(s->{
				
				ZblStudentBase base = studentBaseService.getbyName(s.getName());
				JsonUtil.printJson(s);
				if(base!=null) {
					zblStudentScoreService.updatePracticeScore(base.getNumber(), s.getPracticeScore());
				}
				/*
				if(base != null) {
					String number2 = s.getNumber();
					String number = base.getNumber();
					base.setNumber2(number2);
					s.setNumber(number);
					studentBaseService.update(base);
					zblStudentScoreService.save(s);
					
				}else {
					//没有学号的也保存下来吧
					zblStudentScoreService.save(s);
					l2.add(s.getName());
				}
				*/
			});
			
		}
		System.out.println("没有匹配到学号的姓名：");
		l2.forEach(System.out::println);
	}
	
	
	public static BigDecimal toBigDecimal(String str) {
		if(StringUtils.isEmpty(str)) {
			return BigDecimal.ZERO;
		}
		try {
			return new BigDecimal(str).setScale(2, BigDecimal.ROUND_HALF_UP);
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return BigDecimal.ZERO; 
		
	}
	@Test
	public void init() {
		for(int i=1; i<=4; i++) {
			String file = "D:/desk/期末考试/经济学——平时分(1).xlsx";
			String clazz="19电商" + i;
			int sheet = i;
			int headNumber = 2;
			List<ZblStudentBase> list = testImport(file, clazz, sheet, headNumber);
			System.out.println("start.....................");
			list.forEach(studentBaseService::insert);
			System.out.println("end.....................");
		}
		
	}
	
	public static List<ZblStudentBase>  testImport(String file, String clazz, int sheet, int headNumber) {
		 List<ZblStudentBase> list = new ArrayList<ZblStudentBase>();
		try(InputStream in = new FileInputStream(new File(file))){
			
			LargeExcelImportFacade.init(in).readSheet(sheet, headNumber, new LargeDataRowCallback() {
				
				@Override
				public void callback(Map<String, String> rowData) {
					ZblStudentBase base = new ZblStudentBase();
					base.setClazz(clazz);
					String name = rowData.get("姓名");
					String number = rowData.get("学号");
					base.setName(name);
					base.setNumber(number);
					if(StringUtils.isAnyBlank(name, number)) {
						return;
					}
					
					list.add(base);
					
				}
			});
			System.out.println(list.size());
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

}
