package pers.vic.boot.console.develop.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 开发计划 实体类
 * 
 * @author Vic.xu
 */
public class DevelopPlan extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 计划名称
	 */
	private String name;

	/**
	 * 开始时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	/**
	 * 结束时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	/**
	 * 项目类别
	 */
	private Integer category;

	/**
	 * 进度百分比
	 */
	private Integer progress;

	/**
	 * 状态:0-未开始 1-待定,2-进行中,3-暂停,4-完成
	 */
	private Integer status;

	/**
	 * 说明
	 */
	private String remark;

	/***************** set|get start **************************************/
	/**
	 * set：计划名称
	 */
	public DevelopPlan setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：计划名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：开始时间
	 */
	public DevelopPlan setStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}

	/**
	 * get：开始时间
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * set：结束时间
	 */
	public DevelopPlan setEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}

	/**
	 * get：结束时间
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * set：项目类别
	 */
	public DevelopPlan setCategory(Integer category) {
		this.category = category;
		return this;
	}

	/**
	 * get：项目类别
	 */
	public Integer getCategory() {
		return category;
	}

	/**
	 * set：进度百分比
	 */
	public DevelopPlan setProgress(Integer progress) {
		this.progress = progress;
		return this;
	}

	/**
	 * get：进度百分比
	 */
	public Integer getProgress() {
		return progress;
	}

	/**
	 * set：状态:0-未开始 1-待定,2-进行中,3-暂停,4-完成
	 */
	public DevelopPlan setStatus(Integer status) {
		this.status = status;
		return this;
	}

	/**
	 * get：状态:0-未开始 1-待定,2-进行中,3-暂停,4-完成
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * set：说明
	 */
	public DevelopPlan setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：说明
	 */
	public String getRemark() {
		return remark;
	}
	/***************** set|get end **************************************/
}
