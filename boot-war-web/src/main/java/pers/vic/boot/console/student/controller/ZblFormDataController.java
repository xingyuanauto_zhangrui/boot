package pers.vic.boot.console.student.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.controller.BaseController;

import pers.vic.boot.console.student.service.ZblFormDataService;  
import pers.vic.boot.console.student.model.ZblFormData;

/**
 * @description:表单数据填写结果表 控制层
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
@RestController
@RequestMapping("/student/zblFormData")
public class ZblFormDataController extends BaseController<ZblFormDataService, ZblFormData>{

}
