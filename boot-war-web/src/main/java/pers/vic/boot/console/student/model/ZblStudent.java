package pers.vic.boot.console.student.model;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 学号表-学生唯一标识 并用于登陆 实体类
 * 
 * @author Vic.xu
 */
public class ZblStudent extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 学号
	 */
	private String studentNo;

	/**
	 * 明文密码
	 */
	private String password;

	/**
	 * 姓名
	 */
	private String name;

	/***************** set|get start **************************************/
	/**
	 * set：学号
	 */
	public ZblStudent setStudentNo(String studentNo) {
		this.studentNo = studentNo;
		return this;
	}

	/**
	 * get：学号
	 */
	public String getStudentNo() {
		return studentNo;
	}

	/**
	 * set：明文密码
	 */
	public ZblStudent setPassword(String password) {
		this.password = password;
		return this;
	}

	/**
	 * get：明文密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set：姓名
	 */
	public ZblStudent setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：姓名
	 */
	public String getName() {
		return name;
	}
	/***************** set|get end **************************************/
}
