package pers.vic.boot.console.student.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.vo.BooleanWithMsg;
import pers.vic.boot.base.vo.Select2VO;
import pers.vic.boot.console.student.service.ZblStudentBaseService;  
import pers.vic.boot.console.student.model.ZblStudentBase;

/**
 * @description:学生基本信息 控制层
 * @author Vic.xu
 * @date: 2020-06-29 09:17
 */
@RestController
@RequestMapping("/student/zblStudentBase")
public class ZblStudentBaseController extends BaseController<ZblStudentBaseService, ZblStudentBase>{

	/**
	 * 导入学生基本信息
	 * @param file Excel 文件
	 * @param clazz 班级
	 */
	@PostMapping("/import")
	public BaseResponse<?> importBaseInfo(@RequestParam("upfile")MultipartFile file, String clazz, int year) {
		BooleanWithMsg msg = service.importBaseInfo(file, clazz, year);
		return BaseResponse.success().appendMsg(msg.getMessage());
	}
	
	//班级下拉框
	@PostMapping("/clazzSelect")
	public BaseResponse<?> clazzSelect() {
		List<Select2VO> list = service.clazzSelect();
		return BaseResponse.success(list);
	}
	
	//年级下拉框
	@PostMapping("/yearSelect")
	public BaseResponse<?> yearSelect() {
		List<Select2VO> list = service.yearSelect();
		return BaseResponse.success(list);
	}
}
