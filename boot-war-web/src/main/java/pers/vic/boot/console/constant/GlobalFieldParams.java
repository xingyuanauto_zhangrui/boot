/**
 * 
 */
package pers.vic.boot.console.constant;

/**
 * @description: 当前项目的一些常量
 * @author Vic.xu
 * @date: 2020年9月8日下午2:03:02
 */
public final class GlobalFieldParams {

	/**
	 * 验证码有效时长 秒 (5分钟)
	 */
	public static final long CODE_DURATION = 5 * 60;

	/**
	 * 验证码存入REDIS的前缀
	 */
	public static final String CODE_PREFIX = "msmcode:";

}
