package pers.vic.boot.console.student.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.vo.BooleanWithMsg;
import pers.vic.boot.base.vo.Select2VO;
import pers.vic.boot.console.student.service.ZblStudentScoreService;
import pers.vic.boot.console.student.model.ZblStudentScore;

/**
 * @description: 控制层
 * @author Vic.xu
 * @date: 2020-06-29 09:35
 */
@RestController
@RequestMapping("/student/zblStudentScore")
public class ZblStudentScoreController extends BaseController<ZblStudentScoreService, ZblStudentScore> {

	// 查询某个班级的成绩
	@RequestMapping("/scoreMap")
	public BaseResponse<?> scoreMap(String clazz, String term) {
		Map<String, ZblStudentScore> resMap = service.scoreMap(clazz, term);
		return BaseResponse.success(resMap).appendMsg("当前条件查询出的总条数为:" + resMap.size());
	}

	// 学期下拉框
	@PostMapping("/termSelect")
	public BaseResponse<?> clazzSelect() {
		List<Select2VO> list = service.termSelect();
		return BaseResponse.success(list);
	}

	/**
	 * 导入学生成绩
	 * 
	 * @param file       Excel 文件
	 * @param term       学期
	 * @param numberType 1-学号 2-学号2
	 */
	@PostMapping("/import")
	public BaseResponse<?> importBaseInfo(@RequestParam("upfile") MultipartFile file, String term, int numberType) {
		BooleanWithMsg msg = service.importScore(file, term, numberType);
		return BaseResponse.success().appendMsg(msg.getMessage());
	}
}
