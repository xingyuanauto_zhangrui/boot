package pers.vic.boot.console.student.model;


import pers.vic.boot.base.model.BaseEntity;


/**
 * 学生基本信息 实体类
 * 
 * @author Vic.xu
 */
public class ZblStudentBase extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

								/**
	 * 名称
	 */
	 							    private String name;
	
						/**
	 * 学号
	 */
	 							    private String number;
	
						/**
	 * 学号2
	 */
	 							    private String number2;
	
						/**
	 * 年级
	 */
	 							    private Integer year;
	
						/**
	 * 班级
	 */
	 							    private String clazz;
	
																			
    /***************** set|get  start **************************************/
			    	    	    	/**
	 * set：名称
	 */
	public ZblStudentBase setName(String name) {
		this.name = name;
		return this;
	}
    /**
	* get：名称
	*/
		    public String getName() {
		return name;
	}
		    	    	/**
	 * set：学号
	 */
	public ZblStudentBase setNumber(String number) {
		this.number = number;
		return this;
	}
    /**
	* get：学号
	*/
		    public String getNumber() {
		return number;
	}
		    	    	/**
	 * set：学号2
	 */
	public ZblStudentBase setNumber2(String number2) {
		this.number2 = number2;
		return this;
	}
    /**
	* get：学号2
	*/
		    public String getNumber2() {
		return number2;
	}
		    	    	/**
	 * set：年级
	 */
	public ZblStudentBase setYear(Integer year) {
		this.year = year;
		return this;
	}
    /**
	* get：年级
	*/
		    public Integer getYear() {
		return year;
	}
		    	    	/**
	 * set：班级
	 */
	public ZblStudentBase setClazz(String clazz) {
		this.clazz = clazz;
		return this;
	}
    /**
	* get：班级
	*/
		    public String getClazz() {
		return clazz;
	}
		    	    	    	    	    	    	    	    	        /***************** set|get  end **************************************/
}
