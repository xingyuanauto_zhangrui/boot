package pers.vic.boot.console.student.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblBaseFormDetail;
/**
 * @description:表单设计详情表 Mapper
 * @author Vic.xu
 * @date: 2020-02-02 18:10
 */
public interface ZblBaseFormDetailMapper extends BaseMapper<ZblBaseFormDetail> {
	
}
