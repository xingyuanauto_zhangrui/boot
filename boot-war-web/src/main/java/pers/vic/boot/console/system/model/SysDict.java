package pers.vic.boot.console.system.model;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 数据字典表 实体类
 * 
 * @author Vic.xu
 */
public class SysDict extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 父字典id
	 */
	private Integer pid;

	/**
	 * 编码
	 */
	private String code;
	
	/**
	 * 系统编码: pcode-code ,方便查找和删除子节点
	 */
	private String sysCode;

	/**
	 * 字典名称
	 */
	private String name;

	/**
	 * 字典值
	 */
	private String value;

	/**
	 * 排序
	 */
	private Integer sort;

	/**
	 * 说明
	 */
	private String remark;

	/**
	 * set：父字典id
	 */
	public SysDict setPid(Integer pid) {
		this.pid = pid;
		return this;
	}

	/**
	 * get：父字典id
	 */
	public Integer getPid() {
		return pid;
	}

	/**
	 * set：编码
	 */
	public SysDict setCode(String code) {
		this.code = code;
		return this;
	}

	/**
	 * get：编码
	 */
	public String getCode() {
		return code;
	}
	
	

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	/**
	 * set：字典名称
	 */
	public SysDict setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：字典名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：字典值
	 */
	public SysDict setValue(String value) {
		this.value = value;
		return this;
	}

	/**
	 * get：字典值
	 */
	public String getValue() {
		return value;
	}

	/**
	 * set：排序
	 */
	public SysDict setSort(Integer sort) {
		this.sort = sort;
		return this;
	}

	/**
	 * get：排序
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * set：说明
	 */
	public SysDict setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：说明
	 */
	public String getRemark() {
		return remark;
	}
}
