package pers.vic.boot.console;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @description:
 * @author VIC.xu
 * @date: 2020年1月10日 下午5:33:52
 */
@SpringBootApplication
//MAPPER 接口类包扫描  
@MapperScan(basePackages = "pers.vic.boot.console.**.mapper")
//开启dubbo的自动配置
//@EnableDubboConfiguration
@EnableTransactionManagement(proxyTargetClass = true)
@RestController
@EnableCaching
@EnableScheduling
public class ConsoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsoleApplication.class, args);
	}
	
}
