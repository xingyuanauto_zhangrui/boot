/**
 * 
 */
package pers.vic.boot.console.test.mybatis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pers.vic.boot.base.model.BaseResponse;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年7月1日上午9:30:51
 */
@Controller
@RequestMapping("/test/some")
public class TestSomeController {
	
	@ResponseBody
	@RequestMapping("/add")
	public BaseResponse<?> add(HttpServletRequest request) {
		Map<String, Object>  params = getParam(request);
		System.err.println("----------------------------------------\n"+ params);
		return BaseResponse.success(params);
	}
	
	@RequestMapping("/data")
	@ResponseBody
	public String data() {
		String data = "(function(){\r\n" + 
				"	\r\n" + 
				"	var url = window.location;\r\n" + 
				"	console.info(url);\r\n" + 
				"	console.info(\"你是最棒的 \" + new Date().getTime());\r\n" + 
				"	\r\n" + 
				"	var url = \"http://localhost:10083/test/some/add\";\r\n" + 
				"	var data = \"aa=123&bb=so shi wo\";\r\n" + 
				"	var xhr=new XMLHttpRequest();\r\n" + 
				"	xhr.open('POST',url,false);\r\n" + 
				"	// 添加http头，发送信息至服务器时内容编码类型\r\n" + 
				"	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');\r\n" + 
				"	xhr.onreadystatechange=function(){\r\n" + 
				"		if (xhr.readyState==4){\r\n" + 
				"			if (xhr.status==200 || xhr.status==304){\r\n" + 
				"				// console.log(xhr.responseText);\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"	}\r\n" + 
				"	xhr.send(data);\r\n" + 
				"})();";
		return data;
	}

	private Map<String, Object>  getParam(HttpServletRequest request) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
	    Map<String, String[]> map=request.getParameterMap();  
	    Set<Entry<String, String[]>> keSet=map.entrySet();  
	    for(Iterator<Entry<String, String[]>> itr=keSet.iterator();itr.hasNext();){  
	    	Entry<String, String[]> me= itr.next();  
	        String key=me.getKey();  
	        String[] value=me.getValue();  
	         
	        paramMap.put(key, String.join(",", value));
	    }  
	    return paramMap;

	}
	
}
