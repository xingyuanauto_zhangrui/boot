package pers.vic.boot.console.test.mybatis.model;

import pers.vic.boot.base.model.BaseEntity;

public class TestMybatis extends BaseEntity{
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	private int age;
	
	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public TestMybatis setName(String name) {
		this.name = name;
		return this;
	}
	
	

}
