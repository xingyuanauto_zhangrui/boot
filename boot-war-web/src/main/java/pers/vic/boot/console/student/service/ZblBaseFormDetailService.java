package pers.vic.boot.console.student.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.student.mapper.ZblBaseFormDetailMapper;  
import pers.vic.boot.console.student.model.ZblBaseFormDetail;

/**
 * @description:表单设计详情表 Service
 * @author Vic.xu
 * @date: 2020-02-02 18:10
 */
@Service
public class ZblBaseFormDetailService extends BaseService<ZblBaseFormDetailMapper, ZblBaseFormDetail>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
