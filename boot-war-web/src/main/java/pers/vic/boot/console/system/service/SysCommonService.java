package pers.vic.boot.console.system.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import pers.vic.boot.base.vo.Select2VO;
import pers.vic.boot.console.system.mapper.SysCommonMapper;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月3日 上午11:33:06
 */
@Service
public class SysCommonService {
	
	@Resource
	private SysCommonMapper sysCommonMapper;

	/**
	 * 角色下拉框
	 * @return
	 */
	public List<Select2VO> roleSelect() {
		return sysCommonMapper.roleSelect();
	}
	/**
	 * 字典下拉框
	 * @return
	 */
	public List<Select2VO> dictSelect(String pcode) {
		return sysCommonMapper.dictSelect(pcode);
	}

}
