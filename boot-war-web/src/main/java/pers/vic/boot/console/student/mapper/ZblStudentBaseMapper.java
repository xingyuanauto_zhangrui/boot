package pers.vic.boot.console.student.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblStudentBase;
/**
 * @description:学生基本信息 Mapper
 * @author Vic.xu
 * @date: 2020-06-29 09:17
 */
public interface ZblStudentBaseMapper extends BaseMapper<ZblStudentBase> {

	/**
	 * @param name
	 * @return
	 */
	ZblStudentBase getbyName(@Param("name")String name);
	/**
	 * @param number
	 * @return
	 */
	ZblStudentBase getByNumber2(@Param("number2")String number);

	/**
	 * 新增或修正学号
	 */
	void insertOrUpdate(ZblStudentBase entity);

	/**
	 * 根据number or number2获取实体
	 * @return
	 */
	ZblStudentBase findByEntity(ZblStudentBase entity);

	/**
	 *班级下拉框
	 */
	List<String> clazzList();

	/**
	 * 年级下拉框
	 */
	List<Integer> yearList();

	
	
}
