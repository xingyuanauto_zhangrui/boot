package pers.vic.boot.console.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.system.model.SysUser;

/**
 * 系统用户表 Mapper
 * 
 * @author Vic.xu
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
	
	/**
	 * 判断username是否重复
	 * @return
	 */
	boolean checkUsername(@Param("id")Integer id, @Param("username")String username);
	
	/**
	 * 用户已经拥有的角色
	 */
	List<Integer> userRoleIds(@Param("userId")Integer userId);
	
	/**
	 * 新增用户和角色关系
	 */
	void addUserRole(@Param("userId")int userId,  @Param("ids") List<Integer> needAdd);
    
	/**
	 * 删除用户和角色关系
	 */
	void deleteUserRole(@Param("userId")int userId,  @Param("ids") List<Integer> needDel);

    /**
     * 根据用户id删除全部和角色关系
     */
	void deleteUserRoleByUserId(@Param("userId")int userId);

	/**
	 * 	根据用户名获取用户
	 */
	SysUser findUserByUsername(@Param("username")String username);
}
