/**
 * 
 */
package pers.vic.boot.console.test.requet;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年11月13日上午10:34:35
 */
@RestController
@RequestMapping("/test/mapping/**")
public class RequetUrlCollectionController {

	
	@RequestMapping("/other/*")
	public Object other() {
		return LocalTime.now();
	}
	
	@RequestMapping("/all")
	public Object all(HttpServletRequest request) {
		ServletContext servletContext = request.getSession().getServletContext();
		if (servletContext == null) {
			return null;
		}
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		// 请求url和处理方法的映射
		List<RequestToMethodItem> requestToMethodItemList = new ArrayList<RequestToMethodItem>();
		// 获取所有的RequestMapping
		Map<String, HandlerMapping> allRequestMappings = BeanFactoryUtils.beansOfTypeIncludingAncestors(appContext,
				HandlerMapping.class, true, false);

		for (HandlerMapping handlerMapping : allRequestMappings.values()) {
			// 本项目只需要RequestMappingHandlerMapping中的URL映射
			if (handlerMapping instanceof RequestMappingHandlerMapping) {
				RequestMappingHandlerMapping requestMappingHandlerMapping = (RequestMappingHandlerMapping) handlerMapping;
				Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping
						.getHandlerMethods();
				for (Map.Entry<RequestMappingInfo, HandlerMethod> requestMappingInfoHandlerMethodEntry : handlerMethods
						.entrySet()) {
					RequestMappingInfo requestMappingInfo = requestMappingInfoHandlerMethodEntry.getKey();
					HandlerMethod mappingInfoValue = requestMappingInfoHandlerMethodEntry.getValue();

					RequestMethodsRequestCondition methodCondition = requestMappingInfo.getMethodsCondition();
					String requestType = methodCondition.getMethods().stream().map(RequestMethod::name)
							.collect(Collectors.joining("-", "<", ">"));

					PatternsRequestCondition patternsCondition = requestMappingInfo.getPatternsCondition();
					String requestUrl = patternsCondition.getPatterns().stream()
							.collect(Collectors.joining("-", "(", ")"));
					String controllerName = mappingInfoValue.getBeanType().toString();
					String requestMethodName = mappingInfoValue.getMethod().getName();
					Class<?>[] methodParamTypes = mappingInfoValue.getMethod().getParameterTypes();
					RequestToMethodItem item = new RequestToMethodItem(requestUrl, requestType, controllerName,
							requestMethodName, methodParamTypes);

					requestToMethodItemList.add(item);
				}
				break;
			}
		}
		System.out.println(requestToMethodItemList.size());
		return requestToMethodItemList;
	}

}
