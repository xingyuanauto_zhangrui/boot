package pers.vic.boot.console.student.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblStudentScore;

/**
 * @description: Mapper
 * @author Vic.xu
 * @date: 2020-06-29 09:35
 */
public interface ZblStudentScoreMapper extends BaseMapper<ZblStudentScore> {

	/**
	 * 查询出一个班级全部学生的成绩 根据number降序
	 * @param clazz 班级
	 * @param term  学期
	 */
	List<ZblStudentScore> listByClazz(@Param("clazz") String clazz, @Param("term") String term);

	/**
	 * 更新实训分数
	 * @param number
	 * @param score
	 */
	void updatePracticeScore(@Param("number") String number,@Param("score")  BigDecimal score);

	/**
	 * 根据学期和学号查询是否已经录入
	 * @return
	 */
	ZblStudentScore findByEntity(ZblStudentScore score);

	/**
	 * @return 学期
	 */
	List<String> termList();

}
