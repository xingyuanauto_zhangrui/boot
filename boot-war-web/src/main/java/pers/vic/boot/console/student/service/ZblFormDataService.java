package pers.vic.boot.console.student.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.student.mapper.ZblFormDataMapper;  
import pers.vic.boot.console.student.model.ZblFormData;

/**
 * @description:表单数据填写结果表 Service
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
@Service
public class ZblFormDataService extends BaseService<ZblFormDataMapper, ZblFormData>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
