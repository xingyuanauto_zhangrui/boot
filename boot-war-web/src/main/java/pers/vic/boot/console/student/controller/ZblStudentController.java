package pers.vic.boot.console.student.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.controller.BaseController;

import pers.vic.boot.console.student.service.ZblStudentService;  
import pers.vic.boot.console.student.model.ZblStudent;

/**
 * @description:学号表-学生唯一标识 并用于登陆 控制层
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
@RestController
@RequestMapping("/student/zblStudent")
public class ZblStudentController extends BaseController<ZblStudentService, ZblStudent>{

}
