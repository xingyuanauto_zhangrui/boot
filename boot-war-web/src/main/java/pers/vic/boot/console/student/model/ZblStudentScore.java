package pers.vic.boot.console.student.model;

import java.math.BigDecimal;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 实体类
 * 
 * @author Vic.xu
 */
public class ZblStudentScore extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	private String name;

	/**
	 * 学号
	 */
	private String number;

	/**
	 * 平时成绩
	 */
	private BigDecimal dailyScore;

	/**
	 * 实训成绩
	 */
	private BigDecimal practiceScore;

	/**
	 * 大作业成绩
	 */
	private BigDecimal taskScore;

	/**
	 * 最终成绩
	 */
	private BigDecimal finalScore;

	/**
	 * 学期(类似2020-01)
	 */
	private String term;
	
	/* ***************************************************************** */
	//班级
	private String clazz;

	/***************** set|get start **************************************/
	/**
	 * set：学号
	 */
	public ZblStudentScore setNumber(String number) {
		this.number = number;
		return this;
	}

	/**
	 * get：学号
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * set：平时成绩
	 */
	public ZblStudentScore setDailyScore(BigDecimal dailyScore) {
		this.dailyScore = dailyScore;
		return this;
	}

	/**
	 * get：平时成绩
	 */
	public BigDecimal getDailyScore() {
		return dailyScore == null ? BigDecimal.ZERO: dailyScore;
	}

	/**
	 * set：实训成绩
	 */
	public ZblStudentScore setPracticeScore(BigDecimal practiceScore) {
		this.practiceScore = practiceScore;
		return this;
	}

	/**
	 * get：实训成绩
	 */
	public BigDecimal getPracticeScore() {
		return practiceScore  == null ? BigDecimal.ZERO: practiceScore;
	}

	/**
	 * set：大作业成绩
	 */
	public ZblStudentScore setTaskScore(BigDecimal taskScore) {
		this.taskScore = taskScore;
		return this;
	}

	/**
	 * get：大作业成绩
	 */
	public BigDecimal getTaskScore() {
		return taskScore == null ? BigDecimal.ZERO: taskScore;
	}

	/**
	 * set：最终成绩
	 */
	public ZblStudentScore setFinalScore(BigDecimal finalScore) {
		this.finalScore = finalScore;
		return this;
	}

	/**
	 * get：最终成绩
	 */
	public BigDecimal getFinalScore() {
		return finalScore == null ? BigDecimal.ZERO: finalScore;
	}

	/**
	 * set：学期(类似2020-01)
	 */
	public ZblStudentScore setTerm(String term) {
		this.term = term;
		return this;
	}

	/**
	 * get：学期(类似2020-01)
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the clazz
	 */
	public String getClazz() {
		return clazz;
	}

	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	
	/***************** set|get end **************************************/
	
	
}
