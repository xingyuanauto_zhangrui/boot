package pers.vic.boot.console.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.system.model.SysMenu;
import pers.vic.boot.console.system.model.SysRole;
/**
 * @description:角色表 Mapper
 * @author Vic.xu
 * @date: 2019-58-29 08:58
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

	/**
	 * 全部菜单:选中当前角色拥有的
	 * @param id 角色id
	 */
	List<SysMenu> roleMenus(@Param("roleId")Integer roleId);
	
	/**
	 * 当前角色对应的菜单id集合
	 */
	List<Integer> roleMenuIds(@Param("roleId")Integer roleId);

	/**新建角色和菜单关系*/
	void addRoleMenu(@Param("roleId")int roleId,  @Param("ids") List<Integer> needAdd);

	/**删除角色和菜单的关系*/
	void deleteRoleMenu(@Param("roleId")int roleId,  @Param("ids")List<Integer> needDelete);

	/**根据角色id删除全部关系*/
	void deleteRoleMenuByRoleId(@Param("roleId")int roleId);
	
	/**
	 * 用户有所的角色
	 */
	List<SysRole> findUserRoles(@Param("userId")Integer userId);
	
	/**
	 * 用户的权限
	 */
	List<String> findUserPermissions(@Param("userId")Integer userId);
	
}
