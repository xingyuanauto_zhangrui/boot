package pers.vic.boot.console.student.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.student.mapper.ZblBaseFormMapper;  
import pers.vic.boot.console.student.model.ZblBaseForm;

/**
 * @description:表单基本信息 Service
 * @author Vic.xu
 * @date: 2020-02-02 18:04
 */
@Service
public class ZblBaseFormService extends BaseService<ZblBaseFormMapper, ZblBaseForm>{

	@Override
	protected boolean hasAttachment() {
		return false;
	}

}
