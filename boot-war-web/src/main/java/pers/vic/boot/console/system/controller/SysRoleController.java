package pers.vic.boot.console.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.system.model.SysMenu;
import pers.vic.boot.console.system.model.SysRole;
import pers.vic.boot.console.system.service.SysRoleService;

import java.util.List;

/**
 * @description:角色表 控制层
 * @author Vic.xu
 * @date: 2019-58-29 08:58
 */
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController<SysRoleService, SysRole> {

    /**
     * 角色对应的菜单关系:所有菜单,角色拥有的权限则选中
     * @return
     */
    @GetMapping(value = "/roleMenus")
    public BaseResponse<?> roleMenus(Integer id) {
        List<SysMenu> roleMenus = service.roleMenus(id);
        return BaseResponse.success(roleMenus);
    }

    /**
     * 保存
     * @param entity
     * @return
     */
    @PostMapping(value = "/save", params = {"menuIds"})
    public BaseResponse<?> save(SysRole entity, String menuIds) {
        service.save(entity, menuIds);
        return BaseResponse.success(entity);
    }

    /**
     * 保存
     * @param entity
     * @return
     */
    @Override
    @PostMapping(value = "/save")
    public BaseResponse<?> save(SysRole entity) {
        service.save(entity);
        return BaseResponse.success(entity);
    }

}
