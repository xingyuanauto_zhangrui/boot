package pers.vic.boot.console.config.captcha;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.UUID;


/**
 * @description: 验证码生成
 * @author: Vic.xu
 * @date: 2020年2月18日 下午1:47:42
 */
@Component
public class KaptchGenerate {

    @Autowired
    private DefaultKaptcha producer;

    /**
     * 验证码凭证前缀
     */
    private static String TOKEN_PREFIX = "captcha_";

    /**
     * @description: 生成验证码对象
     * @author: Vic.xu
     * @date: 2020年2月18日 下午1:53:40
     * @return
     * @throws IOException
     */
    public Base64CaptchaModel generateWithoutToken() throws IOException {
        String token = UUID.randomUUID().toString().replace("-", "");
        token = TOKEN_PREFIX + token;
        return generateWithToken(token);
    }

    /**
     *
     * @description: 生成验证码对象
     * @author: Vic.xu
     * @date: 2020年2月18日 下午1:53:56
     * @param token 验证码凭证
     * @return
     * @throws IOException
     */
    public Base64CaptchaModel generateWithToken(String token) throws IOException {
        String text = producer.createText();
        BufferedImage image = producer.createImage(text);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        // 对字节数组Base64编码
        Encoder encoder = Base64.getEncoder();
        String base64Image = new String(encoder.encode(outputStream.toByteArray()));
        return new Base64CaptchaModel(token, text, "data:image/jpg;base64," + base64Image);
    }


}
