package pers.vic.boot.console.student.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.base.vo.BooleanWithMsg;
import pers.vic.boot.base.vo.Select2VO;
import pers.vic.boot.console.student.mapper.ZblStudentBaseMapper;
import pers.vic.boot.console.student.model.ZblStudentBase;
import pers.vic.boot.util.poi.LargeDataRowCallback;
import pers.vic.boot.util.poi.LargeExcelImportFacade;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description:学生基本信息 Service
 * @author Vic.xu
 * @date: 2020-06-29 09:17
 */
@Service
public class ZblStudentBaseService extends BaseService<ZblStudentBaseMapper, ZblStudentBase> {

    private static Logger logger = LoggerFactory.getLogger(ZblStudentBaseService.class);

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    public ZblStudentBase getbyName(String name) {
        return mapper.getbyName(name);
    }

    /**
     * @param number
     */
    public ZblStudentBase getByNumber2(String number) {
        return mapper.getByNumber2(number);

    }

    /**
     * 导入: 根据姓名匹配出学号以及L学号 excel 列: 姓名 学号 姓名2 L学号
     *
     * @param file
     * @param clazz
     */
    public BooleanWithMsg importBaseInfo(MultipartFile file, String clazz, int year) {
        List<ZblStudentBase> list = new ArrayList<ZblStudentBase>();
        //姓名2->L学号  map
        Map<String, String> name2Number2Map = new HashMap<String, String>();
        try (InputStream in = file.getInputStream()) {
            LargeExcelImportFacade.init(in).readSheet(1, 2, new LargeDataRowCallback() {
                @Override
                public void callback(Map<String, String> rowData) {
                    ZblStudentBase base = new ZblStudentBase();
                    base.setClazz(clazz);
                    base.setYear(year);
                    String name = rowData.get("姓名");
                    String number = rowData.get("学号");
                    String name2 = rowData.get("姓名2");
                    String number2 = rowData.get("L学号");
                    if (StringUtils.isBlank(name) && StringUtils.isBlank(name2)) {
                        return;
                    }
                    name2Number2Map.put(StringUtils.trim(name2), StringUtils.trim(number2));

                    name = StringUtils.isBlank(name) ? name2 : name;
                    base.setName(StringUtils.trim(name));
                    base.setNumber(StringUtils.trim(number));
                    list.add(base);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.stream().map(s -> {
            //设置number2
            s.setNumber2(name2Number2Map.get(s.getName()));
            return s;
        }).forEach(this::insertOrUpdate);
        String msg = "成功导入" + list.size() + "条学生信息数据";
        logger.info(msg);
        return BooleanWithMsg.success().setMessage(msg);
    }

    /**
     * 新增或修正学号 ALTER TABLE zbl_student_base ADD UNIQUE
     * unique_number_name(NAME,number); ALTER TABLE zbl_student_base ADD UNIQUE
     * unique_number2_name(NAME,number2);
     *
     * 本来准备使用on duplicate 去实现新增或或者冲突的时候更新,
     * 但是, 首先整个非SQL标准而是MYSQL独有,
     *       另外在更新的时候依然会占用id序列自增 ;
     * 故改为代码判断是新增还是修改
     */
    public void insertOrUpdate(ZblStudentBase entity) {
        ZblStudentBase old = mapper.findByEntity(entity);
        if (old == null) {
            mapper.insert(entity);
            return;
        }
        // 如果数据库中缺失的 就 补全
        if (StringUtils.isEmpty(old.getNumber()) && StringUtils.isNotEmpty(entity.getNumber())) {
            old.setNumber(entity.getNumber());
            mapper.insertOrUpdate(old);
            return;
        }
        if (StringUtils.isEmpty(old.getNumber2()) && StringUtils.isNotEmpty(entity.getNumber2())) {
            old.setNumber2(entity.getNumber2());
            mapper.insertOrUpdate(old);
            return;
        }
    }

    /**
     * 班级下拉框
     *
     * @return
     */
    public List<Select2VO> clazzSelect() {
        List<String> clazzList = mapper.clazzList();
        return clazzList.stream().map(clazz -> {
            return new Select2VO(clazz, clazz);
        }).collect(Collectors.toList());
    }

    /**
     * 年级下拉框
     */
    public List<Select2VO> yearSelect() {
        List<Integer> yearList = mapper.yearList();
        return yearList.stream().map(year -> {
            return new Select2VO(year + "", year + "");
        }).collect(Collectors.toList());
    }


}
