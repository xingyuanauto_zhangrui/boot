/**
 * 
 */
package pers.vic.boot.console.util;

import pers.vic.boot.console.constant.GlobalFieldParams;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年9月8日下午2:33:40
 */
public class CurrentCommonUtil {
	
	private CurrentCommonUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	/**
	 * 获得存入REDIS的验证码的key
	 * @param code 验证码的校验码
	 * @return
	 */
	public static String codeKey(String token) {
		return GlobalFieldParams.CODE_PREFIX + token;
	}

}
