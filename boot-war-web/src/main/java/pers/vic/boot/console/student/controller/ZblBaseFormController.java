package pers.vic.boot.console.student.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.vic.boot.base.controller.BaseController;

import pers.vic.boot.console.student.service.ZblBaseFormService;  
import pers.vic.boot.console.student.model.ZblBaseForm;

/**
 * @description:表单基本信息 控制层
 * @author Vic.xu
 * @date: 2020-02-02 18:04
 */
@RestController
@RequestMapping("/student/zblBaseForm")
public class ZblBaseFormController extends BaseController<ZblBaseFormService, ZblBaseForm>{

	/*
	 * 1. 进入基本表单列表页面 显示需要提交的表单
	 * 2. 编辑基本表单
	 * 3. 编辑表单的字段(表单详情列表)
	 * 4. 根据表单详情列表 填写表单
	 * 5. 已经填写的表单的列表
	 * 6. 导出已经填写的表单
	 */
	
	/*
	 * 1. 学生登录
	 * 2. 修改秘密
	 * 3. 用户身份
	 */
	
	
	
	
}
