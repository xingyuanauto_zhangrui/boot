package pers.vic.sso.common.constant;

/**
 * 描述:
 *   SSO的一些常量
 * @author Vic.xu
 * @date 2021-10-29 11:20
 */
public class SsoConstant {

    /**
     * Ticket Granted Ticket 俗称大令牌，或者说票根，他可以签发ST
     */
    /**
     * TGC：Ticket Granted Cookie（cookie中的value），存在Cookie中，根据他可以找到TGT。
     */
    public static final String TGC ="TGC";
    /**
     * ST：Service Ticket （小令牌），是TGT生成的，默认是用一次就生效了。也即ticket/accessToken值。
     */

    /**
     * 服务端回调客户端地址参数名称
     */
    public static final String REDIRECT_URI = "redirectUri";

    // 登录页
    public static final String LOGIN_PATH = "/login";

    /**
     * 服务端单点登出回调客户端登出参数名称
     */
    public static final String LOGOUT_PARAMETER_NAME = "logoutRequest";

    /**
     * 本地session中的AccessToken信息
     */
    public static final String SESSION_ACCESS_TOKEN = "_sessionAccessToken";

    /**
     * 服务端登录地址
     */
    public static final String LOGIN_URL = "/login";

    /**
     * 服务端登出地址
     */
    public static final String LOGOUT_URL = "/logout";

    /**
     * 模糊匹配后缀
     */
    public static final String URL_FUZZY_MATCH = "/*";
}
