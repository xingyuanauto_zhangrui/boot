package pers.vic.sso.common.model;

import java.io.Serializable;

/**
 * 描述:
 *      AccessToken
 * @author Vic.xu
 * @date 2021-11-02 11:01
 */
public class AccessTokenContent implements Serializable {

    private static final long serialVersionUID = 5024129530174250745L;

    private AuthorizationCode authorizationCode;
    private SsoUser user;
    private String appId;

    public AccessTokenContent() {
    }

    public AccessTokenContent(AuthorizationCode authorizationCode, SsoUser user, String appId) {
        this.authorizationCode = authorizationCode;
        this.user = user;
        this.appId = appId;
    }

    public AuthorizationCode getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(AuthorizationCode authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public SsoUser getUser() {
        return user;
    }

    public void setUser(SsoUser user) {
        this.user = user;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
