package pers.vic.sso.common.constant;

/**
 * 描述:
 *      Oauth2相关的常量
 * @author Vic.xu
 * @date 2021-10-29 17:23
 */
public class Oauth2Constant {
    /**
     * 应用唯一标识 (参数name)
     */
    public static final String APP_ID = "appId";

    /**
     * 刷新token
     */
    public static final String REFRESH_TOKEN = "refreshToken";

    /**
     * 授权码（授权码模式）
     */
    public static final String AUTH_CODE = "code";

    /**
     * 授权方式
     */
    public static final String GRANT_TYPE = "grantType";

    /**
     * 应用密钥
     */
    public static final String APP_SECRET = "appSecret";

    /**
     * 用户名（密码模式）
     */
    public static final String USERNAME = "username";

    /**
     * 密码（密码模式）
     */
    public static final String PASSWORD = "password";

    /**
     * 获取accessToken地址
     */
    public static final String ACCESS_TOKEN_URL = "/oauth2/access_token";

    /**
     * 刷新accessToken地址
     */
    public static final String REFRESH_TOKEN_URL = "/oauth2/refresh_token";

}
