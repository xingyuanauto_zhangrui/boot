package pers.vic.boot.rpc.test.model;


import pers.vic.boot.rpc.base.BaseRpcModel;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月10日 上午11:14:26
 */
public class RpcModel1 extends BaseRpcModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String name;

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
