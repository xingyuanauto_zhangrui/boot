package pers.vic.boot.rpc.test.service;

import pers.vic.boot.rpc.test.model.RpcModel1;

/** 
 * @description: 测试rpc的第一个接口
 * @author: Vic.xu
 * @date: 2019年12月10日 上午11:17:55
 */
public interface FirstRpcService {

	public RpcModel1 getModel(Integer id);
	
	public void deleteModel(Integer id);
}
