package pers.vic.boot.rpc.base;

import java.io.Serializable;

/** 
 * @description: RPC模块的model的基类
 * @author: Vic.xu
 * @date: 2019年12月10日 上午11:15:09
 */
public class BaseRpcModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
