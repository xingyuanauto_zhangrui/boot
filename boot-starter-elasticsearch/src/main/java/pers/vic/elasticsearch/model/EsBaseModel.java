/**
 * 
 */
package pers.vic.elasticsearch.model;

import java.io.Serializable;

/**
 *  @description: 保存到ES中的实体的基类，id 
 *  @author Vic.xu
 *  @date: 2020年9月16日上午10:35:09
 */
public class EsBaseModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

}
