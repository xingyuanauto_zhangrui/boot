/**
 *
 */
package pers.vic.elasticsearch.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 *  @description: ElasticSearch 配置类
 *  @author Vic.xu
 *  @date: 2020年9月10日下午2:48:28
 */
@ConfigurationProperties(prefix = EsProperties.ES_PREFIX)
public class EsProperties {

    /**配置前缀*/
    public static final String ES_PREFIX = "es";

    /*默认超时时间 设为5分钟*/
    public static final int DEFAULT_TIMEOUT = 5 * 60 * 1000;

    /*默认的协议*/
    public static final String DEFAULT_SCHEME = "http";

    /*默认id*/
    public static final String DEFAULT_IP = "127.0.0.1";

    /*默认端口*/
    public static final int DEFAULT_PORT = 9200;

    /*默认的地址*/
    public AddressConfig[] defaultAddress = {new AddressConfig(DEFAULT_IP, DEFAULT_PORT)};

    /**超时时间*/
    private int timeout = DEFAULT_TIMEOUT;

    /**协议*/
    private String scheme = DEFAULT_SCHEME;

    /**IP：端口列表*/
    public AddressConfig[] address = defaultAddress;

    /**index列表*/
    public IndexConfig[] indexes;


    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }


    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public AddressConfig[] getAddress() {
        return address;
    }

    public void setAddress(AddressConfig[] address) {
        this.address = address;
    }

    public IndexConfig[] getIndexes() {
        return indexes;
    }

    public void setIndexes(IndexConfig[] indexes) {
        this.indexes = indexes;
    }


}
