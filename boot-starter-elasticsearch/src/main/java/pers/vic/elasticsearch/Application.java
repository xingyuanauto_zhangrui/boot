package pers.vic.elasticsearch;

import java.time.LocalDateTime;

import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 *  @description: 启动类,用于测试
 *  @author Vic.xu
 *  @date 2020年9月10日 14:46:46
 */
//@SpringBootApplication
//@RestController
public class Application {

	 public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
	}
	
	 
	 @GetMapping("/")
	 public String home() throws JsonProcessingException {
		 return LocalDateTime.now() +"";
	 }
	 
	 
	 
}
