/**
 * 
 */
package pers.vic.elasticsearch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.util.JsonUtil;
import pers.vic.elasticsearch.helper.EsSearchHelper;
import pers.vic.elasticsearch.model.EsLookup;

/**
 *  @description: 测试ES  starter
 *  @author Vic.xu
 *  @date: 2020年9月11日下午2:13:47
 */
public class EsTest extends BaseTest{
	
	String index = "blog_index";
	
	@Autowired
	private EsSearchHelper esSearchHelper;
	
	@Test
	public void test() throws IOException {
		Assert.notNull(esSearchHelper,"esSearchHelper is null");
		String keyword = "java";
		EsLookup<BlogArticle> lookup = new EsLookup<BlogArticle>();
		lookup.setClazz(BlogArticle.class);
		lookup.setIndexName(index);
		lookup.setKeyword(keyword);
		lookup.setSize(5);
		Map<String, BiConsumer<BlogArticle, String>> highlightFieldAndSetFunctionMap = new HashMap<String, BiConsumer<BlogArticle,String>>();
		highlightFieldAndSetFunctionMap.put("title", (article, text)->{
			article.setTitle(text);
		});
		highlightFieldAndSetFunctionMap.put("content", (article, text)->{
			article.setContent(text);
		});
		highlightFieldAndSetFunctionMap.put("summary", (article, text)->{
			article.setSummary(text);
		});
		lookup.setHighlightFieldAndSetFunctionMap(highlightFieldAndSetFunctionMap);
		PageInfo<BlogArticle> search = esSearchHelper.search(lookup);
		System.out.println(search.getTotal());
		System.out.println(search.getSize());
		search.getDatas().forEach(c->{
			JsonUtil.printJson(c.getSummary());
			JsonUtil.printJson(c.getTitle());
			JsonUtil.printJson(c.getContent());
		});
	}
	
	

}
