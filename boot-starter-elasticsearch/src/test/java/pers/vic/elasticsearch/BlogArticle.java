package pers.vic.elasticsearch;


import pers.vic.boot.base.model.BaseEntity;


/**
 * 博客表 实体类
 *
 * @author Vic.xu
 */
public class BlogArticle extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String summary;

    /**
     * 内容
     */
    private String content;

    /**
     * 所属分类id
     */
    private Integer categoryId;

    /**
     * 作者
     */
    private String author;

    /**
     * 是否原创
     */
    private Integer originated;

    /**
     * 来源
     */
    private String source;

    /**
     * 创建人id
     */
    private Integer createId;

    /**
     * 阅读次数
     */
    private Integer readNum;


    /**
     * 标签id,逗号分隔
     */
    private String tagIds;

    /**
     * 标签名称 逗号分隔
     */
    private String tagNames;

    /**
     * 专题id
     */
    private Integer topicId;

    /**
     * 权限 0-公开 1-好友 2-自己
     */
    private Integer authority;

    /**
     * 图片
     */
    private Integer icon;

    /* *********************************************/

    /**
     * 专题名
     */
    private String topicName;

    /**
     * 分类名
     */
    private String categoryName;


    /* ************** set|get  start ************************************* */


    /**
     * set：标题
     */
    public BlogArticle setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * get：标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * set：简介
     */
    public BlogArticle setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    /**
     * get：简介
     */
    public String getSummary() {
        return summary;
    }

    /**
     * set：内容
     */
    public BlogArticle setContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * get：内容
     */
    public String getContent() {
        return content;
    }

    /**
     * set：所属分类id
     */
    public BlogArticle setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    /**
     * get：所属分类id
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * set：作者
     */
    public BlogArticle setAuthor(String author) {
        this.author = author;
        return this;
    }

    /**
     * get：作者
     */
    public String getAuthor() {
        return author;
    }

    /**
     * set：是否原创
     */
    public BlogArticle setOriginated(Integer originated) {
        this.originated = originated;
        return this;
    }

    /**
     * get：是否原创
     */
    public Integer getOriginated() {
        return originated;
    }

    /**
     * set：来源
     */
    public BlogArticle setSource(String source) {
        this.source = source;
        return this;
    }

    /**
     * get：来源
     */
    public String getSource() {
        return source;
    }

    /**
     * set：创建人id
     */
    public BlogArticle setCreateId(Integer createId) {
        this.createId = createId;
        return this;
    }

    /**
     * get：创建人id
     */
    public Integer getCreateId() {
        return createId;
    }

    /**
     * set：阅读次数
     */
    public BlogArticle setReadNum(Integer readNum) {
        this.readNum = readNum;
        return this;
    }

    /**
     * get：阅读次数
     */
    public Integer getReadNum() {
        return readNum;
    }


    /**
     * set：标签id,逗号分隔
     */
    public BlogArticle setTagIds(String tagIds) {
        this.tagIds = tagIds;
        return this;
    }

    /**
     * get：标签id,逗号分隔
     */
    public String getTagIds() {
        return tagIds;
    }

    /**
     * set：标签名称 逗号分隔
     */
    public BlogArticle setTagNames(String tagNames) {
        this.tagNames = tagNames;
        return this;
    }

    /**
     * get：标签名称 逗号分隔
     */
    public String getTagNames() {
        return tagNames;
    }

    /**
     * set：专题id
     */
    public BlogArticle setTopicId(Integer topicId) {
        this.topicId = topicId;
        return this;
    }

    /**
     * get：专题id
     */
    public Integer getTopicId() {
        return topicId;
    }

    /**
     * set：权限 0-公开 1-好友 2-自己
     */
    public BlogArticle setAuthority(Integer authority) {
        this.authority = authority;
        return this;
    }

    /**
     * get：权限 0-公开 1-好友 2-自己
     */
    public Integer getAuthority() {
        return authority;
    }


    /**
     * set：图片
     */
    public BlogArticle setIcon(Integer icon) {
        this.icon = icon;
        return this;
    }

    /**
     * get：图片
     */
    public Integer getIcon() {
        return icon;
    }

    /***************** set|get  end **************************************/

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
