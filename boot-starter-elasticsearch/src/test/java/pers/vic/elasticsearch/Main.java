/**
 * 
 */
package pers.vic.elasticsearch;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.validation.ValidationProviderResolver;
import javax.validation.spi.BootstrapState;

import org.hibernate.validator.internal.engine.ConfigurationImpl;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年9月11日下午4:03:31
 */
public class Main {
	public static void main(String[] args) {
		
		ConfigurationImpl i = new ConfigurationImpl(new BootstrapState() {
			
			@Override
			public ValidationProviderResolver getValidationProviderResolver() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ValidationProviderResolver getDefaultValidationProviderResolver() {
				// TODO Auto-generated method stub
				return null;
			}
		});
		i.getDefaultParameterNameProvider();
		
		Foo<String> foo = new Foo<String>() {
		};
		// 在类的外部这样获取
		Type type = ((ParameterizedType) foo.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		System.out.println(type);
		// 在类的内部这样获取
		System.out.println(foo.getTClass());
	}
}

abstract class Foo<T> {
	public Class<T> getTClass() {
		@SuppressWarnings("unchecked")
		Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		return tClass;
	}
}