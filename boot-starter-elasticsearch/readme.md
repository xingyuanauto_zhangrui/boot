[toc]

## 006-把elasticsearch(7.7.1)修改为starter方式

### 一初衷：为什么想把es写成一个starter

>  博客项目集成了ES7.7.1；但是本地启动项目的时候，每次还需要启动es，觉着有点麻烦，故希望可以改成可配置是否启动等；

1. 方便插拔，在不需要时，可通过配置移除
2. 方便版本控制
3. 方便统一扩展，和复用

### 二  大概要实现的功能点

> 把一些常用的功能集成起来，既方便使用，也方便后续统一扩展

1. 统一配置
   1. addresses：ip:port
   2. scheme
   3. timeout等
2. RestHighLevelClient配置和创建
   1. 启动的时候判断index是否创建，没有则创建
   2. 关闭的销毁RestHighLevelClient
3. 提供一些基础的api操作
   1. 操作index
   2. es文档的基本增删改查
   3. 高亮查询

### 三 按照大概要实现的功能点开始编码

#### 3.1 依赖

在parent项目中已经管理es的版本,在当前项目只需要引入即可

> <dependency>
> 	<groupId>org.elasticsearch.client</groupId>
> 	<artifactId>elasticsearch-rest-high-level-client</artifactId>
> </dependency>

[parent.xml配置如下](https://gitee.com/lcxm/vic-environment/blob/master/parent/pom.xml)

```
<elasticsearch.version>7.7.1</elasticsearch.version>
<!-- 重新定义es的版本依赖 start -->
<dependency>
	<groupId>org.elasticsearch.client</groupId>
	<artifactId>elasticsearch-rest-high-level-client</artifactId>
	<version>${elasticsearch.version}</version>
</dependency>
<dependency>
	<groupId>org.elasticsearch</groupId>
	<artifactId>elasticsearch</artifactId>
	<version>${elasticsearch.version}</version>
</dependency>
<dependency>
	<groupId>org.elasticsearch.client</groupId>
	<artifactId>elasticsearch-rest-client</artifactId>
	<version>${elasticsearch.version}</version>
</dependency>
<!-- 重新定义es的版本依赖 start -->		
```

#### 3.2 一些通用配置，参见配置类[EsProperties](https://gitee.com/lcxm/boot/blob/master/boot-starter-elasticsearch/src/main/java/pers/vic/elasticsearch/autoconfigure/EsProperties.java)

```properties
# 是否开启es starter   默认true
es.enabled=true
# 超时时间 默认5分钟5 * 60 * 1000
es.timeout=3000000
#scheme 默认http
es.scheme=http
## es的地址列表，默认会配置一个127.0.0.1：9200
es.address[0].ip=127.0.0.1
es.address[0].port=9200
## index列表， 会在启动项目的时候检测是否存在， 不存在则创建
#index name
es.indexes[0].name=indexName
#别名 不配置 则根据名称+_alias
es.indexes[0].alias=indexNamealias
# 分片数 默认3
es.indexes[0].shards=3
#备份数 默认0
es.indexes[0].replicas=0
# 分词器 默认 ik_max_word
es.indexes[0].analyzer=ik_max_word
### index的mapping配置，field：字段； type:类型    ;默认会一个id= keyword
es.indexes[0].properties[0].field=title
es.indexes[0].properties[0].type=text
es.indexes[0].properties[1].field=id
es.indexes[0].properties[1].type=keyword

```

##### 换成yaml格式如下

```yaml
es:
  address:
  - ip: 127.0.0.1
    port: 9200
  enabled: true
  indexes:
  - alias: indexNamealias
    analyzer: ik_max_word
    name: indexName
    properties:
    - field: title
      type: text
    - field: id
      type: keyword
    replicas: 0
    shards: 3
  scheme: http
  timeout: 3000000
```

#### 3.3 [自动配置入口类EsAutoconfigure](https://gitee.com/lcxm/boot/blob/master/boot-starter-elasticsearch/src/main/java/pers/vic/elasticsearch/autoconfigure/EsAutoconfigure.java)

##### 3.3.1 配置RestHighLevelClient  

##### 3.3.2 创建index操作工具类IndexHelper

- 删除索引
- 创建索引
- 判断索引是否存在
- 构建索引的配置属性

##### 3.3.3 创建EsDocumentHelper 方便对文档的一些 操作

- 单个文档 的增删改查
- 文档批量增删改查

##### 3.3.4 创建EsSearchHelper 用于高亮查询

> 详情参见代码

- 分页查询, 高亮显示内容, 最多只能查询10000条
- [注意ESLookup构建](https://gitee.com/lcxm/boot/blob/master/boot-starter-elasticsearch/src/main/java/pers/vic/elasticsearch/model/EsLookup.java)
- 可以用如下方式构建：

```java
EsLookup<EsTesting> lookup = EsLookup.build("indexName", EsTesting.class).keyword("keyword").page(1, 2)
				.addHighlightFieldAndSetFunctionString("name", (model, name) -> {
					model.setName(name);
				}).addHighlightFieldAndSetFunctionString("remark", (model, name) -> {
					model.setName(name);
				}).end();
```

##### 3.3.5  项目启动后创建索引 #initIndex

* 不存在的则创建

##### 3.3.6 项目关闭的时候关闭客户端#closeClient

### 四 META-INF\spring.factories（略）

> 本文非介绍如何构建springboot-starter
>
> 只大体记录了当前实现的部分功能，如果感兴趣可以查看源码，相互交流。