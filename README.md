# boot

### 介绍

这是一个springboot项目, 它集成了一些常见的功能, 常用的模块, 或者一些demo.

### 后端相关目录文档

[003-编码代码生成starter](/docs/02-后端/003-编码代码生成starter.md)

[007-02-shiro+jwt改为 security-starter集成shiro,jwt,mybatis数据拦截器](/boot-starter-security)

[009-权限(RBAC)代码简要设计](/docs/02-后端/009-权限部分代码设计.md)

[010.基于mybatis拦截器的数据权限处理](/docs/02-后端/010.基于mybatis拦截器的数据权限处理.md)

[011.基于cas协议的sso单点登录](http://xuqiudong.gitee.io/technology/#/solution/cas-sso/)

