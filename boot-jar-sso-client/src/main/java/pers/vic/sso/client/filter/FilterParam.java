package pers.vic.sso.client.filter;

import pers.vic.sso.common.constant.SsoConstant;

/**
 * 描述:
 *    存放Filter和SSO SERVER交互的参数
 * @author Vic.xu
 * @date 2021-11-02 9:36
 */
public class FilterParam {

    /**
     * 客户端标识
     */
    protected String appId;
    /**
     * 密码
     */
    protected String appSecret;
    /**
     * sso server 地址
     */
    protected String serverUrl;

    /**
     * 客户端退出登录地址
     */
    protected String clientLogoutUrl = SsoConstant.LOGOUT_URL;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getClientLogoutUrl() {
        return clientLogoutUrl;
    }

    public void setClientLogoutUrl(String clientLogoutUrl) {
        this.clientLogoutUrl = clientLogoutUrl;
    }
}
