package pers.vic.sso.client.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 描述:
 * 前后端分离的login过滤器,
 * 前端在判断没有登录的情况下，直接在浏览器访问后端的某个url，然后后端进行相关重定向 <br />
 *      怎么判断： 比如返回状态码为302  则操作
 *
 *      <p>
 *          1. 前后端应该在一个域内，保证session一致
 *          校验流程：<br />
 *          1. 访问后端某个接口， 判断没有登录<br />
 *          2. 跳转到sso， 但是携带的redirectUrl地址应是当前后端的特点地址<br />
 *          3. 进行一系列校验工作后，登录成功，判断当前访问的是这个特定地址，则重定向到配置的前端地址<br />
 *          区别：<br />
 *          1. 需要配置htmlUrl：即检验成功后 重定向的前端地址<br />
 *          2. 需要配置clientHost：当前后端的根地址，用户sso Server重定向回来<br />
 *
 * </p>
 *
 * @author Vic.xu
 * @date 2021-11-12 11:13
 */
public class SeparationLoginFilter extends LoginFilter {

    /**
     * 当前项目的根地址
     */
    private String clientHost;

    /**
     * 检验成功后 重定向的前端地址
     */
    protected String htmlUrl;

    /**
     * 从sso返回接口客户端的特定地址
     */
    private String redirectFlagPath = "_redirectFlagPath";


    public SeparationLoginFilter(String clientHost, String htmlUrl) {
        this.clientHost = clientHost;
        this.htmlUrl = htmlUrl;
    }


    /**
     * 重定向的是当前后台的特定地址
     * @param request
     * @return
     */
    @Override
    protected String getRedirectUrl(HttpServletRequest request) {
        logger.info("getRedirectUrl : {}", clientHost);
        return clientHost;
    }

    /**
     * 校验成功后， 重定向后前端页面
     * @param request
     * @param response
     * @throws IOException
     */
    @Override
    protected void redirectLocalRemoveCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(htmlUrl);
    }

}
