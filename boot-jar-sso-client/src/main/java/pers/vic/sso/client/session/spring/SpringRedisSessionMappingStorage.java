package pers.vic.sso.client.session.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.session.SessionRepository;
import pers.vic.sso.client.session.SessionMappingStorage;

import javax.servlet.http.HttpSession;

/**
 *  如果客户端使用spring-session-data-redis 管理session的话  （@EnableRedisHttpSession）
 *
 *
 */
public final class SpringRedisSessionMappingStorage implements SessionMappingStorage {
	
	private static final String SESSION_TOKEN_KEY = "session_token_key_";
	private static final String TOKEN_SESSION_KEY = "token_session_key_";

	@Autowired(required = false)
	private SessionRepository<?> sessionRepository;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

    @Override
    public synchronized void addSessionById(final String accessToken, final HttpSession session) {
		stringRedisTemplate.opsForValue().set(SESSION_TOKEN_KEY + session.getId(), accessToken);
		
		stringRedisTemplate.opsForValue().set(TOKEN_SESSION_KEY + accessToken, session.getId());
    }

    @Override
    public synchronized void removeBySessionById(final String sessionId) {
		final String accessToken = stringRedisTemplate.opsForValue().get(SESSION_TOKEN_KEY + sessionId);
		if (accessToken != null) {
			stringRedisTemplate.delete(TOKEN_SESSION_KEY + accessToken);
			stringRedisTemplate.delete(SESSION_TOKEN_KEY + sessionId);
			
			sessionRepository.deleteById(sessionId);
		}
    }

    @Override
    public synchronized HttpSession removeSessionByMappingId(final String accessToken) {
        final String sessionId = stringRedisTemplate.opsForValue().get(TOKEN_SESSION_KEY + accessToken);
        if (sessionId != null) {
            removeBySessionById(sessionId);
        }
        return null;
    }
}
