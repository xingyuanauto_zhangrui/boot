package pers.vic.sso.client.session;

import javax.servlet.http.HttpSession;

/**
 * 描述:
 *   accessToken和session的映射关系
 * @author Vic.xu
 * @date 2021-11-03 14:16
 */
public interface SessionMappingStorage {
    HttpSession removeSessionByMappingId(String accessToken);

    void removeBySessionById(String sessionId);

    void addSessionById(String accessToken, HttpSession session);
}
