/**
 * 
 */
package pers.vic.boot.util.json.fastjosn.test.model;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月13日上午9:19:51
 */
public class ModelOne {
	
	private Integer id;
	
	private String name;
	
	private String remark;
	
	

	/**
	 * @param id
	 * @param name
	 * @param remark
	 */
	public ModelOne(Integer id, String name, String remark) {
		super();
		this.id = id;
		this.name = name;
		this.remark = remark;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
