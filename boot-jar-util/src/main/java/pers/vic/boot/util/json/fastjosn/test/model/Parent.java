/**
 * 
 */
package pers.vic.boot.util.json.fastjosn.test.model;

import java.util.List;

import pers.vic.boot.util.json.fastjosn.annotation.CustomerJsonField;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月13日上午9:19:15
 */
public class Parent {
	
	private Integer id;
	
	private String other ;
	
	private String name;
	
//	@JSONField(serializeUsing = IdOnlySerializer.class, deserializeUsing = IdOnlyDeserializer.class)
	@CustomerJsonField("id")
	private ModelOne modelOne;
	
	@CustomerJsonField({"id", "list"})
	private List<ModelTwo> list;
	
	

	/**
	 * @param id
	 * @param name
	 * @param modelOne
	 * @param list
	 */
	public Parent(Integer id, String name, ModelOne modelOne, List<ModelTwo> list) {
		super();
		this.id = id;
		this.name = name;
		this.modelOne = modelOne;
		this.list = list;
		this.other = "other1111";
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the modelOne
	 */
	public ModelOne getModelOne() {
		return modelOne;
	}

	/**
	 * @param modelOne the modelOne to set
	 */
	public void setModelOne(ModelOne modelOne) {
		this.modelOne = modelOne;
	}

	/**
	 * @return the list
	 */
	public List<ModelTwo> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<ModelTwo> list) {
		this.list = list;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}
	
	
	

}
