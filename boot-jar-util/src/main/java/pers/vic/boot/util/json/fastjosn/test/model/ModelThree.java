/**
 * 
 */
package pers.vic.boot.util.json.fastjosn.test.model;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年5月13日上午9:20:40
 */
public class ModelThree {

	private Integer id;

	private String name;

	private String other;
	
	

	/**
	 * @param id
	 * @param name
	 * @param other
	 */
	public ModelThree(Integer id, String name, String other) {
		super();
		this.id = id;
		this.name = name;
		this.other = other;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}
	
	
}
