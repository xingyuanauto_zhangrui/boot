/**
 * 
 */
package pers.vic.boot.util.json.fastjosn;

import java.lang.reflect.Type;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;

/**
 *  @description: 只反序列化id, 必须和 IdOnlySerializer成对出现
 *  使用方法,方在对象属性上,对象必须包含id属性: 
 *    @JSONField(serializeUsing = IdOnlySerializer.class, deserializeUsing = IdOnlyDeserializer.class)
 *  @author Vic.xu
 *  @date: 2020年5月12日下午2:49:55
 */
public class IdOnlyDeserializer implements ObjectDeserializer  {

	@Override
	public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
		String json = parser.getLexer().stringVal();
		return JSONObject.parseObject(json, type);
	}

	@Override
	public int getFastMatchToken() {
		return 0;
	}


}
