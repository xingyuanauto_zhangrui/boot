/**
 * 
 */
package pers.vic.boot.util.json.fastjosn.test;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;

import pers.vic.boot.util.JsonUtil;
import pers.vic.boot.util.json.fastjosn.CustomerPropertyFilter;
import pers.vic.boot.util.json.fastjosn.test.model.ModelOne;
import pers.vic.boot.util.json.fastjosn.test.model.ModelThree;
import pers.vic.boot.util.json.fastjosn.test.model.ModelTwo;
import pers.vic.boot.util.json.fastjosn.test.model.Parent;

/**
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月13日上午9:22:38
 */
public class TestFastjosn {
	
	public static void main(String[] args) {
		testCustomerPropertyFilter();
//		testIdOnly();
	}
	/**
	 * 测试 IdOnlyDeserializer 和 IdOnlySerializer
	 */
	public static void testIdOnly() {
		Parent p = build();
		System.out.print("原对象:");
		JsonUtil.printJson(p);
		String json1 = JSONObject.toJSONString(p);
		System.out.println("IdOnly-->\t" + json1);
		Parent p2 = JSONObject.parseObject(json1, Parent.class);
		System.out.print("转回的对象:");
		JsonUtil.printJson(p2);
	}
	
	/**
	 * 测试CustomerPropertyFilter 只序列化指定的字段, 配合CustomerJsonField注解使用
	 */
	public static void testCustomerPropertyFilter() {
		Parent p = build();
		System.out.print("原对象:");
		JsonUtil.printJson(p);
		String json = JSONObject.toJSONString(p, new CustomerPropertyFilter());
		System.out.println("json字符串: " + json);
		Parent p2 = JSONObject.parseObject(json, Parent.class);
		System.out.print("转回的对象:");
		JsonUtil.printJson(p2);
	}
	
	public static Parent build() {
		ModelOne one = new ModelOne(2, "one", "我是one");
		List<ModelThree> listThree = new ArrayList<ModelThree>();
		listThree.add(new ModelThree(55, "name55", "other55"));
		listThree.add(new ModelThree(66, "name66", "other66"));
		List<ModelTwo> list = new ArrayList<ModelTwo>();
		list.add(new ModelTwo(3, "李四", "我是谁", listThree));
		list.add(new ModelTwo(4, "王五", "备注1"));
		Parent p = new Parent(1, "张三", one, list);
		return p;
	}
}
