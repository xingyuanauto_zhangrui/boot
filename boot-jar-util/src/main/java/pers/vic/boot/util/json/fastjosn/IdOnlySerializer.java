/**
 * 
 */
package pers.vic.boot.util.json.fastjosn;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;

import pers.vic.boot.util.reflect.ReflectionUtils;

/**
 *  @description: 只序列化对象的id,必须后IdOnlyDeserializer成对出现
 *  使用方法,方在对象属性上,对象必须包含id属性: 
 *    @JSONField(serializeUsing = IdOnlySerializer.class, deserializeUsing = IdOnlyDeserializer.class)
 *  @author Vic.xu
 *  @date: 2020年5月12日下午2:49:55
 */
public class IdOnlySerializer implements ObjectSerializer {
	
	@Override
	public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features)
			throws IOException {
		Object id = ReflectionUtils.getFieldValue(object, "id");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
        String text = JSONObject.toJSONString(map);
        serializer.write(text);
	}
	

}
