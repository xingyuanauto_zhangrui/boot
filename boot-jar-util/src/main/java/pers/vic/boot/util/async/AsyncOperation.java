/**
 *
 */
package pers.vic.boot.util.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 异步操作, 应该注入到spring, 利用spring的单例bean特性进行一步操作
 * @author Vic.xu
 *
 */
//@Service
public class AsyncOperation extends Thread {

    /**
     * 给BlockingQueue一个大小值, 防止线程堆积
     */
    private static final Integer CAPACITY = 200;

    private final Logger logger = LoggerFactory.getLogger(AsyncOperation.class);
    private final BlockingQueue<Runnable> sharedQueue = new LinkedBlockingQueue<>(CAPACITY);

    @Override
    public void run() {
        while (true) {
            try {
                Runnable runnable = sharedQueue.take();
                runnable.run();
            } catch (Exception ex) {
                logger.error("忽略异步操作错误:" + ex.getMessage(), ex);
            }
        }
    }

    public void put(Runnable runnable) {
        try {
            sharedQueue.put(runnable);
            notifyNewPut();
        } catch (InterruptedException ex) {
            throw new RuntimeException("加入异步操作出错", ex);
        }
    }

    private void notifyNewPut() {
        if (!isAlive()) {
            setDaemon(true);
            start();
        }
    }

    public static void main(String[] args) {
        AsyncOperation asyncOperation = new AsyncOperation();
        for (int i = 0; i < CAPACITY + 10; i++) {
            Runnable r = () -> {
                try {
                    System.out.println(LocalTime.now());
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            asyncOperation.put(r);
            System.out.println("put " + i + "   " + LocalTime.now());
        }
    }


}