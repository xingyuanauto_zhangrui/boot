package pers.vic.boot.util.sql;

/**
 * 关于SQL的一些工具类
 * 
 * @author Vic.xu
 * @date 2021/10/12
 */
public class SqlUtil {

    private SqlUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 把oracle对应的SQL语句包装为分页语句
     * 
     * @param prefixSql
     *            原来的完整的sql
     * @param page
     *            页码
     * @param size
     *            每页数据量
     */
    public static String oracleLimit(String prefixSql, int page, int size) {
        int start = (page - 1) * size;
        int end = page * size;
        String pattern =
            "select * from( select rownum as rowno, t.*  from ( %s ) t where rownum < %d) tt where tt.rowno > %d";

        String sql = String.format(pattern, prefixSql, end, start);
        return sql;
    }

    public static void main(String[] args) {
        String sql = "select * from user_tab_columns a where 1=1 order by a.CHAR_LENGTH desc";
        System.out.println(oracleLimit(sql, 2, 10));
    }

}
