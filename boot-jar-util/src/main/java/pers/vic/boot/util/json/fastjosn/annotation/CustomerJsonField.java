package pers.vic.boot.util.json.fastjosn.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import pers.vic.boot.util.json.fastjosn.CustomerPropertyFilter;

/**
 * 只序列化对象化属性的指定属性:
 *  @see CustomerPropertyFilter
 *  使用方法: 加在需要定制序列化的属性字段上
 *  @description: 
 *  @author Vic.xu
 *  @date: 2020年5月13日上午9:36:30
 */
@Retention (RetentionPolicy.RUNTIME)
@Target({  ElementType.FIELD })
public @interface CustomerJsonField {
	
	String[] value() default {};
	
 }
