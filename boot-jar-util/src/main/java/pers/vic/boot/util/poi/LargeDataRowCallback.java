package pers.vic.boot.util.poi;

import java.util.Map;

/** 
 * @description: 导入的数据的每一行的处理回调
 * @author: Vic.xu
 * @date: 2019年12月16日 下午3:31:40
 */
public interface LargeDataRowCallback {
	
	/**
	 * @param rowData 行数据: key为列头名称   value为当前行对应列头的单元格值;
	 * 		当cell值不存在的时候,则不存在key,注意判断
	 */
	void callback(Map<String, String> rowData);
	
}
