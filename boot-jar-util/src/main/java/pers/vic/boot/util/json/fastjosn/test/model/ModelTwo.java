/**
 * 
 */
package pers.vic.boot.util.json.fastjosn.test.model;

import java.util.List;

import pers.vic.boot.util.json.fastjosn.annotation.CustomerJsonField;

/**
 * @description:
 * @author Vic.xu
 * @date: 2020年5月13日上午9:20:40
 */
public class ModelTwo {

	private Integer id;

	private String name;

	private String other;
	
	@CustomerJsonField({"id"})
	private List<ModelThree> list;
	
	

	/**
	 * @param id
	 * @param name
	 * @param other
	 */
	public ModelTwo(Integer id, String name, String other) {
		super();
		this.id = id;
		this.name = name;
		this.other = other;
	}
	public ModelTwo(Integer id, String name, String other, List<ModelThree> list) {
		super();
		this.id = id;
		this.name = name;
		this.other = other;
		this.list = list;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}
	/**
	 * @return the list
	 */
	public List<ModelThree> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(List<ModelThree> list) {
		this.list = list;
	}
	
	
	
	
}
