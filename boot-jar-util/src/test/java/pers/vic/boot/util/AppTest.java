package pers.vic.boot.util;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.text.StringEscapeUtils;

/**
 * Unit test for simple App.
 */
public class AppTest {

    public static void main(String[] args) {

        String str = StringEscapeUtils.unescapeHtml4("&lt;p&gt;【产品名称】艾酷维多种维生素锌软糖&lt;/p&gt;");
        System.out.println(str);

        System.out.println(StringEscapeUtils.unescapeEcmaScript("<script>alert(\'123\')<script>"));

        long l = Stream.iterate(1L, i -> i + 1).limit(4).reduce(Long::sum).get();
        System.out.println(l);
        List<Integer> list = Stream.iterate(1, n -> n + 1).limit(10).collect(Collectors.toList());
        System.out.println(list);
        list.remove(list.size() - 1);
        System.out.println(list);
    }

    public static void json() {
        Model m = new Model(1, new Date());
        JsonUtil.printJson(m);

        String s = "{\"id\":1}";
        Model m2 = JsonUtil.jsonToObject(s, Model.class);
        JsonUtil.printJson(m2);
    }

}

class Model {

    int id;

    Date date;

    /**
     * @param id
     * @param date
     */
    public Model(int id, Date date) {
        super();
        this.id = id;
        this.date = date;
    }

    /**
     * 
     */
    public Model() {
        super();
        this.date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
