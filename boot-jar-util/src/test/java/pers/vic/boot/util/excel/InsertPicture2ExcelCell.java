/**
 * 
 */
package pers.vic.boot.util.excel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @description: insert2() is my answer : insert a picture to a cell
 * @author Vic.xu
 * @date: 2021年3月2日上午11:53:50
 */
public class InsertPicture2ExcelCell {
	
	private static String png = "D:desk/test.png";
	
	private static String getExcel() {
		return "d:/desk/output-"+DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss")+".xlsx";
	}

	/**
	 * Insert image in column to excel using Apache POI
	 * {@literal https://stackoverflow.com/questions/28238078/insert-image-in-column-to-excel-using-apache-poi/28238262}
	 * @throws Exception
	 */
	public static void insert() throws Exception {
		
		try (Workbook workbook = new XSSFWorkbook()) {
			Sheet sheet = workbook.createSheet("MYSheet");
			InputStream inputStream = new FileInputStream(png);

			   byte[] imageBytes = IOUtils.toByteArray(inputStream);

			   int pictureureIdx = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);

			   inputStream.close();

			   CreationHelper helper = workbook.getCreationHelper();

			   Drawing<?> drawing = sheet.createDrawingPatriarch();

			   ClientAnchor anchor = helper.createClientAnchor();

			   anchor.setCol1(10);
			   anchor.setRow1(10);

			   drawing.createPicture(anchor, pictureureIdx);


			   FileOutputStream fileOut = null;
			   fileOut = new FileOutputStream(getExcel());
			   workbook.write(fileOut);
			   fileOut.close();
		}
		
	}
	/**
	 * https://stackoverflow.com/questions/33712621/how-put-a-image-in-a-cell-of-excel-java/33721090
	 */
	public static void insert2() {
		try {

			   Workbook wb = new XSSFWorkbook();
			   Sheet sheet = wb.createSheet("My Sample Excel");
			   //FileInputStream obtains input bytes from the image file
			   InputStream inputStream = new FileInputStream(png);
			   //Get the contents of an InputStream as a byte[].
			   byte[] bytes = IOUtils.toByteArray(inputStream);
			   //Adds a picture to the workbook
			   int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
			   //close the input stream
			   inputStream.close();
			   //Returns an object that handles instantiating concrete classes
			   CreationHelper helper = wb.getCreationHelper();
			   //Creates the top-level drawing patriarch.
			   Drawing drawing = sheet.createDrawingPatriarch();

			   //Create an anchor that is attached to the worksheet
			   ClientAnchor anchor = helper.createClientAnchor();

			   //create an anchor with upper left cell _and_ bottom right cell   当前在第三行 第二列
			   anchor.setCol1(1); //Column B
			   anchor.setRow1(2); //Row 3
			   anchor.setCol2(2); //Column C
			   anchor.setRow2(3); //Row 4

			   //Creates a picture
			   Picture pict = drawing.createPicture(anchor, pictureIdx);

			   //Reset the image to the original size
			   //pict.resize(); //don't do that. Let the anchor resize the image!

			   //Create the Cell B3
			   Cell cell = sheet.createRow(2).createCell(1);

			   //set width to n character widths = count characters * 256
			   //int widthUnits = 20*256;
			   //sheet.setColumnWidth(1, widthUnits);

			   //set height to n points in twips = n * 20
			   //short heightUnits = 60*20;
			   //cell.getRow().setHeight(heightUnits);

			   //Write the Excel file
			   FileOutputStream fileOut = null;
			   fileOut = new FileOutputStream(getExcel());
			   wb.write(fileOut);
			   fileOut.close();

			  } catch (IOException ioex) {
			  }
	}
	
	public static void main(String[] args) throws Exception {
		insert2();
	}

}
