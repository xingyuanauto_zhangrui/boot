package pers.vic.readmd.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 附件starter 入口
 * @author: Vic.xu
 * @date: 2019年12月12日 上午9:05:28
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnExpression("${readmd.enabled:true}")
@EnableConfigurationProperties(ReadmdProperties.class)
public class ReadmdAutoconfigure  {
	
	ReadmdProperties readmdProperties;

	/**
	 * 构造器注入
	 */
	public ReadmdAutoconfigure(ReadmdProperties readmdProperties) {
		super();
		this.readmdProperties = readmdProperties;
	}
	
	


}
