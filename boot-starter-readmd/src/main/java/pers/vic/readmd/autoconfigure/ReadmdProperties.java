package pers.vic.readmd.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** 
 * @description: 读取md文件的配置文件
 * @author: Vic.xu
 * @date: 2020年5月15日 14:42:19
 */
@Configuration
@ConfigurationProperties(prefix = ReadmdProperties.PROPERTIES_PREFIX)
public class ReadmdProperties {
	
	public static final String PROPERTIES_PREFIX = "readmd";
	
	/**
	 * md文件的路径. 只会读这个目录下的后缀为.md的文件
	 */
	private String filePath;

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	};
	
	
	
	
}
