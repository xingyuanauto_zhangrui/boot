package pers.vic.boot.base.service;

import java.util.List;

/**
 * @description: 附件状态处理service
 * @author: Vic.xu
 * @date: 2019年12月11日 下午1:37:30
 */
public interface AttachmentStatusOperationServiceI {

	/** 新增对象中的全部附件 */
	public <T> boolean addAttachmengFromObj(T t) ;

	// 删除对象中的全部附件
	public <T> void deleteAttachmengFromObj(T t);
	
	// 批量删除
	public <T> void deleteAttachmengFromObj(List<T> ts);

	/**
	 * 分开对象中要删除和要新增的附件 需要 AttachmentFlag 注解
	 * @param old 原来对象
	 * @param now 新的对象
	 */
	public <T> void handleOldAndNowAttachment(T old, T now);
}
