/**
 * 
 */
package pers.vic.boot.base.web.intercept;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pers.vic.boot.base.tool.Tools;


/**
 * 保存最后一次get请求的拦截器
 * @author VIC
 *
 */
public class SaveRequestInterceptor extends HandlerInterceptorAdapter{
	
	public static String LAST_URL = "last_request_get_url";
	
	/**
	 * 放在请求后保存到session中
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if("GET".equalsIgnoreCase(request.getMethod())) {
			request.getSession().setAttribute(LAST_URL, Tools.getRequestUrl(request));
		}
		super.postHandle(request, response, handler, modelAndView);
	}


}