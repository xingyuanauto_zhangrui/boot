package pers.vic.boot.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.lookup.Lookup;

public interface BaseMapper<T>{

	/**查询列表*/
	List<T> list(Lookup lookup);
	
	/**根据主键id查询对象*/
	T findById(@Param("id") int id);
	
	/**插入对象*/
	int insert(T entity);
	
	/**更新数据*/
	int update(T entity);
	
	/**批量删除*/
	int  delete(@Param("ids")int[] ids);
	
	/**批量获取*/
	List<T> findByIds(@Param("ids")int[] ids);

	/**
	 * 查询列字段是否没有重复:
	 * @param id: 如果不传 则判断表里的全部项,如果传了id,则排除当前id所对应的列
	 * @param value 需要判断是否重复的列
	 * @param column 列名称
	 * @return
	 */
	boolean checkNotRepeat(@Param("id")Integer id, @Param("value")String value, @Param("column")String column);
	/*
	 <select id="checkNotRepeat" resultType="boolean">
        SELECT COUNT(1)=0 FROM tableName WHERE ${column} = #{value}
        <if test="id !=null">
            AND id!= #{id}
        </if>
    </select>
	 */
	
}