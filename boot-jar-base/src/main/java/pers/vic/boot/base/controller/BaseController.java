package pers.vic.boot.base.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pers.vic.boot.base.model.BaseEntity;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.base.service.BaseService;

/**
 * @description: controller 的基类
 * @author: Vic.xu
 * @date: 2019年11月13日 下午2:40:41
 */
public abstract class BaseController<S extends BaseService<?, T>, T extends BaseEntity> {

    @Autowired
    protected S service;

    @Resource
    protected HttpServletRequest request;

    /**
     * 列表
     * 
     * @return
     */
    @GetMapping(value = "list")
    public BaseResponse<?> list(T lookup) {
        PageInfo<T> list = service.page(lookup);
        return BaseResponse.success(list);
    }

    /**
     * 保存
     * 
     * @param entity
     * @return
     */
    @PostMapping(value = "/save")
    public BaseResponse<?> save(T entity) {
        service.save(entity);
        return BaseResponse.success(entity);
    }

    /**
     * 详情
     * 
     * @param id
     * @return
     */
    @GetMapping(value = "/detail")
    public BaseResponse<?> detail(Integer id) {
        T entity = service.findById(id);
        return BaseResponse.success(entity);
    }

    @PostMapping(value = "/delete")
    public BaseResponse<?> delete(int id) {
        service.delete(id);
        return BaseResponse.success();
    }

    @PostMapping(value = "/batchDelete")
    public BaseResponse<?> delete(@RequestParam("ids[]") int[] ids) {
        service.delete(ids);
        return BaseResponse.success();
    }

    /**
     * 检测"name"是否未重复
     * 
     * @param id
     * @param name
     * @return
     */
    @PostMapping(value = "/check")
    public BaseResponse<?> check(Integer id, String name) {
        boolean ok = checkNotRepeat(id, name, "name");
        return BaseResponse.success(ok);
    }

    /**
     * 查询列字段是否没有重复:
     * 
     * @param id:
     *            如果不传 则判断表里的全部项,如果传了id,则排除当前id所对应的列
     * @param value
     *            需要判断是否重复的列
     * @param column
     *            列名称
     * @return
     */
    public boolean checkNotRepeat(Integer id, String value, String column) {
        boolean repeat = service.checkNotRepeat(id, value, column);
        return repeat;
    }

}
