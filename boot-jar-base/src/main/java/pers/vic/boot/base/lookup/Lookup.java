package pers.vic.boot.base.lookup;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * 查询参数 默认带分页信息
 * @author VIC.xu
 *
 */
public class Lookup implements Serializable{
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	protected int page = 1;
	
	@JsonIgnore
	protected int size = 10;
	
	//排序字段
	@JsonIgnore
	protected String sortColumn;
	
	//排序方式 asc  desc
	@JsonIgnore
	protected String  sortOrder;
	
	
	public Lookup() {
		
	}
	
	public Lookup(int page, int size) {
		this.page = page;
		this.size = size;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

    /**
     * 当前数据索引.
     * @return 数据索引
     */
	@JsonIgnore
    public int getIndex() {
        return (getPage() - 1) * getSize();
    }


	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
    
    


	
	
}
