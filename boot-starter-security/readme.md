[toc]

### 007-02- `security-starter`:集成`shiro`,`jwt`,`mybatis`数据拦截器

#### 背景: 

##### 适用场景:

1. 前后端分离集成jwt和shiro
2. 使用mybatis拦截器对追加where条件对数据权限进行拦截

##### 大体流程:

1. 用户**登录**,构建用户基本信息对象AuthorityInfo:包含部门列表/岗位列表/用户id/用户名等
2. 通过对象AuthorityInfo,调用`JwtService`的buildTokenAndCache方法,构建出token,并缓存token和AuthorityInfo到redis中;token有效期很长,基本不再过期,但是存在redis里有效时间为2小时;
   1. `AuthorityInfo`中有Map<String, Object> other = new HashMap<>();属性扩展其他信息;
3. 普通需要token的请求,经过JwtFilter拦截器,判断token有效性,延长token在redis中的有效期,并从redis中获取到AuthorityInfo信息,调用RowDataHelper#setAuthrityInfo()存在本地线程,方便后续mybtais拦截数据权限使用;
4. 对于需要拦截数据权限的service,调用RowDataHelper#start(RowDataHandlerType type, String column);(类似PagerHelper.start()分页插件)
   其中type是RowDataHandlerType 接口的枚举实现类,参考`AuthDataRowSqlHandlerType`可自行扩展
5. **退出登录**的时候,调用`JwtService#logout(token)`删除redis中的token

> 

#### 1 前言 及功能点

>  把shiro + jwt 的集成改为一个starter, 并集成mybatis的拦截器

1. 可配置是否开启shiro功能
2. 提供jwt的工具类
3. 提供缓存登录信息的data:
   1. token保持简短, 值保存最基本的信息 userId,userName等
   2. 登录时保存在redis中的信息
      1. token-->userId
      2. userId->userInfo
4. 提供基于mybatis拦截器数据权限拦截器
5. token失效与续期改为redis控制
   1. 每次请求延长token-->userId的过期时间
   2. 退出登录,删除token-->userId

#### 2 集成shiro的方式:

**一般两种方式:**

##### 1 原生的集成

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring</artifactId>
</dependency>
```

##### 2 使用Shiro Starter

> https://shiro.apache.org/spring-boot.html
>
> https://github.com/apache/shiro/tree/master/support/spring-boot

```xml
<dependency>
    <groupId>org.apache.shiro</groupId>
    <artifactId>shiro-spring-boot-web-starter</artifactId>
</dependency>

```



由于本项目中需要实现不少额外的功能点, 故选择使用原生的方式去集成, 以方便额外的拓展

#### 3 本starter相关代码

##### 3.1 代码入口 `SecurityAutoconfigure`

> 其中配置需要激活的几个config: redis, shiro,mybatis拦截器
>
> 另外,注册了AuthGlobalExceeptionHandler,全局shiro权限相关的拦截器

``` java
@Configuration
@ConditionalOnWebApplication
@ConditionalOnExpression("${auth.security.enabled:true}")
@EnableConfigurationProperties({SecurityProperties.class, RedisConfig.class, ShiroConfig.class, MybatisInterceptorConfig.class})
public class SecurityAutoconfigure {
	
	@Autowired 
	SecurityProperties securityProperties;
	
	
	/**
	 * authGlobalExceeptionHandler
	 */
	@Bean
	@ConditionalOnMissingBean
	@ConditionalOnExpression("${auth.handler.exception.enabled:true}")
	public AuthGlobalExceeptionHandler authGlobalExceeptionHandler() {
		return new AuthGlobalExceeptionHandler();
	}
}
```

1. 激活配置类`SecurityProperties`
2. 激活redis配置类`RedisConfig`
3. 激活shiro配置类`ShiroConfig`
4. 激活mybatis拦截器配置类`MybatisInterceptorConfig`

##### 3.2配置项

* `auth.security.enabled`: 是否启用整个安全项目 默认true

- `auth.redis.enabled`： 是否启用`redis` 默认true
- `auth.shiro.enabled`：是否以用 `shiro` 默认true
- `auth.mybatis.interceptor.enabled`: 是否启用`mybatis`数据拦截器，  默认true
- `auth.handler.exception.enabled`： 是否启用`shiro`的全局捕捉异常，  默认true
- `auth.filteRule`: shiro的url过滤策略,是一个map,配置在`SecurityProperties`中,为`shiroFilter`使用
  - 配置方式`auth.filteRule.[filterName]=url1,url2...`  表示每个存在的过滤器名称对应哪些url
  - filterName必须是存在的,如`anon ` `authc` 以及本项目本身配置的 `jwt`等
  - 一个过滤器对应多个url,使用英文逗号分割
  - 最终会转化为shiroFilter`识别的 url1="filter1,filter2"这样的形式

##### 3.3 RedisConfig

```java
@Configuration
@ConditionalOnExpression("${auth.redis.enabled:true}")
@ConfigurationProperties(prefix = SecurityProperties.SECURITY_PREFIX)
public class RedisConfig {

	/**
	 * 使用Lettuce 连接池的方式
	 * @param connectionFactory
	 * @return
	 */
	@Bean("redisTemplateCustomize")
	public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory connectionFactory) {
		// 配置redisTemplate
		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(connectionFactory);
		// key序列化
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		// value序列化
		redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
		redisTemplate.afterPropertiesSet();
		return redisTemplate;
	}

	/**
	 * 
	 * /** 缓存配置管理器
	 */
	@Bean
	@ConditionalOnMissingBean
	public CacheManager cacheManager(LettuceConnectionFactory factory) {
		// 以锁写入的方式创建RedisCacheWriter对象
		RedisCacheWriter writer = RedisCacheWriter.lockingRedisCacheWriter(factory);
		// 创建默认缓存配置对象
		RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofDays(1))
				.serializeKeysWith(
						RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
				.serializeValuesWith(RedisSerializationContext.SerializationPair
						.fromSerializer(new GenericJackson2JsonRedisSerializer()))
				.disableCachingNullValues();
		RedisCacheManager cacheManager = new RedisCacheManager(writer, config);
		return cacheManager;
	}

	/**
	 * redisService
	 */
	@Bean
	@ConditionalOnMissingBean
	public RedisService redisService(@Qualifier("redisTemplateCustomize")RedisTemplate<String, Object> redisTemplate) {
		return new RedisService(redisTemplate);
	}

}
```

> RedisService 封装了对redis的一些基本操作, 依赖配置的redisTemplateCustomize:RedisTemplate

##### 3.4 shiro相关配置

###### 3.4.1 ShiroLifecycleBeanPostProcessorConfig 单独注册LifecycleBeanPostProcessor

> [BeanPostProcessor加载次序及其对Bean造成的影响(影响注入其他属性):]( https://crazyblitz.github.io/2019/08/24/BeanPostProcessor%E5%8A%A0%E8%BD%BD%E6%AC%A1%E5%BA%8F%E5%8F%8A%E5%85%B6%E5%AF%B9Bean%E9%80%A0%E6%88%90%E7%9A%84%E5%BD%B1%E5%93%8D/)

###### 3.4.2 ShiroConfig 

> 注入

1. JwtFilter的注册,并通过`FilterRegistrationBean`设置jwt filter不自动注册到spring管理的监听器中，防止与shiro filter同级，导致该监听器必定执行;
2. 注册`MyRealm`作为shiro数据源的处理(shiro的login和权限来源)
3. 注册`securityManager`(`DefaultWebSecurityManager`)
4. 注册`shiroFilter`
   1. 在filter链中加入JwtFilter(`jwt`)
   2. 一些常用url配置拦截器
   3. 把配置文件中配置的url对应拦截器写入filterRuleMap, 配置参见`SecurityProperties`的`filteRule`属性
   4. 其他shiro的bean

```java
@Configuration
@ConfigurationProperties(prefix = SecurityProperties.SECURITY_PREFIX)
@ConditionalOnClass(Filter.class)
@ConditionalOnBean(RedisTemplate.class)
@ConditionalOnExpression("${auth.shiro.enabled:true}")
@EnableConfigurationProperties({ ShiroLifecycleBeanPostProcessorConfig.class })
@AutoConfigureAfter(ShiroLifecycleBeanPostProcessorConfig.class)
public class ShiroConfig {
	/*
	 * Autowired无法正常注入的疑难杂症 https://www.lagou.com/lgeduarticle/66756.html\
	 */

	@Autowired
	private SecurityProperties securityProperties;

	/**
	 * jwtService
	 */
	@Bean
	@ConditionalOnBean(RedisTemplate.class)
	@ConditionalOnMissingBean
	public JwtService jwtService(RedisService redisService) {
		return new JwtService(redisService);
	}

	@Bean
	public JwtFilter jwtFilter() {
		return new JwtFilter();
	}

	@Bean
	@ConditionalOnMissingBean
	public MyRealm myRealm(JwtService jwtService) {
		return new MyRealm(jwtService);
	}

	@Bean
	@ConditionalOnMissingBean
	public FilterRegistrationBean<JwtFilter> registerJwtFilter(@Autowired JwtFilter jwtFilter) {
		// 设置jwt filter不自动注册到spring管理的监听器中，防止与shiro filter同级，导致该监听器必定执行
		FilterRegistrationBean<JwtFilter> jwtFilterRegister = new FilterRegistrationBean<>(jwtFilter);
		jwtFilterRegister.setEnabled(false);

		return jwtFilterRegister;
	}

	@Bean("securityManager")
	@ConditionalOnMissingBean
	public DefaultWebSecurityManager getManager(MyRealm realm) {
		DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
		/*
		 * 关闭shiro自带的session，详情见文档
		 * http://shiro.apache.org/session-management.html#SessionManagement-
		 * StatelessApplications%28Sessionless%29
		 */
		DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
		DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
		defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
		subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
		manager.setSubjectDAO(subjectDAO);
		// 使用自己的realm
		manager.setRealm(realm);
		return manager;
	}

	@Bean("shiroFilter")
	@ConditionalOnMissingBean
	public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager securityManager) {
		ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();

		// 添加自己的过滤器并且取名为jwt
		Map<String, Filter> filterMap = new HashMap<>(16);
		/**
		 * 修改new JwtFilter() 为jwtFilter(), JwtFilter交由spring管理,方便在JwtFilter 中注入
		 * 但是JwtFilter交给Spring管理后，Spring将其注册到filterChain中了，与ShiroFilter同级，
		 * 所以即使设置了filter的order，在shiroFilter完了之后也会经过JwtFilter，从而导致认证请求调用链的异常
		 * 依然把JwtFilter交由Spring管理，但是设置这个bean不要注册到filter调用链中:
		 * 通过FilterRegistrationBean取消JwtFilter的自动注册， 参考 文档
		 * https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-disable-registration-of-a-servlet-or-filter
		 * 
		 */
		filterMap.put("jwt", jwtFilter());
		factoryBean.setFilters(filterMap);
		factoryBean.setSecurityManager(securityManager);
		/*
		 * 自定义url规则 http://shiro.apache.org/web.html#urls-
		 */
		Map<String, String> filterRuleMap = new LinkedHashMap<>();

		// 访问401和404页面不通过我们的Filter
		// 可匿名访问
		filterRuleMap.put("/login", "anon");
		filterRuleMap.put("/test/**", "anon");
		// 验证码
		filterRuleMap.put("/captcha", "anon");
		// 加入自定义配置的拦截规则
		filterRuleMap.putAll(securityProperties.getFilteRuleMap());
		// 所有请求通过我们自己的 也可put "jwt,authc"\
		// 加了authc不用每个requestMapping都加@RequiresAuthentication
		filterRuleMap.put("/**", "jwt,authc");
		factoryBean.setFilterChainDefinitionMap(filterRuleMap);
		return factoryBean;
	}

	/**
	 * 下面的代码是添加注解支持
	 */
	// 扫描上下文，寻找所有的Advistor(一个Advisor是一个切入点和一个通知的组成)，将这些Advisor应用到所有符合切入点的Bean中
	@Bean
	@ConditionalOnMissingBean
	@DependsOn("lifecycleBeanPostProcessor")
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		// 强制使用cglib，防止重复代理和可能引起代理出错的问题
		// https://zhuanlan.zhihu.com/p/29161098
		defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
		return defaultAdvisorAutoProxyCreator;
	}

	// 开启shiro aop注解支持
	@Bean
	@ConditionalOnMissingBean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(
			DefaultWebSecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
		advisor.setSecurityManager(securityManager);
		return advisor;
	}
}

```

###### 3.4.3 JwtFilter

1. 进入这filter标识需要登录, 请求头中无token,或者token不通过验证,直接返回json给前端
2. token检验成功,直接舒心token在redis中的失效事件,类似session

```java
/**
 * @description: 代码的执行流程 preHandle -> isAccessAllowed -> isLoginAttempt ->
 * executeLogin 。
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:55:08
 */
public class JwtFilter extends BasicHttpAuthenticationFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 是否更新token在REDIS中的过期事件
     */
    private static final boolean REFRESH_REDIS_TOKEN = true;
    
    /**
     * 存在header中的token的key
     */
    private static final String HEADER_TOKEN = "token";

    @Resource
    private RedisService redisService;
    
    @Resource
    private JwtService jwtService;
    
    
    /**
     * 表示当访问拒绝时是否已经处理了；如果返回true表示需要继续处理；如果返回false表示该拦截器实例已经处理了，将直接返回即可。
     */
    @Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		//此处返回json提示
    	BaseResponse baseResponse = BaseResponse.error(401, "请先登录");
    	CommonUtils.writeJson(baseResponse, (HttpServletResponse)response);
		return false;
	}

	/**
     * 判断用户是否想要登入。 检测header里面是否包含token字段即可
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader(HEADER_TOKEN);
        boolean attempt = StringUtils.isNoneBlank(authorization);
        return attempt;
    }

    /**
     * 执行登陆操作
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String authorization = httpServletRequest.getHeader(HEADER_TOKEN);
        JwtToken token = new JwtToken(authorization);
        // 提交给realm进行登入，如果错误他会抛出异常并被捕获
        getSubject(request, response).login(token);
        // 如果登陆成功在判断是否需要刷新token
        afterLogin(authorization);
        return true;
    }

    /**
	 * 在登录成功后所需要做的操作
	 */
	private void afterLogin(String token) {
		//刷新token
		needRefreshToken(token);
		//把用户权限数据从REDIS中取出 放到当前线程中去
		RowDataHelper.setAuthrityInfo(jwtService.getAuthorityInfo(token));
	}

	/**
     * @param token
     * @description: 判断token是否将要过期 提醒前端获取新的token
     * @author: Vic.xu
     * @date: 2020年1月14日 下午2:57:51
     */
    private void needRefreshToken(String token) {
       // String userId = (String) redisService.get(token);
        // 直接刷新token在redis中的过期时间
        if (REFRESH_REDIS_TOKEN) {
        	jwtService.refreshToken(token);
        }

    }

    /**
     * /** 这里我们详细说明下为什么最终返回的都是true，即允许访问 例如我们提供一个地址 GET /article 登入用户和游客看到的内容是不同的
     * 如果在这里返回了false，请求会被直接拦截，用户看不到任何东西 所以我们在这里返回true，Controller中可以通过
     * subject.isAuthenticated() 来判断用户是否登入
     * 如果有些资源只有登入用户才能访问，我们只需要在方法上面加上 @RequiresAuthentication 注解即可
     * 但是这样做有一个缺点，就是不能够对GET,POST等请求进行分别过滤鉴权(因为我们重写了官方的方法)，但实际上对应用影响不大
     */
    /**
     * 2020年5月14日  不再返回true, 而是进入整个filter的请求,在未登录的情况下提示登录 {@link #onAccessDenied(ServletRequest, ServletResponse)}
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (isLoginAttempt(request, response)) {
            try {
                executeLogin(request, response);
            } catch (Exception e) {
                logger.error("executeLogin error: ", e);
                CommonUtils.writeJson(BaseResponse.error(401, "请先登陆"), (HttpServletResponse) response);
                return false;
            }
        }
        return false;
    }

    /**
     * 对跨域提供支持
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "*"
                /* httpServletRequest.getHeader("Access-Control-Request-Headers") */);
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }

}
```

###### 3.4.5 MyRealm  认证策略

1. `doGetAuthorizationInfo`方法根据token把用户的权限信息拿出来放到`AuthorizationInfo`,在后续controller中检验权限注解时需要使用
2. `doGetAuthenticationInfo`方法即JwtFilter中login的具体执行方法,对token的一些检验

```java
**
 * @description: 认证策略 
 * 在doGetAuthorizationInfo方法中把用户的权限设置进去,以配合controller的方法的注解对操作
 * 进行拦截. 用户的权限可以考虑在login的时候 以token为key存储在REDIS中,
 * 也可以在构造token的时候把信息存入token
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:50:13
 */
public class MyRealm extends AuthorizingRealm {

    protected static final Logger LOGGER = LoggerFactory.getLogger(MyRealm.class);

    private JwtService jwtService;
    
    public MyRealm(JwtService jwtService) {
    	this.jwtService = jwtService;
    }

    /**
     * 必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    	String token = principals.toString();
        AuthorityInfo info = jwtService.getAuthorityInfo(token);
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addRoles(info.getRoles());
        simpleAuthorizationInfo.addStringPermissions(info.getPermission());
        LOGGER.info("doGetAuthorizationInfo:{}", info);
        return simpleAuthorizationInfo;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。 SecurityUtils.getSubject().login的时候被调用
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String username = JwtUtil.getUsername(token);
        if (username == null) {
            throw new AuthenticationException("token invalid");
        }

        JwtVerifyEnum verify = jwtService.verify(token);

        if (JwtVerifyEnum.SUCCESS != verify) {
            throw new AuthenticationException("Username or password error");
        }

        return new SimpleAuthenticationInfo(token, token, "my_realm");
    }
}

```



##### 3.5  JWT相关

###### 3.5.1 `JwtUtil`: 基于auth0.jwt生成token,解析token,校验token等

###### 3.5.2 `AuthorityInfo`:放用户的一些权限信息

> userId/username/roles/permission/depts/positions等

###### 3.5.3 `JwtService`:对外提供对token的一些基本操作

> 1. 检测token的合法性: 是否存在redis中
> 2. 根据`AuthorityInfo`构建token, 并分别缓存(token和权限信息): 理应登录时调用
> 3. 根据token或者useId获取缓存的用户权限基本信息
> 4. 退出登录

```java
@Service
public class JwtService {

	@Resource
	private RedisService redisService;

	public JwtService() {

	}

	public JwtService(RedisService redisService) {
		this.redisService = redisService;
	}

	/**
	 * @param token
	 * @return
	 * @description: 校验token的合法性
	 * @author: Vic.xu
	 * @date: 2020年3月20日 上午10:23:40
	 */
	public JwtVerifyEnum verify(String token) {
		boolean result = JwtUtil.verify(token);
		if (!result) {
			return JwtVerifyEnum.INVALID;
		}

		boolean existToken = existToken(token);
		if (!existToken) {
			return JwtVerifyEnum.EXPIRE;
		}
		return JwtVerifyEnum.SUCCESS;
	}

	/**
	 * @param token
	 * @return
	 * @description: token是否在缓存中存在
	 * @author: Vic.xu
	 * @date: 2020年3月20日 上午10:21:54
	 */
	public boolean existToken(String token) {
		Boolean hasKey = redisService.hasKey(AuthUtil.getTokenKey(token));
		return hasKey == null ? false : hasKey;
	}

	/**
	 * 刷新token的过期时间
	 * 
	 * @param token
	 * @return
	 */
	public boolean refreshToken(String token) {
		redisService.expire(AuthUtil.getTokenKey(token), AuthSystemConstant.TOKEN_EXPIRE);
		return true;
	}
	
	/**
	 * 根据用户相关信息 构建token 并且缓存token和用户信息
	 * @param authorityInfo
	 * @return
	 */
	public String buildTokenAndCache(@NotNull AuthorityInfo authorityInfo) {
		String token = JwtUtil.sign(authorityInfo);
		cacheTokenAndAuthorityInfo(token, authorityInfo);
		return token;
	}

	/**
	 * 缓存token以及token对应的用户信息, 理应在登录成功的时候调用此方法
	 * 
	 * @param token
	 * @param authorityInfo
	 * @return
	 */
	public boolean cacheTokenAndAuthorityInfo(String token, @NotNull AuthorityInfo authorityInfo) {
		Integer userId = authorityInfo.getUserId();
		if (userId == null) {
			return false;
		}
		redisService.set(AuthUtil.getTokenKey(token), authorityInfo.getUserId(), AuthSystemConstant.TOKEN_EXPIRE);
		redisService.set(AuthUtil.getUserKey(userId), authorityInfo);
		return true;
	}
	
	/**
	 * 退出登录 : 从REDIS中删除token
	 * @param token
	 * @return
	 */
	public boolean logout(String token) {
		redisService.del(AuthUtil.getTokenKey(token));
		return true;
	}

	/**
	 * 根据token获取REDIS中用户的权限相关信息
	 * @param token
	 * @return
	 */
	public AuthorityInfo getAuthorityInfo(String token) {
		if (!existToken(token)) {
			return new AuthorityInfo();
		}
		Integer userId = (Integer) redisService.get(AuthUtil.getTokenKey(token));
		return getAuthorityInfo(userId);
	}
	
	/**
	 * 根据userId获取REDIS中用户的权限相关信息
	 * @param userId
	 * @return
	 */
	public AuthorityInfo getAuthorityInfo(Integer userId) {
		if(userId == null) {
			return new AuthorityInfo();
		}
		AuthorityInfo info = (AuthorityInfo) redisService.get(AuthUtil.getUserKey(userId));
		return info;
	}
	
}

```

##### 3.6 数据权限拦截器

> 1. 基于前面的jwt中存储的用户的岗位信息/部门信息等数据权限信息
> 2. 基于mybatis的拦截器

###### 3.6.1 `MybatisInterceptorConfig`:为所有的`SqlSessionFactory`配置数据拦截器`RowDataHelper`

```java
@Configuration
@ConfigurationProperties(prefix = SecurityProperties.SECURITY_PREFIX)
@ConditionalOnBean({ SqlSessionFactory.class, SqlSessionFactoryBean.class, MybatisAutoConfiguration.class })
@ConditionalOnExpression("${auth.mybatis.interceptor.enabled:true}")
public class MybatisInterceptorConfig {

	@Autowired(required = false)
	private List<SqlSessionFactory> sqlSessionFactoryList;

	@PostConstruct
	public void mybatisInterceptor() {
		if (sqlSessionFactoryList == null) {
			return;
		}
		RowDataHelper interceptor = new RowDataHelper();
		for (SqlSessionFactory sqlSessionFactory : sqlSessionFactoryList) {
			sqlSessionFactory.getConfiguration().addInterceptor(interceptor);
		}
	}
}
```

###### 3.6.2 `AuthorityMethod`数据权限拦截器的基类

```java
public abstract class AuthorityMethod {

	/**
	 * 当前线程中需要拦截的权限字段数据
	 */
	protected static final ThreadLocal<DataRowAuthModel> LOCAL_AUTHORITY_DATA = new ThreadLocal<DataRowAuthModel>();

	/**
	 * 设置 AuthorityData 参数
	 *
	 * @param AuthorityData
	 */
	protected static void setLocalAuthorityData(DataRowAuthModel authorityData) {
		LOCAL_AUTHORITY_DATA.set(authorityData);
	}

	/**
	 * 获取 AuthorityData 参数
	 *
	 * @return
	 */
	public static DataRowAuthModel getLocalAuthorityData() {
		return LOCAL_AUTHORITY_DATA.get();
	}

	/**
	 * 移除本地变量
	 */
	public static void clearAuthorityData() {
		LOCAL_AUTHORITY_DATA.remove();
	}

	/**
	 * 业务代码设置过滤条件
	 * 
	 * @param type   数据行权限过滤类型
	 * @param column SQL 中需要过滤的字段 如a.dept_id
	 */
	public static void start(AuthDataRowSqlHandlerType type, String column) {
		DataRowAuthModel data = getLocalAuthorityData();
		if (data == null) {
			data = new DataRowAuthModel();
		}
		DataRowAuthColumn filterColumn = new DataRowAuthColumn(type, column);
		data.addFilterColumn(filterColumn);
		setLocalAuthorityData(data);
	}

	/**
	 * 用处理后的SQL覆盖原始SQL
	 * 
	 * 通过反射把SQL设置回去
	 */
	public void overrideSql(BoundSql boundSql, String sql) {
		try {
			Field field = boundSql.getClass().getDeclaredField("sql");
			field.setAccessible(true);
			field.set(boundSql, sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
```



###### 3.6.3 `RowDataHelper`: 权限拦截器 ,  拦截StatementHandler对象的prepare ,然后重写SQL 加入where条件过滤;

> 使用方式类似PagerHelper:RowDataHelper.start(RowDataHandlerType type, String column);
>
> 

```java
@Intercepts({
		@Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class, Integer.class }) })
public class RowDataHelper extends AuthorityMethod implements Interceptor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RowDataHelper.class);

	/**
	 * 1、获取到拦截到的StatementHandler对象； 
	   2、封装StatementHandler对象；
	 * 3、执行Invocation对应的数据库操作方法；
	 */
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		StatementHandler target = (StatementHandler) invocation.getTarget();
		handler(target);
		return invocation.proceed();
	}

	/**
	 * 返回目标对象的代理对象
	 */
	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}


	/**
	 * 数据权限 处理 SQL追加
	 * 
	 * @param handler
	 */
	public void handler(StatementHandler handler) {
		BoundSql boundSql = handler.getBoundSql();
		String sql = boundSql.getSql();
		DataRowAuthModel authorityData = getLocalAuthorityData();
		if (authorityData == null) {
			return;
		}
		Set<DataRowAuthColumn> columns = authorityData.getFilterColumns();
		if (CollectionUtils.isEmpty(columns)) {
			return;
		}
		for (DataRowAuthColumn column : columns) {
			RowDataHandlerType sqlHandler = column.getType();
			// 处理SQL 追加权限
			sql = sqlHandler.handlerSql(sql, column.getColumn());
		}
		overrideSql(boundSql, sql);
	}

	@Override
	public void setProperties(Properties properties) {
		String dialect = properties.getProperty("dialect");
        LOGGER.info("RowDataHelper for mybatis intercept dialect:{}", dialect);
	}
}
```



###### 3.6.4 `AuthDataRowSqlHandlerType` 数据行权限过滤类型枚举, 实现了`RowDataHandlerType`接口

> 如果要扩展类型,则编写新的枚举并实现`RowDataHandlerType`

###### TODO 枚举处理sql方法中,获取有用户的基本信息来自shiro中保存的用户

```java
public interface RowDataHandlerType {	
	/**
	 * 某个类型对SQL的处理
	 * @param sql 原SQL
	 * @param column 需要关联的SQL
	 * @return 新的SQL
	 */
	public abstract String handlerSql(String sql, String column);
}
```

```java
public enum AuthDataRowSqlHandlerType implements RowDataHandlerType{
	/**
	 * 按照部门进行数据权限控制
	 */
	DEPT {
		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereIn(sql, column, info.getDepts(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	},
	/**
	 * 按照岗位进行数据权限控制
	 */
	POSITION {

		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereIn(sql, column, info.getPositions(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	},
	/**
	 * 按照用户进行数据权限控制
	 */
	USER {
		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereEquals(sql, column, info.getUserId(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	};


}
```

###### 3.6.5 `SQLParserUtils`: 基于CCJSqlParserUtil 对本项目中使用的sql追加条件做简单的封装

```java
public class SQLParserUtils {
	
	private SQLParserUtils() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * 
	 * @description: 追加in的where条件
	 * @author: Vic.xu
	 * @date: 2020年3月3日 下午10:31:20
	 * @param sql
	 * @param column
	 * @param conditions
	 * @param needParenthesis
	 * @return
	 * @throws JSQLParserException
	 */
	public static String appendWhereIn(String sql, String column, List<String> conditions, boolean needParenthesis)
			throws JSQLParserException {
		String append = findIn(column, conditions);
		return appendWhere(sql, append, needParenthesis);

	}

	/**
	 * 
	 * @description: 追加Equals的where条件
	 * @author: Vic.xu
	 * @date: 2020年3月3日 下午10:31:34
	 * @param sql
	 * @param column
	 * @param value
	 * @param needParenthesis
	 * @return
	 * @throws JSQLParserException
	 */
	public static String appendWhereEquals(String sql, String column, Object value, boolean needParenthesis)
			throws JSQLParserException {
		String append = findEquals(column, value);
		return appendWhere(sql, append, needParenthesis);

	}

	/**
	 * 
	 * @description:
	 * @author: Vic.xu
	 * @date: 2020年3月3日 下午10:08:11
	 * @param sql             原SQL
	 * @param append          需要追加的条件
	 * @param needParenthesis 追加的条件是否需要括弧
	 * @return
	 * @throws JSQLParserException
	 */
	public static String appendWhere(String sql, String append, boolean needParenthesis) throws JSQLParserException {
		Select select = (Select) CCJSqlParserUtil.parse(sql);
		PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
		Expression where = CCJSqlParserUtil.parseCondExpression(append);
		if (needParenthesis) {
			where = new Parenthesis(where);
		}
		plainSelect.setWhere(new AndExpression(plainSelect.getWhere(), where));
		return plainSelect.toString();
	}

	/**
	 * @description: 拼接in 条件： column in ('','')
	 * @author: Vic.xu
	 * @date: 2020年3月3日 下午10:11:43
	 * @param column
	 * @param conditions
	 * @return
	 */
	public static String findIn(String column, List<String> conditions) {
		return column + " in (" + CommonUtils.list2DatabaseIn(conditions) + ")";
	}

	/**
	 * @description: 拼接等于条件： column ='value'
	 * @author: Vic.xu
	 * @date: 2020年3月3日 下午10:11:43
	 * @param column
	 * @param conditions
	 * @return
	 */
	public static String findEquals(String column, Object value) {
		if(value instanceof Number) {
			return column + " = " + value + " ";
		}
		return column + " = '" + value + "' ";
	}

}

```

#### 4 遗留的问题

1. `RowDataHelper`清除线程中权限数据
2. 权限拦截器扩展性优化
3. (后续再补充吧)