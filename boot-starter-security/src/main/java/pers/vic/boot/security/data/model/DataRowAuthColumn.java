package pers.vic.boot.security.data.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import pers.vic.boot.security.data.enums.RowDataHandlerType;

/**
 * @description: 需要过滤的权限列字段; 当type一致 就认为是一个对象
 * @author VIC.xu
 * @date: 2020年2月11日 下午3:47:17
 */
public class DataRowAuthColumn {

    // 权限类型
    private RowDataHandlerType type;

    // 过滤的列 如a.dept_id
    private String column;

    public DataRowAuthColumn(RowDataHandlerType type, String column) {
        super();
        this.type = type;
        this.column = column;
    }

    public RowDataHandlerType getType() {
        return type;
    }

    public void setType(RowDataHandlerType type) {
        this.type = type;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(type).append(column).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DataRowAuthColumn)) {
            return false;
        }
        DataRowAuthColumn data = (DataRowAuthColumn) obj;
        return new EqualsBuilder().append(type, data.getType()).append(column, data.getColumn()).isEquals();
    }
}
