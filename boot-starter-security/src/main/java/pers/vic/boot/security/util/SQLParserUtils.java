package pers.vic.boot.security.util;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import pers.vic.boot.util.CommonUtils;

import java.util.List;

/**
 * @description: 本项目中使用的一些SQL解析
 * @author VIC.xu
 * @date: 2020年3月3日 下午10:04:49
 */
public class SQLParserUtils {

    private SQLParserUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     *
     * @description: 追加in的where条件
     * @author: Vic.xu
     * @date: 2020年3月3日 下午10:31:20
     * @param sql
     * @param column
     * @param conditions
     * @param needParenthesis
     * @return
     * @throws JSQLParserException
     */
    public static String appendWhereIn(String sql, String column, List<String> conditions, boolean needParenthesis)
            throws JSQLParserException {
        String append = findIn(column, conditions);
        return appendWhere(sql, append, needParenthesis);

    }

    /**
     *
     * @description: 追加Equals的where条件
     * @author: Vic.xu
     * @date: 2020年3月3日 下午10:31:34
     * @param sql
     * @param column
     * @param value
     * @param needParenthesis
     * @return
     * @throws JSQLParserException
     */
    public static String appendWhereEquals(String sql, String column, Object value, boolean needParenthesis)
            throws JSQLParserException {
        String append = findEquals(column, value);
        return appendWhere(sql, append, needParenthesis);

    }

    /**
     *
     * @description:
     * @author: Vic.xu
     * @date: 2020年3月3日 下午10:08:11
     * @param sql             原SQL
     * @param append          需要追加的条件
     * @param needParenthesis 追加的条件是否需要括弧
     * @return
     * @throws JSQLParserException
     */
    public static String appendWhere(String sql, String append, boolean needParenthesis) throws JSQLParserException {
        Select select = (Select) CCJSqlParserUtil.parse(sql);
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        Expression where = CCJSqlParserUtil.parseCondExpression(append);
        if (needParenthesis) {
            where = new Parenthesis(where);
        }
        if (plainSelect.getWhere() == null) {
            plainSelect.setWhere(where);
        } else {
            plainSelect.setWhere(new AndExpression(plainSelect.getWhere(), where));

        }
        return plainSelect.toString();
    }

    /**
     * @description: 拼接in 条件： column in ('','')
     * @author: Vic.xu
     * @date: 2020年3月3日 下午10:11:43
     * @param column
     * @param conditions
     * @return
     */
    public static String findIn(String column, List<String> conditions) {
        return column + " in (" + CommonUtils.list2DatabaseIn(conditions) + ")";
    }

    /**
     * @description: 拼接等于条件： column ='value'
     * @author: Vic.xu
     * @date: 2020年3月3日 下午10:11:43
     * @param column
     * @param value
     * @return
     */
    public static String findEquals(String column, Object value) {
        if (value instanceof Number) {
            return column + " = " + value + " ";
        }
        return column + " = '" + value + "' ";
    }

}
