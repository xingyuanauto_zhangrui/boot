package pers.vic.boot.security.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pers.vic.boot.security.data.MybatisInterceptorConfig;
import pers.vic.boot.security.redis.RedisConfig;
import pers.vic.boot.security.shiro.AuthGlobalExceeptionHandler;
import pers.vic.boot.security.shiro.ShiroConfig;


/**
 * @description: 安全starter 入口
 * @author: Vic.xu
 * @date: 2020年3月30日
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnExpression("${auth.security.enabled:true}")
@EnableConfigurationProperties({SecurityProperties.class})
@Import({RedisConfig.class, ShiroConfig.class, MybatisInterceptorConfig.class})
public class SecurityAutoconfigure {

    @Autowired
    SecurityProperties securityProperties;

    /**
     * Security 自动配置需要完成的功能：
     * 1. SHIRO的配置ShiroConfig √  依赖于redisService jwtService等
     * 2. 数据权限拦截的配置 MybatisInterceptorConfig √
     * 3. JWT service的注入 √ 依赖于redisTemplate
     * 4. REDIS 配置和service的注入，被JWT相关依赖 √
     * 5. 鉴权异常捕捉类 AuthGlobalExceeptionHandler √
     *
     */

    /**
     * authGlobalExceeptionHandler
     */
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnExpression("${auth.handler.exception.enabled:true}")
    public AuthGlobalExceeptionHandler authGlobalExceeptionHandler() {
        return new AuthGlobalExceeptionHandler();
    }


}
