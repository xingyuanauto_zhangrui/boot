package pers.vic.boot.security.jwt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;


/**
 * @description:存放用户的一些权限信息
 * @author VIC.xu
 * @date: 2020年1月13日 上午10:13:15
 */
public class AuthorityInfo {

	private Integer userId;
	// 用户名
	private String username;
	// 拥有的角色
	private List<String> roles = new ArrayList<String>();
	// 拥有的权限
	private List<String> permission = new ArrayList<String>();

	// 当前用户所在的部门
	private List<String> depts = new ArrayList<String>();
	// 当前用户所在的岗位
	private List<String> positions = new ArrayList<String>();
	
	//其他信息
	private Map<String, Object> other = new HashMap<>();

	/**
	 * 开始检测是否需要提醒前端更换token
	 */
	private long expireCheckTime;

	/**
	 * @description: 是否需要提醒刷新
	 * @author: Vic.xu
	 * @date: 2020年1月14日 下午1:43:08
	 * @return
	 */
	public boolean needRefresh() {
		return expireCheckTime < System.currentTimeMillis();
	}
	
	public AuthorityInfo addOther(String key, Object value) {
		other.put(key, value);
		return this;
	}

	public AuthorityInfo(Integer userId, String username, List<String> roles, List<String> permission) {
		super();
		this.userId = userId;
		this.username = username;
		this.roles = roles;
		this.permission = permission;
		filterNull();
	}

	public AuthorityInfo(Integer userId, String username, String[] roles, String[] permission, long expireCheckTime) {
		super();
		this.userId = userId;
		this.username = username;
		this.expireCheckTime = expireCheckTime;
		this.roles = new ArrayList<String>(Arrays.asList(roles == null ? new String[] {} : roles));
		this.permission = new ArrayList<String>(Arrays.asList(permission == null ? new String[] {} : permission));
		filterNull();

	}

	/**
	 * @description: 过滤掉空
	 * @author: Vic.xu
	 * @date: 2020年1月13日 下午5:15:44
	 */
	private void filterNull() {
		this.roles = filterEmptyItem(roles);
		this.permission = filterEmptyItem(permission);
	}

	/**
	 * 
	 * @description: 过滤
	 * @author: Vic.xu
	 * @date: 2020年1月13日 下午5:13:41
	 * @param list
	 * @return
	 */
	private static List<String> filterEmptyItem(List<String> list) {
		return Optional.ofNullable(list).orElse(new ArrayList<String>()).stream().filter(item -> {
			return StringUtils.isNotEmpty(item);
		}).collect(Collectors.toList());
	}

	public AuthorityInfo() {
	}

	public Set<String> findRoleSet() {
		if (CollectionUtils.isNotEmpty(roles)) {
			return new HashSet<String>(roles);
		}
		return Collections.emptySet();
	}
	public Set<String> findPermissionSet() {
		if (CollectionUtils.isNotEmpty(permission)) {
			return new HashSet<String>(permission);
		}
		return Collections.emptySet();
	}
	
	
	/************************ 自动生成的getter and setter ↓***************************************************/

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the roles
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	/**
	 * @return the permission
	 */
	public List<String> getPermission() {
		return permission;
	}

	/**
	 * @param permission the permission to set
	 */
	public void setPermission(List<String> permission) {
		this.permission = permission;
	}

	/**
	 * @return the depts
	 */
	public List<String> getDepts() {
		return depts;
	}

	/**
	 * @param depts the depts to set
	 */
	public void setDepts(List<String> depts) {
		this.depts = depts;
	}

	/**
	 * @return the positions
	 */
	public List<String> getPositions() {
		return positions;
	}

	/**
	 * @param positions the positions to set
	 */
	public void setPositions(List<String> positions) {
		this.positions = positions;
	}

	/**
	 * @return the expireCheckTime
	 */
	public long getExpireCheckTime() {
		return expireCheckTime;
	}
	
	

	/**
	 * @return the other
	 */
	public Map<String, Object> getOther() {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther(Map<String, Object> other) {
		this.other = other;
	}

	/**
	 * @param expireCheckTime the expireCheckTime to set
	 */
	public void setExpireCheckTime(long expireCheckTime) {
		this.expireCheckTime = expireCheckTime;
	}
	
	
	

}
