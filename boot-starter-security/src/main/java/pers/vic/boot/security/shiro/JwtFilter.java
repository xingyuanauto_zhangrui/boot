package pers.vic.boot.security.shiro;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.security.data.interceptor.RowDataHelper;
import pers.vic.boot.security.jwt.JwtService;
import pers.vic.boot.security.jwt.JwtToken;
import pers.vic.boot.security.redis.RedisService;
import pers.vic.boot.util.CommonUtils;

/**
 * @description: 代码的执行流程 preHandle -> isAccessAllowed -> isLoginAttempt ->
 * executeLogin 。
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:55:08
 */
public class JwtFilter extends BasicHttpAuthenticationFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 是否更新token在REDIS中的过期事件
     */
    private static final boolean REFRESH_REDIS_TOKEN = true;
    
    /**
     * 存在header中的token的key
     */
    private static final String HEADER_TOKEN = "token";

    @Resource
    private RedisService redisService;
    
    @Resource
    private JwtService jwtService;
    
    
    /**
     * 表示当访问拒绝时是否已经处理了；如果返回true表示需要继续处理；如果返回false表示该拦截器实例已经处理了，将直接返回即可。
     */
    @Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		//此处返回json提示
    	BaseResponse<?> baseResponse = BaseResponse.error(401, "请先登录");
    	CommonUtils.writeJson(baseResponse, (HttpServletResponse)response);
		return false;
	}

	/**
     * 判断用户是否想要登入。 检测header里面是否包含token字段即可
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader(HEADER_TOKEN);
        boolean attempt = StringUtils.isNoneBlank(authorization);
        return attempt;
    }

    /**
     * 执行登陆操作
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String authorization = httpServletRequest.getHeader(HEADER_TOKEN);
        JwtToken token = new JwtToken(authorization);
        // 提交给realm进行登入，如果错误他会抛出异常并被捕获
        getSubject(request, response).login(token);
        // 如果登陆成功在判断是否需要刷新token
        afterLogin(authorization);
        return true;
    }

    /**
	 * 在登录成功后所需要做的操作
	 */
	private void afterLogin(String token) {
		//刷新token
		needRefreshToken(token);
		//把用户权限数据从REDIS中取出 放到当前线程中去
		RowDataHelper.setAuthrityInfo(jwtService.getAuthorityInfo(token));
	}

	/**
     * @param token
     * @description: 判断token是否将要过期 提醒前端获取新的token
     * @author: Vic.xu
     * @date: 2020年1月14日 下午2:57:51
     */
    private void needRefreshToken(String token) {
       // String userId = (String) redisService.get(token);
        // 直接刷新token在redis中的过期时间
        if (REFRESH_REDIS_TOKEN) {
        	jwtService.refreshToken(token);
        }

    }

    /**
     * /** 这里我们详细说明下为什么最终返回的都是true，即允许访问 例如我们提供一个地址 GET /article 登入用户和游客看到的内容是不同的
     * 如果在这里返回了false，请求会被直接拦截，用户看不到任何东西 所以我们在这里返回true，Controller中可以通过
     * subject.isAuthenticated() 来判断用户是否登入
     * 如果有些资源只有登入用户才能访问，我们只需要在方法上面加上 @RequiresAuthentication 注解即可
     * 但是这样做有一个缺点，就是不能够对GET,POST等请求进行分别过滤鉴权(因为我们重写了官方的方法)，但实际上对应用影响不大
     */
    /**
     * 2020年5月14日  不再返回true, 而是进入整个filter的请求,在未登录的情况下提示登录 {@link #onAccessDenied(ServletRequest, ServletResponse)}
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
    	logger.info("请求 {} 进入jwtFilter", CommonUtils.getRequestUrl((HttpServletRequest) request));
        if (isLoginAttempt(request, response)) {
            try {
                executeLogin(request, response);
                return true;
            } catch (Exception e) {
                logger.error("executeLogin error: ", e);
                CommonUtils.writeJson(BaseResponse.error(401, "请先登陆"), (HttpServletResponse) response);
                return false;
            }
        }
        return false;
    }

    /**
     * 对跨域提供支持
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "*"
                /* httpServletRequest.getHeader("Access-Control-Request-Headers") */);
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }

}