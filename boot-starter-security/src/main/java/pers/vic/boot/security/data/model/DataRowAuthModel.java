package pers.vic.boot.security.data.model;

import java.util.HashSet;
import java.util.Set;

/** 
 * @description: 存储在当前线程中用于权限处理拦截器的基本数据
 * @author: Vic.xu
 * @date: 2020年2月11日 下午3:41:07
 */
public class DataRowAuthModel {

	
	/**
	 * 需要过滤的权限字段
	 */
	private Set<DataRowAuthColumn> filterColumns = new HashSet<DataRowAuthColumn>();
	
	/**
	 * 新增要过滤的权限字段
	 */
	public DataRowAuthModel addFilterColumn(DataRowAuthColumn column) {
		this.filterColumns.add(column);
		return this;
	}
	public Set<DataRowAuthColumn> getFilterColumns() {
		return filterColumns;
		
	}
}
