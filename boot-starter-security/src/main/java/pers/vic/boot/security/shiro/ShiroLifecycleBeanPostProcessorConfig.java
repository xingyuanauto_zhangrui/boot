/**
 * 
 */
package pers.vic.boot.security.shiro;

import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  @description: 把LifecycleBeanPostProcessor从shiroConfig中抽离出来,以防造成Autowired注入的问题
 *  @author Vic.xu
 *  @date: 2020年5月13日下午5:56:26
 */
@Configuration
public class ShiroLifecycleBeanPostProcessorConfig {
	/**
	 * TODO Autowired无法正常注入的疑难杂症 :
	 * https://www.lagou.com/lgeduarticle/66756.html
	 * BeanPostProcessor加载次序及其对Bean造成的影响:
	 * https://crazyblitz.github.io/2019/08/24/BeanPostProcessor%E5%8A%A0%E8%BD%BD%E6%AC%A1%E5%BA%8F%E5%8F%8A%E5%85%B6%E5%AF%B9Bean%E9%80%A0%E6%88%90%E7%9A%84%E5%BD%B1%E5%93%8D/
	 */

	// 生命周期控制
	@Bean
	@ConditionalOnMissingBean
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	
}
