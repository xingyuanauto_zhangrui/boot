package pers.vic.boot.security.jwt.enums;

/** 
 * @description: JWT校验结构
 * @author: Vic.xu
 * @date: 2020年3月20日 上午10:19:26
 */
public enum JwtVerifyEnum {
	
	// 成功
	SUCCESS,
	
	// 不合法的 无效的
	INVALID,
	
	//过期的
	EXPIRE;
}
