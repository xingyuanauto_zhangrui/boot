package pers.vic.boot.security.constant;

/**
 * @description:
 * @author: Vic.xu
 * @date: 2020年3月5日 上午11:20:51
 */
public class AuthSystemConstant {

	private AuthSystemConstant() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * 存在REDIS中的token的前缀， 完整key： 前缀 + token
	 */
	public static final String TOKEN_KEY_PREFIX = "auth_token:";
	
	/**
	 * 存在REDIS中的AuthorityInfo的前缀， 完整key： 前缀 + userId
	 */
	public static final String USER_KEY_PREFIX = "auth_user:";
	/**
	 * token过期时间 秒(2小时)
	 */
	public static final long TOKEN_EXPIRE = 2 * 60 * 60;
	
	/**
	 * user过期时间 秒(8小时)
	 */
	public static final long USER_EXPIRE = 8 * 60 * 60;

}
