package pers.vic.boot.security;

import org.springframework.boot.SpringApplication;

/**
 *  @description: 启动类
 *  @author Vic.xu
 *  @date: 2020年5月6日下午3:12:43
 */
//@SpringBootApplication
public class Application {

	 public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
	}
}
