package pers.vic.boot.security.autoconfigure;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import pers.vic.boot.security.shiro.ShiroConfig;

/**
 * @description: security配置项
 * @author: Vic.xu
 * @date: 2020年3月30日
 */
@Configuration
@ConfigurationProperties(prefix = SecurityProperties.SECURITY_PREFIX)
public class SecurityProperties {

	/**
	 * 本SDK配置参数前缀
	 */
	public static final String SECURITY_PREFIX = "auth";

	/**
	 * SHIRO 过滤URL规则 和filteRuleMap相反
	 * 类似filterName1-> url1,url2, filterName2->url2,url3,url4 
	 * @see ShiroConfig#shiroFilter(org.apache.shiro.web.mgt.DefaultWebSecurityManager)
	 */
	private Map<String, String> filteRule;

	private boolean enabled;

	/**
	 * 转换配置的shiro的url匹配规则
	 * 把类似filterName1-> url1,url2, filterName2->url2,url3,url4 转化为filterRuleMap
	 * @return
	 */
	public Map<String, String> getFilteRuleMap() {
		if (filteRule == null) {
			return new HashMap<String, String>();
		}
		Map<String, String> result = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : filteRule.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if (value != null) {
				String[] values = value.split(",");
				for (String url : values) {
					String filterName = result.get(url);
					if (filterName == null) {
						result.put(url, key);
					} else {
						result.put(url, filterName + "," + key);
					}
				}
			}
		}
		return result;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the filteRule
	 */
	public Map<String, String> getFilteRule() {
		return filteRule;
	}

	/**
	 * @param filteRule the filteRule to set
	 */
	public void setFilteRule(Map<String, String> filteRule) {
		this.filteRule = filteRule;
	}

}
