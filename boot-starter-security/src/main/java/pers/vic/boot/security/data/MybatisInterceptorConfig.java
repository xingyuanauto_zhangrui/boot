package pers.vic.boot.security.data;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import pers.vic.boot.security.data.interceptor.RowDataHelper;

import javax.annotation.PostConstruct;
import java.util.List;


@Configuration
@ConditionalOnBean({ SqlSessionFactory.class, SqlSessionFactoryBean.class, MybatisAutoConfiguration.class })
@ConditionalOnExpression("${auth.mybatis.interceptor.enabled:true}")
public class MybatisInterceptorConfig {

	@Autowired(required = false)
	private List<SqlSessionFactory> sqlSessionFactoryList;

	@PostConstruct
	public void mybatisInterceptor() {
		if (sqlSessionFactoryList == null) {
			return;
		}
		RowDataHelper interceptor = new RowDataHelper();
		for (SqlSessionFactory sqlSessionFactory : sqlSessionFactoryList) {
			sqlSessionFactory.getConfiguration().addInterceptor(interceptor);
		}
	}
}
