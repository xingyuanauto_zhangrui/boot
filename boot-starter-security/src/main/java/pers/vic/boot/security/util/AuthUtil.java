package pers.vic.boot.security.util;

import pers.vic.boot.security.constant.AuthSystemConstant;
import pers.vic.boot.security.jwt.AuthorityInfo;

/** 
 * @description: 权限业务工具类
 * @author: Vic.xu
 * @date: 2020年5月13日 12:57:46
 */
public class AuthUtil {
	
	private AuthUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	/**
	 * 
	 * @description: 存储token的key
	 * @author: Vic.xu
	 * @date: 2020年3月5日 上午11:25:48
	 * @param userId
	 * @return
	 */
	public static String getTokenKey(String token) {
		return AuthSystemConstant.TOKEN_KEY_PREFIX + token;
	}
	
	/**
	 * 存在REDIS中的 @see {@link AuthorityInfo}  的key： 前缀 + userId
	 */
	public static String getUserKey(Integer userId) {
		return AuthSystemConstant.USER_KEY_PREFIX + userId;
	}
	
	

}
