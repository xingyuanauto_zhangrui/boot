package pers.vic.boot.security.data.enums;

import pers.vic.boot.security.data.interceptor.RowDataHelper;
import pers.vic.boot.security.jwt.AuthorityInfo;
import pers.vic.boot.security.util.SQLParserUtils;

/**
 * 数据行权限过滤类型
 * @author Vic.xu
 *
 */
public enum AuthDataRowSqlHandlerType implements RowDataHandlerType{
	/**
	 * 按照部门进行数据权限控制
	 */
	DEPT {
		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereIn(sql, column, info.getDepts(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	},
	/**
	 * 按照岗位进行数据权限控制
	 */
	POSITION {

		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereIn(sql, column, info.getPositions(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	},
	/**
	 * 按照用户进行数据权限控制
	 */
	USER {
		@Override
		public String handlerSql(String sql, String column) {
			AuthorityInfo info = RowDataHelper.getAuthrityInfo();
			if (info == null) {
				return sql;
			}
			try {
				return SQLParserUtils.appendWhereEquals(sql, column, info.getUserId(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return sql;
		}
	};


}
