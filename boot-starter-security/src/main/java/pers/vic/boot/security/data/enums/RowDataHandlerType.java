/**
 * 
 */
package pers.vic.boot.security.data.enums;

/**
 *  @description: 数据权限类型枚举的接口
 *  @author Vic.xu
 *  @date: 2020年5月14日上午10:15:12
 */
public interface RowDataHandlerType {
	
	/**
	 * 某个类型对SQL的处理
	 * @param sql 原SQL
	 * @param column 需要关联的SQL
	 * @return 新的SQL
	 */
	public abstract String handlerSql(String sql, String column);

}
