package pers.vic.sso.demo.user.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pers.vic.boot.util.JsonUtil;
import pers.vic.sso.common.model.RpcAccessToken;

/**
 * 描述:
 *
 * @author Vic.xu
 * @date 2021-11-09 16:39
 */
@Service
public class UserService {

    private Logger logger = LoggerFactory.getLogger(UserService.class);


    /**
     * 登录成功之后的回调
     */
    public void afterLogin(RpcAccessToken accessToken) {
        logger.info("登录成功之后的回调：  {}", JsonUtil.toJson(accessToken));
    }

    /**
     * 登出成功之后的回调
     */
    public void afterLogout(String accessToken) {
        logger.info("登出成功之后的回调：  {}", accessToken);
    }
}
