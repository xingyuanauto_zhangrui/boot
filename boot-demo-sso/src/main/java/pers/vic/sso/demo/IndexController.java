package pers.vic.sso.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalTime;

/**
 * 描述:
 *
 * @author Vic.xu
 * @date 2021-11-04 10:13
 */
@RestController
@RequestMapping
public class IndexController {

    @GetMapping(value = {"/", ""})
    public String index(){

        return LocalTime.now() + ", welcome to index!";
    }

    @GetMapping(value = {"index", "home"})
    public String home(){
        return LocalTime.now() + ", welcome go home!";
    }
}
